$("#envio_form").on('click', function (e){
    $("#cargaIcono").css("display","inline-block");
    //Aparentamos la carga de la tabla
    $("#formulario").css("background-color","#ddd");

    // Evitamos que salte el enlace.
    e.preventDefault(); 
    var validaciones = validar();
    if(validaciones){
        $('input').attr('disabled',false);
        $('textarea').attr('disabled',false);
        
    	var paqueteDatos = new FormData(document.getElementById('formulario'));
    	var base = $("#sitio").val();

        $.ajax({
            url: base+"inventario_lavadores/inventario/guardar_formulario", 
            type: 'post',
            contentType: false,
            data: paqueteDatos,
            processData: false,
            cache: false,
            success:function(resp){
                console.log(resp);
                if (resp.indexOf("handler         </p>")<1) {
                    var respuesta = resp.split("_");
                    if (respuesta[0] == "OK") {
                        //location.href = base+"index.php/inventario_lavadores/inventario/index"; 
                        location.href = base+"index.php/inventario_lavadores/inventario/generar_revision/"+respuesta[1];
                    } else {
                        $("#formulario_error").text(respuesta[0]);
                    }

                }else{
                    $("#formulario_error").text("SE PRODUJO UN ERROR AL ENVIAR LA INFORMACIÓN");
                    //$("#formulario_error").text(resp);
                }
                //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                $("#cargaIcono").css("display","none");
                //Regresamos los colores de la tabla a la normalidad
                $("#formulario").css("background-color","white");
            //Cierre de success
            },
              error:function(error){
                console.log(error);
                //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                $("#cargaIcono").css("display","none");
                //Regresamos los colores de la tabla a la normalidad
                $("#formulario").css("background-color","white");
                $("#formulario_error").text("SE PRODUJO UN ERROR EN EL ENVIO");
                //location.href = base+"index.php/inventario/index/"+orden;
            }
        });
    }else{
        //Si ya se termino de cargar la tabla, ocultamos el icono de carga
        $("#cargaIcono").css("display","none");
        //Regresamos los colores de la tabla a la normalidad
        $("#formulario").css("background-color","white");
    }
});

function validar() {
    var retorno = true;
    var tipo_formulario = $("input[name='envio_formulario']").val();
    if (tipo_formulario == "1") {
        var danos = $("input[name='marca_danos']").val();
        if (danos == "0") {
            $("#danosMarcas").val("statics/inventario/imgs/diagrama_moto.jpeg"); 
        }else{
            var canvas_diagrama = document.getElementById('canvas_3');
            var dataURL = canvas_diagrama.toDataURL('image/jpeg');
            $("#danosMarcas").val(dataURL);

            //guardar_img();
        }
    }

    var campo_11 = $("input[name='nivel_gasolina']").val();
    if (campo_11 == "") {
        var error_11 = $("#nivel_gasolina_error");
        error_11.empty();
        error_11.append('<label class="form-text text-danger">Campo requerido</label>');
        retorno = false;
    }else{
        var error_11 = $("#nivel_gasolina_error");
        error_11.empty();
    }

    var campo_12 = $("input[name='ruta_firma_lavador']").val();
    if (campo_12 == "") {
        var error_12 = $("#ruta_firma_lavador_error");
        error_12.empty();
        error_12.append('<label class="form-text text-danger">Falta firma del asesor</label>');
        retorno = false;
    }else{
        var error_12 = $("#ruta_firma_lavador_error");
        error_12.empty();
    }

    var check_inidcadores = $(".topicos:checked").length;
    if (check_inidcadores <= 2) {
        var error_8 = $("#topicos_error");
        error_8.empty();
        error_8.append('<label class="form-text text-danger">Faltan inidcadores que marcar</label>');
        retorno = false;
    }else{
        var error_8 = $("#topicos_error");
        error_8.empty();
    }

    return retorno;
}

function guardar_img() {
    console.log("entro a la funcion");
    var base = $("#sitio").val();
    var diagrama = $("#danosMarcas").val();
    var url_sis = base+"inventario_lavadores/inventario/guardar_diagrama";
    $.ajax({
        url: url_sis,
        method: 'post',
        data: {
            diagrama : diagrama
        },
        success:function(resp){
            console.log(resp);
            if (resp.indexOf("handler           </p>")<1) {
                var respuesta = resp.split("=");
                if (respuesta[0] == "OK") {
                   $("#danosMarcas").val(respuesta[1]);
                } else {
                    $("#formulario_error").text("NO SE PUDO GUARDAR EL DIAGRAMA");
                }
            }
        //Cierre de success
        },
        error:function(error){
            console.log(error);
        //Cierre del error
        }
    //Cierre del ajax
    });
}