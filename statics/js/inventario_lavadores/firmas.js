$(document).ready(function() {
    var alto = screen.height;
    var ancho = screen.width;

    //Verificamos el tamaño de la pantalla
    if (ancho < 501) {
        //Calculamos las medidad de la pantalla
        var nvo_altura = alto * 0.20;
        var nvo_ancho = ancho * 0.80;
        console.log("Movil");
    }else if ((ancho > 499) && (ancho < 1030)) {
        //Calculamos las medidad de la pantalla
        var nvo_altura = (alto * 0.22)+10;
        var nvo_ancho = (ancho * 0.50)+10;
        console.log("Tablet");
    }else {
        //Calculamos las medidad de la pantalla
        var nvo_altura = alto * 0.22;
        var nvo_ancho = ancho * 0.33;
        console.log("PC");
    }

    // Asignamos los valores al recuadro de la firma
    $("#canvas").prop("width",nvo_ancho.toFixed(2));
    $("#canvas").prop("height","200");

    //Lienzo para firma general
    var signaturePad = new SignaturePad(document.getElementById('canvas'));

    $("#btnSign").on('click', function(){
        //Recuperamos la ruta de la imagen
        var data = signaturePad.toDataURL('image/png');
        //Comprobamos a donde se enviara
        var destino = $('#destinoFirma').val();
        //console.log(destino);
        // Comprobamos el tipo de usuario que firmara
        if (destino == "lavador") {
            $('#ruta_firma_lavador').val(data);
            $("#firma_lavador_url").attr("src",data);

        } else if (destino == "supervisor") {
            $('#ruta_firma_supervisor').val(data);
            $("#firma_supervisor_url").attr("src",data);

        }

        signaturePad.clear();
    });

    $('#limpiar').on('click', function(){
        signaturePad.clear();
    });

    $('#cerrarCuadroFirma').on('click', function(){
        signaturePad.clear();
    });
});

$(".cuadroFirma").on('click',function() {
    var firma = $(this).data("value");
    $('#destinoFirma').val(firma);
    //console.log(firma);
    // Comprobamos el tipo de usuario que firmara
    if (firma == "lavador") {
        //Cambiamos el encabezado del modal
        $("#firmaDigitalLabel").text("Firma del Lavador.");
    } else if (firma == "supervisor") {
        //Cambiamos el encabezado del modal
        $("#firmaDigitalLabel").text("Firma del Supervisor.");
    }

    //$(".modal-backdrop").css("z-index","-1");
});
