    //Evidencia
    $(".fileupload_evidencia").each(function() {
        $(this).fileupload({
                dataType: 'json',
                done: function(e, data) {
                    $.each(data.result, function(index, file) {
                        if (file.isok) {
                            $("#evidencia").val(file.file_name);
                            $("#path_evidencia").val(file.path);
                            var html = "<a href='statics/evidencia/"+file
                            .file_name +"' target='_blank' title='" + file.client_name +
                                "' class='btn btn-info btn-xs'><i class='fa fa-save'></i> Descargar</a>";
                            html = html +
                                "<a href='{{ base_url() }}servicios/upload_evidencia/" + file
                                .file_name + "' data-id='" + file.file_name +
                                "' 'title='Eliminar " + file.client_name +
                                "' class='btn btn-xs eliminar_evidencia text-right'><i class='fa fa-trash'></i></a>";

                            $("#files_evidencia").html(html);
                            $("#divfileupload").hide();
                        } else {
                            ErrorCustom("Error al procesar el documento:<br/>" + file.error,
                                "warning");
                        }
                    });
                },

                start: function(e, data) {
                    $('#progress_evidencia').show();
                    $('#progress_evidencia .progress-bar_evidencia').html('0%');
                    $('#progress_evidencia .progress-bar_evidencia').css(
                        'width',
                        '0%'
                    );
                },
                progressall: function(e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('#progress_evidencia .progress-bar_evidencia').html(progress + '%');
                    $('#progress_evidencia .progress-bar_evidencia').css(
                        'width',
                        progress + '%'
                    );
                    setTimeout(function() {
                        $('#progress_evidencia').hide();
                    }, 1000);

                },
                progress: function(e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('#progress_evidencia .progress-bar_evidencia').html(progress + '%');
                    $('#progress_evidencia .progress-bar_evidencia').css(
                        'width',
                        progress + '%'
                    );
                }
            }).prop('disabled', !$.support.fileInput)
            .parent().addClass($.support.fileInput ? undefined : 'disabled');

    }); //fileupload pdf
    $("body").on("click",'.eliminar_evidencia',function(e){
        e.preventDefault();
        archivo_eliminar = $("#path_evidencia").val();
        ConfirmCustom("¿Está seguro de eliminar el archivo?", callbackEliminar,"", "Confirmar", "Cancelar");
    });
    function callbackEliminar(){
         ajaxJson(site_url+"/servicios/upload_delete_evidencia", {"path_evidencia":$("#path_evidencia").val(),"factura_evidencia":$("#factura_evidencia").val()}, "POST", true, function(j){ 
             var j=$.parseJSON(j); 
            if(j.isok){
                ExitoCustom("Archivo eliminado correctamente");
                $("#files_evidencia").html("");
                $("#divfileupload").show();
                $("#evidencia").val('');
                $("#path_evidencia").val('');
            }
         });
    }