$("#enviarOrdenForm").on('click', function (e){
    //console.log("Se envia formulario");
    //Creamos visualmente el efecto de carga
    //Si se hizo bien la conexión, mostramos el simbolo de carga
    $("#cargaIcono").css("display","inline-block");
    //Aparentamos la carga de la tabla
    $("#formulario").css("background-color","#ddd");

    // Evitamos que salte el enlace.
    e.preventDefault(); 
    var validaciones = validar();
    if(validaciones){
        $('input').attr('disabled',false);
        
    	var paqueteDatos = new FormData(document.getElementById('formulario'));
        var orden = $("input[name='orden']").val();
    	var base = $("#sitio").val();

        $.ajax({
            url: base+"inventario/guardar_formulario",
            type: 'post',
            contentType: false,
            data: paqueteDatos,
            processData: false,
            cache: false,
            success:function(resp){
                console.log(resp);
                if (resp.indexOf("handler         </p>")<1) {
                    var respuesta = resp.split("_");
                    if (respuesta[0] == "OK") {
                        location.href = base+"index.php/inventario/generar_revision/"+respuesta[1];
                    } else {
                        $("#formulario_error").text(respuesta[0]);
                    }

                }else{
                    $("#formulario_error").text("SE PRODUJO UN ERROR AL ENVIAR LA INFORMACIÓN");
                }
                //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                $("#cargaIcono").css("display","none");
                //Regresamos los colores de la tabla a la normalidad
                $("#formulario").css("background-color","white");
            //Cierre de success
            },
              error:function(error){
                console.log(error);
                //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                $("#cargaIcono").css("display","none");
                //Regresamos los colores de la tabla a la normalidad
                $("#formulario").css("background-color","white");
                $("#formulario_error").text("SE PRODUJO UN ERROR EN EL ENVIO");
                //location.href = base+"index.php/inventario/index/"+orden;
            }
        });
    }else{
        //Si ya se termino de cargar la tabla, ocultamos el icono de carga
        $("#cargaIcono").css("display","none");
        //Regresamos los colores de la tabla a la normalidad
        $("#formulario").css("background-color","white");
    }
});

function validar() {
    var retorno = true;
    var tipo_formulario = $("input[name='envio_formulario']").val();
    
    if (tipo_formulario == "1") {
        var danos = $("input[name='marca_danos']").val();
        if (danos == "0") {
            $("#danosMarcas").val("statics/inventario/imgs/car.jpeg");
        }else{
            var canvas_diagrama = document.getElementById('canvas_3');
            var dataURL = canvas_diagrama.toDataURL('image/jpeg');
            $("#danosMarcas").val(dataURL);
        }
    }

    var campo_1 = $("input[name='orden']").val();
    if (campo_1 == "") {
        var error_1 = $("#orden_error");
        error_1.empty();
        $("#orden_error").css("display","inline-block");
        error_1.append("<br>");
        error_1.append('<label class="form-text text-danger">Campo incompleto</label>');
        retorno = false;
    }else{
        var error_1 = $("#orden_error");
        error_1.empty();
    }

    var check_inidcadores = $(".interiores:checked").length;

    //console.log("indicadores = "+check_inidcadores);
    if (check_inidcadores <= 20) {
        var error_8 = $("#interiores_error");
        error_8.empty();
        $("#interiores_error").css("display","inline-block");
        error_8.append("<br>");
        error_8.append('<label class="form-text text-danger">Faltan inidcadores que marcar</label>');
        retorno = false;
    }else{
        var error_8 = $("#interiores_error");
        error_8.empty();
    }

    var check_cajuela = $(".cajuela:checked").length;

    //console.log("cajuela = "+check_cajuela);
    if (check_cajuela <= 5) {
        var error_9 = $("#cajuela_error");
        error_9.empty();
        $("#cajuela_error").css("display","inline-block");
        error_9.append("<br>");
        error_9.append('<label class="form-text text-danger">Faltan inidcadores que marcar</label>');
        retorno = false;
    }else{
        var error_9 = $("#cajuela_error");
        error_9.empty();
    }

    var check_exteriores = $(".exteriores:checked").length;

    //console.log("exteriores = "+check_exteriores);
    if (check_exteriores <= 3) {
        var error_10 = $("#exteriores_error");
        error_10.empty();
        $("#exteriores_error").css("display","inline-block");
        error_10.append("<br>");
        error_10.append('<label class="form-text text-danger">Faltan inidcadores que marcar</label>');
        retorno = false;
    }else{
        var error_10 = $("#exteriores_error");
        error_10.empty();
    }

    var check_documentacion = $(".documentacion:checked").length;

    //console.log("documentacion = "+check_documentacion);
    if (check_documentacion <= 3) {
        var error_11 = $("#documentacion_error");
        error_11.empty();
        $("#documentacion_error").css("display","inline-block");
        error_11.append("<br>");
        error_11.append('<label class="form-text text-danger">Faltan inidcadores que marcar</label>');
        retorno = false;
    }else{
        var error_11 = $("#documentacion_error");
        error_11.empty();
    }

    var campo_12 = $("input[name='ruta_firma_asesor']").val();
    if (campo_12 == "") {
        var error_12 = $("#ruta_firma_asesor_error");
        error_12.empty();
        $("#ruta_firma_asesor_error").css("display","inline-block");
        error_12.append('<label class="form-text text-danger">Falta firma del asesor</label>');
        retorno = false;
    }else{
        var error_12 = $("#ruta_firma_asesor_error");
        error_12.empty();
    }

    var campo_13 = $("input[name='nombre_asesor']").val();
    if (campo_13 == "") {
        var error_13 = $("#nombre_asesor_error");
        error_13.empty();
        $("#nombre_asesor_error").css("display","inline-block");
        error_13.append('<label class="form-text text-danger">Falta nombre del asesor</label>');
        retorno = false;
    }else{
        var error_13 = $("#nombre_asesor_error");
        error_13.empty();
    }

    var campo_14 = $("input[name='nombre_cliente']").val();
    if (campo_14 == "") {
        var error_14 = $("#nombre_cliente_error");
        error_14.empty();
        $("#nombre_cliente_error").css("display","inline-block");
        error_14.append('<label class="form-text text-danger">Falta nombre del cliente</label>');
        retorno = false;
    }else{
        var error_14 = $("#nombre_cliente_error");
        error_14.empty();
    }

    var campo_15 = $("input[name='ruta_firma_cliente']").val();
    if (campo_15 == "") {
        var error_15 = $("#ruta_firma_cliente_error");
        error_15.empty();
        $("#ruta_firma_cliente_error").css("display","inline-block");
        error_15.append('<label class="form-text text-danger">Falta firma del cliente</label>');
        retorno = false;
    }else{
        var error_15 = $("#ruta_firma_cliente_error");
        error_15.empty();
    }

    var campo_a = $("input[name='ruta_firma_cliente_c1']").val();
    if (campo_a == "") {
        var error_a = $("#ruta_firma_cliente_c1_error");
        error_a.empty();
        $("#ruta_firma_cliente_c1_error").css("display","inline-block");
        error_a.append('<label class="form-text text-danger">Falta firma del cliente</label>');
        retorno = false;
    }else{
        var error_a = $("#ruta_firma_cliente_c1_error");
        error_a.empty();
    }

    var campo_b = $("input[name='ruta_ccfirma_asesor']").val();
    if (campo_b == "") {
        var error_b = $("#ruta_ccfirma_asesor_error");
        error_b.empty();
        $("#ruta_ccfirma_asesor_error").css("display","inline-block");
        error_b.append('<label class="form-text text-danger">Falta firma del asesor</label>');
        retorno = false;
    }else{
        var error_b = $("#ruta_ccfirma_asesor_error");
        error_b.empty();
    }

    /*var campo_a1 = $("input[name='cnombre_asesor']").val();
    if (campo_a1 == "") {
        var error_a1 = $("#cnombre_asesor_error");
        error_a1.empty();
        $("#cnombre_asesor_error").css("display","inline-block");
        error_a1.append("<br>");
        error_a1.append('<label class="form-text text-danger">Falta nombre del asesor</label>');
        retorno = false;
    }else{
        var error_a1 = $("#cnombre_asesor_error");
        error_a1.empty();
    }*/

    var campo_c = $("input[name='ruta_ccfirma_cliente']").val();
    if (campo_c == "") {
        var error_c = $("#ruta_firma_ccliente_error");
        error_c.empty();
        $("#ruta_firma_ccliente_error").css("display","inline-block");
        //error_c.append("<br>");
        error_c.append('<label class="form-text text-danger">Falta firma del cliente</label>');
        //retorno = false;
    }else{
        var error_c = $("#ruta_firma_ccliente_error");
        error_c.empty();
    } 

    return retorno;
}

/*function guardar_img() {
    console.log("entro a la funcion");
    var base = $("#sitio").val();
    var diagrama = $("#danosMarcas").val();
    var url_sis = base+"inventario/guardar_diagrama";
    $.ajax({
        url: url_sis,
        method: 'post',
        data: {
            diagrama : diagrama
        },
        success:function(resp){
            console.log(resp);
            if (resp.indexOf("handler           </p>")<1) {
                var respuesta = resp.split("=");
                if (respuesta[0] == "OK") {
                   $("#danosMarcas").val(respuesta[1]);
                } else {
                    $("#formulario_error").text("NO SE PUDO GUARDAR EL DIAGRAMA");
                }
            }
        //Cierre de success
        },
        error:function(error){
            console.log(error);
        //Cierre del error
        }
    //Cierre del ajax
    });
}*/