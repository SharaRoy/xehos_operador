$(document).ready(function() {
    var alto = screen.height;
    var ancho = screen.width;

    //Verificamos el tamaño de la pantalla
    if (ancho < 501) {
        //Calculamos las medidad de la pantalla
        var nvo_altura = alto * 0.20;
        var nvo_ancho = ancho * 0.80;
        console.log("Movil");
    }else if ((ancho > 499) && (ancho < 1030)) {
        //Calculamos las medidad de la pantalla
        var nvo_altura = (alto * 0.22)+10;
        var nvo_ancho = (ancho * 0.50)+10;
        console.log("Tablet");
    }else {
        //Calculamos las medidad de la pantalla
        var nvo_altura = alto * 0.22;
        var nvo_ancho = ancho * 0.33;
        console.log("PC");
    }

    // Asignamos los valores al recuadro de la firma
    $("#canvas").prop("width",nvo_ancho.toFixed(2));
    $("#canvas").prop("height","200");

    //Lienzo para firma general
    var signaturePad = new SignaturePad(document.getElementById('canvas'));

    $("#btnSign").on('click', function(){
        //Recuperamos la ruta de la imagen
        var data = signaturePad.toDataURL('image/png');
        //Comprobamos a donde se enviara
        var destino = $('#destinoFirma').val();
        //console.log(destino);
        // Comprobamos el tipo de usuario que firmara
        if (destino == "Asesor_2") {
            $('#ruta_firma_asesor').val(data);
            $("#firma_asesor_Img").attr("src",data);

        } else if (destino == "Cliente_2") {
            $('#ruta_firma_cliente').val(data);
            $("#firma_cliente_Img").attr("src",data);

        }else if (destino == "CCliente1") {
            $('#ruta_firma_cliente_c1').val(data);
            $("#firma_cliente_c1").attr("src",data);

        }else if (destino == "CAsesor1") {
            $('#ruta_ccfirma_asesor').val(data);
            $("#ccfirma_asesor_Img").attr("src",data);

        }else if (destino == "CCliente2") {
            $('#ruta_ccfirma_cliente').val(data);
            $("#ccfirma_cliente_Img").attr("src",data);
        }

        signaturePad.clear();
    });

    $('#limpiar').on('click', function(){
        signaturePad.clear();
    });

    $('#cerrarCuadroFirma').on('click', function(){
        signaturePad.clear();
    });
});

$(".cuadroFirma").on('click',function() {
    var firma = $(this).data("value");
    $('#destinoFirma').val(firma);
    //console.log(firma);
    // Comprobamos el tipo de usuario que firmara
    if ((firma == "Asesor_2")||(firma == "CAsesor1")) {
        //Cambiamos el encabezado del modal
        $("#firmaDigitalLabel").text("Firma del Operador.");
    } else if ((firma == "Cliente_2")||(firma == "CCliente1")||(firma == "CCliente2")) {
        //Cambiamos el encabezado del modal
        $("#firmaDigitalLabel").text("Firma del Cliente.");
    }

    //$(".modal-backdrop").css("z-index","-1");
});
