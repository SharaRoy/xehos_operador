 function grabar_movimiento() {
    if ("geolocation" in navigator){
        navigator.geolocation.getCurrentPosition(success,error);
    }
}

function success(position) {
    var indice = $("#paginado").val();
    var base = $("#sitio").val();
    var lat_3 = position.coords.latitude;
    var lng_3 = position.coords.longitude;
    console.log("coordenadas: "+lat_3+","+lng_3);
    var url_sis = base+"operadores/grabar_posicion";
    $.ajax({
        url: url_sis,
        method: 'post',
        data: {
            indice : indice,
            lat_3 : lat_3,
            lng_3 : lng_3
        },
        success:function(resp){
          console.log(resp);
            if (resp.indexOf("handler           </p>")<1) {

            }
        //Cierre de success
        },
        error:function(error){
            console.log(error);
        //Cierre del error
        }
    //Cierre del ajax
    });
}

function error() {
    console.log("Error permisos");
}

//Verificamos que sea una vista permitida
if ($('#paginado').length) {
    //Definimos el tiempo que se tardara en enviar la coordenada
    //Si esta en vistas principales o vistas que no estan relacionadas a un servicio, sera c/3 min
    if ($('#paginado').val() == "0") {
        setInterval('grabar_movimiento()',180000);
    //Si esta en una vista de un servicio, dependera si el servico esta activo o no
    } else {
        if ($("input[name='paso_evidencia']").length) {
            if ($("input[name='paso_evidencia']").val() != "6") {
                setInterval('grabar_movimiento()',5000); 
            } else {
                setInterval('grabar_movimiento()',10000);    
            }
        } else {
            setInterval('grabar_movimiento()',60000);
        }   
    }
} 
