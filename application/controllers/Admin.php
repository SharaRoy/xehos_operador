<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Admin extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mgeneral', '', TRUE);
        $this->load->library(array('session'));
        $this->load->helper(array('validation', 'url'));
        if (!$this->session->userdata('id') || !PermisoAccion('editar_fecha_servicio')) {
            redirect('login/');
        }
    }
    public function v_horarios()
    {
        //Obtener la última fecha
        $q = $this->db->select('fecha')->order_by('fecha', 'desc')->limit(1)->get('horarios_lavadores');
        if ($q->num_rows() == 1) {
            $nuevafecha = strtotime('+1 day', strtotime($q->row()->fecha));
            $fecha = date('Y-m-d', $nuevafecha);
        } else {
            $nuevafecha = strtotime('+1 day', strtotime(date('Y-m-d')));
            $fecha = date('Y-m-d', $nuevafecha);
        }
        $data['fecha_apartir'] = $fecha;
        $this->blade->render('admin/generar_horarios', $data);
    }
    public function generarhorariosAux()
    {
        ini_set('max_execution_time', 300);

        if ($this->input->post('fecha_apartir') >= date2sql($this->input->post('fecha_hasta'))) {
            echo -1;
            die();
        }
        $lavadores = $this->db->get('lavadores')->result();
        $random = random(10);
        $contador = 0;
        foreach ($lavadores as $key => $value) {
            $contador++;
            $fecha = $this->input->post('fecha_apartir');
            while ($fecha < date2sql($this->input->post('fecha_hasta'))) {
                $time = '08:00';
                $contador = 0;
                while ($contador < 11) {
                    $fecha_parcial = explode('-', $fecha);
                    $aux = array(
                        'hora' => $time,
                        'id_lavador' => $value->lavadorId,
                        'fecha_creacion' => date('Y-m-d'),
                        'dia' => $fecha_parcial[2],
                        'mes' => $fecha_parcial[1],
                        'anio' => $fecha_parcial[0],
                        'fecha' => $fecha,
                        'ocupado' => 0,
                        'activo' => 1,
                        'random' => $random

                    );
                    $timestamp = strtotime($time) + 60 * 60;
                    $time = date('H:i', $timestamp);
                    $contador++;
                    $this->db->insert('horarios_lavadores', $aux);
                }
                $nuevafecha = strtotime('+1 day', strtotime($fecha));
                $fecha = date('Y-m-d', $nuevafecha);
            }
        }

        echo $contador;
    }
}
