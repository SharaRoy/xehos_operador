<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Login extends MX_Controller
{

    /**

     **/
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mgeneral', 'consulta', TRUE);
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'companies', 'url'));
        $this->load->library('form_validation');
        date_default_timezone_set(CONST_TIMEZONE);
    }

    public function index()
    {
        $this->load->view('login/login', '', FALSE); 
    }
    
    public function iniciar_sesion()
    {
        $telefono = $_POST["recordUsername"];
        $pass = $_POST["recordPassword"];

        $usuario_data = $this->consulta->get_result_field("lavadorTelefono",$telefono,"activo","1","lavadores");
        foreach ($usuario_data as $row){
            $pass_db = $row->lavadorPassword;
            $usuario = $row->lavadorNombre;
            $id_usuario = $row->lavadorId;
            $telefono = $row->lavadorTelefono;
            $sucursal = $row->id_sucursal;
        }
  
        if (isset($pass_db)) {
            if ((md5($pass) === $pass_db)||($pass === $pass_db)) { 
                $array_session = array(
                    'id_usuario'=>$id_usuario,
                    'usuario'=> $usuario,
                    'telefono'=> $telefono,
                    'rol_usuario' => "lavador",
                    'id_sucursal' => $sucursal
                );
                
                $this->session->set_userdata($array_session);

                if($this->session->userdata('id_usuario')){
                    echo 'OK'; 
                }else{
                    echo "Reintentar";
                }
            } else { 
              echo 'Usuario o Contraseña incorrectos'; 
            } 
        } else {
            echo 'El usuario no existe o no ha sido activado'; 
        } 
    }

    public function cerrar_sesion()
    {
        $this->session->unset_userdata('id_usuario');
        $this->session->sess_destroy();
        redirect('login');
    }
}
