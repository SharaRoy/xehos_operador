<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**

 **/
class Api extends MX_Controller
{

  /**

   **/
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mgeneral', '', TRUE);
    $this->load->model('M_transiciones', 'mt', TRUE);
    $this->load->model('servicios/M_servicios', 'ms', TRUE);
    $this->load->library(array('session'));
    $this->load->helper(array('form', 'html', 'validation', 'url'));

    date_default_timezone_set(CONST_TIMEZONE);
  }

  public function registro()
  {
    //validar si ya existe el correo
    $existe_usuario = $this->ms->existeUsuario();
    if ($existe_usuario) {
      echo json_encode(array('exito' => false, 'mensaje' => 'El usuario ya existe'));
      return;
    }
    $this->ms->guardarUsuario();
    $usuario = $this->Mgeneral->get_row('usuario_Id', $this->db->insert_id(), 'usuarios');
    echo json_encode(array('exito' => true, 'usuario' => $usuario));
  }

  public function login()
  {
    $email = $this->input->post('email');
    $password = $this->input->post('password');
    $this->db->where('usuarioEmail', $email);
    $this->db->where('usuarioPassword', $password);
    $data = $this->db->get('usuarios')->row();
    $response = [];
    if (is_object($data)) {
      $response['error'] = 0;
      $response['id'] = $data->usuarioID;
      $response['mensaje'] = "";
    } else {
      $response['error'] = 1;
      $response['mensaje'] = "Usuario o contraseña incorrectos";
    }
    echo json_encode($response);
  }

  public function prueba_notificacion($id_usuario)
  {

    $token = $this->Mapi->get_row('usuarioID', $id_usuario, 'usuarios');
    $this->load->library('Firebase');
    $firebase = new Firebase();
    $res = array();
    $res['data']['mensaje'] = "mensaje";
    $res['data']['id_servicio'] = "";
    $res['data']['foto'] = "";


    $response4 = $firebase->send($token->token, $res);

    var_dump($response4);
  }

  public function servicios()
  {
    $res = $this->Mgeneral->get_table('servicios');
    echo json_encode($res);
  }

  public function get_puntos()
  {
    $id_usuario = $this->input->post('id_usuario');
    $res = $this->Mgeneral->get_row('usuarioID', $id_usuario, 'usuarios');
    $data = [];
    if (is_object($res)) {
      $puntos = $res->usuarioPuntos;
    } else {
      $puntos = 0;
    }

    echo json_encode(array('exito' => true, 'puntos' => $puntos));
  }

  // public function get_horarios()
  // {
  //   $fecha = $this->input->post("fecha");
  //   $res = $this->db->like('date', $fecha)->get('appointment')->result();
  //   echo json_encode($res);
  // }
  public function registro_auto()
  {
    $this->ms->guardarAuto(0);
    $auto = $this->Mgeneral->get_row('id', $this->db->insert_id(), 'autos');
    echo json_encode(array('exito' => true, 'auto' => $auto));
  }
  public function estados()
  {
    $res = $this->Mgeneral->get_table('estados');
    echo json_encode($res);
  }
  public function municipios($id_municipio)
  {
    $res = $this->ms->getMunicipiosByEstado($id_municipio);
    echo json_encode($res);
  }
  //Obtiene los colores
  public function colores()
  {
    $res = $this->Mgeneral->get_table('cat_colores');
    echo json_encode($res);
  }
  //Obtiene las marcas
  public function marcas()
  {
    $res = $this->Mgeneral->get_table('cat_marcas');
    echo json_encode($res);
  }
  //Obtiene los modelos
  public function modelos($idmarca)
  {
    $res = $this->ms->getModelosByMarca($idmarca);
    echo json_encode($res);
  }
  //crear tabla y CRUD
  public function tipo_auto()
  {
    $data = $this->db->get('cat_tipo_autos')->result();
    echo json_encode($data);
  }

  public function indica_ubicacion()
  {
    $data = [
      'id_usuario' => $this->input->post('id_usuario'),
      'numero_int' => $this->input->post('numero_int'),
      'numero_ext' => $this->input->post('numero_ext'),
      'colonia' => $this->input->post('colonia'),
      'id_estado' => $this->input->post('id_estado'),
      'id_municipio' => $this->input->post('id_municipio'),
      'latitud' => $this->input->post('latitud'),
      'longitud' => $this->input->post('longitud'),
      'created_at' => date('Y-m-d H:i:s')
    ];
    $this->Mgeneral->save_register('ubicacion_usuario', $data);

    $ubicacion = $this->Mgeneral->get_row('id', $this->db->insert_id(), 'ubicacion_usuario');
    echo json_encode(array('exito' => true, 'ubicacion' => $ubicacion));
  }
  // obtener los carros de un usuario
  public function getAutosByUsuario($idusuario)
  {
    $autos = $this->db->where('autoIdUsuario', $idusuario)->get('autos')->result();
    echo json_encode($autos);
  }
  //Obtener los horarios disponibles del día
  public function getHorariosDisponibles()
  {
    $horarios = $this->ms->getHorariosDisponibles($_POST['fecha']);
    echo json_encode($horarios);
  }
  //Guardar todo el servicio
  public function saveService()
  {
    $horario_lavador = $this->ms->getIdLavador();
    if (count($horario_lavador) == 0) {
      echo json_encode(array('exito' => false, 'mensaje' => 'El horario ya está ocupado'));
      return;
    }
    $horario_lavador = $horario_lavador[0];
    $cupon = '';
    //Validar cupón si existe
    if (isset($_POST['cupon'])) {
      $validar_cupon = $this->ms->validarCupon($_POST['cupon']);
      if (!$validar_cupon) {
        echo json_encode(array('exito' => false, 'mensaje' => 'El cupón no es válido'));
        return;
      }
      $cupon = $validar_cupon->cupon;
      //Desactivar cupón
      $this->db->where('id', $validar_cupon->id)->set('activo', 0)->update('cupones');
    }
    //Guardar ubicacion 
    $this->ms->guardarUbicacion(0, $_POST['id_usuario']);
    $id_ubicacion = $this->db->insert_id();

    $servicio = [
      'id_usuario' => $_POST['id_usuario'],
      'id_auto' => $_POST['id_auto'],
      'id_lavador' => $horario_lavador->id_lavador,
      'id_servicio' => $_POST['id_servicio'],
      'puntos' => $_POST['puntos'],
      'comentarios' => $_POST['comentarios'],
      'calificacion' => $_POST['calificacion'],
      'created_at' => date('Y-m-d H:i:s'),
      'status' => 1,
      'latitud_lavador' => $_POST['latitud_lavador'],
      'longitud_lavador' => $_POST['longitud_lavador'],
      'id_horario' => $horario_lavador->id,
      'id_ubicacion' => $id_ubicacion,
      'nombre_cliente' => isset($_POST['nombre_cliente']) ? $_POST['nombre_cliente'] : '',
      'cupon' => $cupon,
      'origen' => 2
    ];
    $lavador = $this->db->select('lavadorNombre,lavadorFoto,lavadorEmail,qr,id_id')->where('lavadorId', $horario_lavador->id_lavador)->get('lavadores')->row();
    $ubicacion = $this->db->where('id', $id_ubicacion)->get('ubicacion_usuario')->row();
    $this->db->insert('servicio_lavado', $servicio);
    $this->db->where('id', $horario_lavador->id)->set('ocupado', 1)->update('horarios_lavadores');
    echo json_encode(array('exito' => true, 'servicio' => $servicio, 'horario' => $horario_lavador, 'lavador' => $lavador, 'ubicacion' => $ubicacion));
  }
  //saber si ya está ocupado un horario
  public function validarHorario($fecha, $hora, $id_lavador)
  {
    $q = $this->db->where('fecha', $fecha)
      ->where('hora', $hora)
      ->where('hora', $id_lavador)
      ->where('activo', 1)
      ->where('ocupado', 0)
      ->limit(1)
      ->get('horarios_lavadores')
      ->result();
    return $q->num_rows();
  }
  public function cambiarStatus()
  {
    if ($_POST['id_status_actual'] == $_POST['id_status_nuevo']) {
      echo json_encode((array('exito' => false, "mensaje" => "Los estatus no deben ser iguales")));
      return;
    }
    //validar servicio
    $q = $this->db->where('id', $_POST['id_servicio'])->limit(1)->get('servicio_lavado');
    if ($q->num_rows() == 0) {
      echo json_encode((array('exito' => false, "mensaje" => "El servicio no existe")));
      return;
    }
    $this->mt->insertTransicion();
    echo json_encode((array('exito' => true, "mensaje" => "Estatus cambiado correctamente")));
  }
  public function get_status_lavado()
  {
    $data = $this->db->where('activo', 1)->get('cat_estatus_lavado')->result();
    echo json_encode($data);
  }
}
