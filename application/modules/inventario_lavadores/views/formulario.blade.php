@layout('layout')

@section('included_css')
    <!-- inventario -->
    <link rel="stylesheet"
        href="<?php echo base_url(); ?>statics/css/css_inventario.css">
    <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">--> 
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css">

    <style type="text/css">
        #cargaIcono {
            /*-webkit-animation: rotation 1s infinite linear;*/
            font-size: 55px !important;
            color: darkblue;
            display: none;
        }
    </style>
@endsection

@section('contenido')
	<form id="formulario" method="post" action="" autocomplete="on" enctype="multipart/form-data">
		<div class="row">
            <div class="col-sm-12">
                <div class="panel-body titulo_pdf" align="center" style="margin-left: 15px; margin-right: 15px;">
                    <h5>DIAGNÓSTICO MOTOCICLETA </h5>
                </div>
                <div class="panel-body">
                    <div class="col-sm-12 table-responsive" style="width: 100%;" align="center">
                        <table style="background-color: white; width: 100%;">
                            <tr>
                                <td colspan="2">
                                    <h4 style="text-align: left;">
                                        Lavador:
                                        <span style="color:darkblue;"><?php if(isset($n_lavador)) echo $n_lavador; ?></span>
                                    </h4>
                                    <input type="hidden" name="n_lavador" value="<?php if(isset($n_lavador)) echo $n_lavador; ?>">
                                    <br>
                                </td>
                            </tr>
                            @if (isset($id))
                                <tr>
                                    <td colspan="2">
                                        <h4 style="text-align: left;">
                                            Revisado por:
                                            <span style="color:darkblue;"><?php if(isset($supervisor)) echo $n_supervisor; else echo "Diagnóstico sin revisar"?></span>
                                        </h4>
                                        <input type="hidden" name="supervisor" value="<?php if(isset($supervisor)) echo $supervisor; else echo ''; ?>">
                                        <input type="hidden" name="n_supervisor" value="<?php if(isset($n_supervisor)) echo $n_supervisor; else echo ''; ?>">
                                        <br>
                                    </td>
                                </tr>
                            @endif
                            <tr>
                                <td style="width: 50%;">
                                    <h6 style="text-align: left;">
                                        Unidad:
                                        <span style="color:darkblue;"><?php if(isset($unidad)) echo $unidad; ?></span>
                                    </h6>
                                </td>
                                <td style="width: 50%;">
                                    <h6 style="text-align: left;">
                                        Marca:
                                        <span style="color:darkblue;"><?php if(isset($marca)) echo $marca; ?></span>
                                    </h6>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 50%;">
                                    <h6 style="text-align: left;">
                                        Placas:
                                        <span style="color:darkblue;"><?php if(isset($placas)) echo $placas; ?></span>
                                    </h6>
                                </td>
                                <td style="width: 50%;">
                                    <h6 style="text-align: left;">
                                        Serie:
                                        <span style="color:darkblue;"><?php if(isset($placas)) echo $placas; ?></span>
                                    </h6>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 50%;">
                                    <h6 style="text-align: left;">
                                        Color:
                                        <span style="color:darkblue;"><?php if(isset($color)) echo $color; ?></span>
                                    </h6>
                                </td>
                                <td style="width: 50%;">
                                    <h6 style="text-align: left;">
                                        Marca:
                                        <span style="color:darkblue;"><?php if(isset($anio)) echo $anio; ?></span>
                                    </h6>
                                </td>
                            </tr>
                        </table>
                    </div>

                    <br><hr>
                    <div class="col-sm-12 table-responsive" style="width: 100%;" align="center">
                        <table class="table table-bordered" style="background-color: white;">
                            <tr>
                                <td style="width: 100%;">
                                    Si&nbsp;&nbsp;
                                    <input type="radio" class="danos" style="transform: scale(1.5);" value="1" name="danos" <?php if(isset($marca_danos)){ if($marca_danos == "1") echo "checked";}else { echo 'checked'; }?> <?= ((isset($id) ? 'disabled' : '')) ?>>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    No&nbsp;&nbsp;
                                    <input type="radio" class="danos" style="transform: scale(1.5);" value="0" name="danos" <?php if(isset($marca_danos)) if($marca_danos == "0") echo "checked";?> <?= ((isset($id) ? 'disabled' : '')) ?>>

                                    <input type="hidden" name="marca_danos" value="<?php if(isset($marca_danos)) echo $marca_danos; else echo '1'?>">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100%;">
                                    <input type="radio" style="transform: scale(1.5);" id="golpes" name="marcasRadio" checked value="hit" <?= ((isset($id) ? 'disabled' : '')) ?>>
                                    &nbsp;&nbsp;
                                    Golpes
                                    &nbsp;&nbsp;
                                    <input type="radio" style="transform: scale(1.5);" id="roto" name="marcasRadio" value="broken" <?= ((isset($id) ? 'disabled' : '')) ?>>
                                    &nbsp;&nbsp;
                                    Roto / Estrellado
                                    &nbsp;&nbsp;
                                    <input type="radio" style="transform: scale(1.5);" id="rayones" name="marcasRadio" value="scratch" <?= ((isset($id) ? 'disabled' : '')) ?>>
                                    &nbsp;&nbsp;
                                    Rayones
                                </td>
                            <tr>
                                <td align="center" style="width: 100%;">
                                	<img src="<?php if(isset($danosMarcas)) { echo (($danosMarcas != "") ? $danosMarcas : base_url().'statics/inventario/imgs/diagrama_moto.jpeg'); } else{ echo base_url().'statics/inventario/imgs/diagrama_moto.jpeg'; } ?>" style="width:510px; height:500px;" <?php if(!isset($danosMarcas)) echo 'hidden'; ?>>

                                	<canvas id="canvas_3" width="510" height="500" <?php if(isset($danosMarcas)) echo 'hidden'; ?>>
		                                Su navegador no soporta canvas.
		                            </canvas>
                                </td>
                            </tr>
                        </table> 

                        <input type="hidden" name="" id="direccionFondo" value="<?php echo base_url().'statics/inventario/imgs/diagrama_moto.jpeg'; ?>">
                        <input type="hidden" name="danosMarcas" id="danosMarcas" value="<?php if(isset($danosMarcas)) echo $danosMarcas; ?>">
                    </div>

                    <div class="col-sm-12 table-responsive" align="center">
                        <br>
                        <a href="<?php if(isset($danosMarcas)) echo base_url().$danosMarcas; else echo '#'; ?>" class="btn btn-info" id="btn-download" download="Diagrama_Diagnostico" style="font-size: 12px !important;">
                            <i class="fa fa-download" aria-hidden="true"></i>
                            Descargar diagrama
                        </a>

                        @if(!isset($id))
                            <button type="button" class="btn btn-primary" id="resetoeDiagrama" style="margin-left: 20px;" <?php if(isset($danosMarcas)) echo 'hidden'; ?> >
                                <i class="fa fa-refresh" aria-hidden="true"></i>
                                Restaurar diagrama
                            </button>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <br><br>
        <div class="row">
            <div class="col-sm-6 table-responsive" align="center">
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <td colspan="2" align="center">
                                <h5>Nivel de Gasolina:</h5>
                                <img src="<?php echo base_url();?>statics/inventario/imgs/gasolina_a.png" style="height: 5cm;" id="nivel_gasolina_img">

                                @if(!isset($id))
                                    <br>
                                    <div id="slider-2"style="width:200px;"></div>
                                @endif
                                
                                <input type="hidden" name="nivel_gasolina" style="width:1.5cm;" value="<?php if(isset($nivel_gasolina)) echo $nivel_gasolina; else echo "0"; ?>">
                                <br>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div id="nivel_gasolina_error"></div>
            </div>
            <div class="col-sm-6" style="background-color: white;" align="">
                <br>
                <h5>Observaciones Lavador</h5>
                <textarea class="form-control" name="notas_lavador" rows="2" <?= ((isset($id) ? 'disabled' : '')) ?>><?php if(isset($notas_lavador)) echo $notas_lavador; else echo ''; ?></textarea>
                <br>
            </div>
        </div> 

        <br>
        <div class="row">
            <div class="col-sm-3">
                <h5>Checklist suministros:</h5>
            </div>
            <div class="col-sm-7">
                @if($checklist != NULL)
                    <table>
                        @foreach ($checklist as $serv)
                            <tr>
                                <td style="width: 1cm;">
                                    <input type="checkbox" class="form-control topicos" name="topicos[]" style="transform: scale(1.5);" value="{{ $serv->id}}" <?php if(isset($cheking)) {if(in_array($serv->id, $cheking)) echo 'checked';} ?> >        
                                </td>
                                <td style="vertical-align: middle;width: auto;">
                                    &nbsp;&nbsp; <label for="">{{ $serv->nombre}}</label>   
                                </td>
                            </tr>
                        @endforeach
                    </table>
                @endif
                <div id="topicos_error"></div>
            </div>
        </div>        

        <br>
        <div class="row">
            <div class="col-sm-12" style="background-color: white;" align="right">
                <br>
                <h5>Evidencias</h5>
                <input type="file" class="form-control" name="archivo[]" value="" multiple>
                <br>
                
                <?php if (isset($evidencias)): ?>
                    <?php for ($x = 0 ; $x < count($evidencias) ; $x++): ?>
                        <a href="<?php echo base_url().$evidencias[$x]; ?>" class="btn" style="background-color: #47bba0; color:white;font-size: 10px;" target="_blank">Archivo (<?php echo $x+1; ?>)</a>
                    <?php endfor ?>
                <?php endif ?>
            </div>
        </div>

        <br><br>
        <div class="row">
            <div class="col-sm-<?= ((isset($id)) ? '6' : '12') ?>" align="center">
                <img class="marcoImg" src="<?php if(isset($firma_lavador)) { echo (($firma_lavador != "") ? $firma_lavador : base_url().'statics/inventario/imgs/fondo_bco.jpeg'); } else{ echo base_url().'statics/inventario/imgs/fondo_bco.jpeg'; } ?>" id="firma_lavador_url" style="width: 2.5cm;">

                <input type="hidden" id="ruta_firma_lavador" name="ruta_firma_lavador" value="<?php if(isset($firma_lavador)) echo $firma_lavador;?>">

                <h6 id="h_cliente" style="text-align: center;">
                    <?php if(isset($n_lavador)) echo $n_lavador;?>
                </h6>

                @if(!isset($id))
                    <br>
                    <a class="cuadroFirma btn btn-primary" data-value="lavador" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firma del lavador </a>
                @endif
                <div id="ruta_firma_lavador_error"></div>
            </div>

            @if(isset($id))
                <div class="col-sm-6" align="center">
                    <img class="marcoImg" src="<?php if(isset($firma_supervisor)) { echo (($firma_supervisor != "") ? $firma_supervisor : base_url().'statics/inventario/imgs/fondo_bco.jpeg'); } else{ echo base_url().'statics/inventario/imgs/fondo_bco.jpeg'; } ?>" id="firma_supervisor_url" style="width: 2.5cm;">

                    <input type="hidden" id="ruta_firma_supervisor" name="ruta_firma_supervisor" value="<?php if(isset($firma_supervisor)) echo $firma_supervisor;?>">

                    <h6 id="h_cliente" style="text-align: center;">
                        <?php if(isset($n_supervisor)) echo $n_supervisor;?>
                    </h6>

                    @if($firma_supervisor == "")
                        <br>
                        <a class="cuadroFirma btn btn-primary" data-value="supervisor" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firma del supervisor </a>
                    @endif
                    <div id="ruta_firma_supervisor_error"></div>

                    <hr>
                    <h5>Observaciones Revisión</h5>
                    <textarea class="form-control" name="notas_supervisor" rows="2"><?php if(isset($notas_supervisor)) echo $notas_supervisor; else echo ''; ?></textarea>
                </div>
            @endif
        </div>

        <div class="row">
            <div class="col-sm-12" style="background-color: white;" align="center">
                <br>
                <i id="cargaIcono" class="fa fa-spinner cargaIcono fa-spin"></i>
                <br>
                <h5 class="error" id="formulario_error"></h5>
                <?php if (isset($id)): ?>
                    <input type="hidden" name="envio_formulario" value="2">
                    <input type="button" class="btn btn-success" name="envio_form" id="envio_form" value="ACTUALIZAR">
                <?php else: ?>
                    <input type="hidden" name="envio_formulario" value="1">
                    <input type="button" class="btn btn-success" name="envio_form" id="envio_form" value="GUARDAR">
                <?php endif ?>
                <input type="hidden" readonly="" id="sitio" value="{{base_url()}}">
                <input type="hidden" name="inventario" value="<?php if(isset($id)) echo $id; else echo '0'; ?>">
                <input type="hidden" name="lavador" value="<?php if(isset($lavador)) echo $lavador; else echo '0'; ?>">
                <input type="hidden" name="servicio" value="<?php if(isset($n_unidad)) echo $n_unidad; else echo '0'; ?>">
                <br>    
            </div>
        </div>
	</form>
	
	<!-- Modal para la firma del quien elaboro el diagnóstico-->
	<div class="modal fade" id="firmaDigital" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index: 30000;">
        <div class="modal-dialog modal-lg" role="document" style="z-index: 30001;">
            <div class="modal-content">
	            <div class="modal-header">
	                <h5 class="modal-title" id="firmaDigitalLabel"></h5>
	            </div>
	            <div class="modal-body" align="center">
	                <div class="signatureparent_cont_1">
	                    <canvas id="canvas" width="430" height="200" style='border: 1px solid #CCC;'>
	                        Su navegador no soporta canvas
	                    </canvas>
	                </div>
	            </div>
	            <div class="modal-footer">
	                <input type="hidden" name="" id="destinoFirma" value="">
	                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
	                <button type="button" class="btn btn-info" id="limpiar">Limpiar firma</button>
	                <button type="button" class="btn btn-primary" name="btnSign" id="btnSign" data-dismiss="modal">Aceptar</button>
	            </div>
	        </div>
	    </div>
	</div>

    <!--<input type="hidden" id="paginado" value="<?= ((isset($servicioId) ? $servicioId : "0")) ?>">-->
@endsection
@section('included_js')
    @include('main/scripts_dt')
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
	<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

	<script src="https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js"></script>

    <script src="{{ base_url('statics/js/inventario_lavadores/envio_formulario.js') }}"></script>
    <script src="{{ base_url('statics/js/inventario_lavadores/diagrama_danos.js') }}"></script>
    <script src="{{ base_url('statics/js/inventario_lavadores/firmas.js') }}"></script>

    <script type="text/javascript">
        var site_url = "{{site_url()}}";
        var notnotification = true;
    </script>
    
    <script type="text/javascript">

        if ($("#ruta_firma_supervisor").length) {
            if ($("#ruta_firma_supervisor").val() != "") {
                $('input').attr('disabled',true);
                $('textarea').attr('disabled',true);
                $("#envio_form").css("display","none");
            }
        }
            
    </script>
@endsection