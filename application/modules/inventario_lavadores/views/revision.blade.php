@layout('layout')

@section('included_css')
    <!-- inventario -->
    <link rel="stylesheet"
        href="<?php echo base_url(); ?>statics/css/css_inventario.css">
    <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">--> 
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css">
@endsection

@section('contenido')
    <div class="row">
        <div class="col-sm-12">
            <table class="cabecera" style="width:100%;">
                <tr>
                    <td align="center">
                        <img src="<?= base_url(); ?>statics/inventario/imgs/logos/xehos.png" class="logo" style="height:2cm;">
                    </td>
                    <td colspan="2" align="center">
                        <h4>
                            SISTEMAS OPERATIVOS HEXADECIMAL. S.A. DE C.V. 
                        </h4>

                        <p class="justify" style="text-align: center;font-size: 13px;">
                            AV. IGNACIO SANDOVAL, #1939. LOC L6. PLAZA MILAN. <br>
                            COL. PASEO DE LA CANTERA. COLIMA COLIMA. C.P. 28017.<br>
                            E-mail: info@xehos.com RFC: SOH190520AR4 <br>  Tels. 33 3835 2043 / 33 3835 2058  <br>
                            Horarios de Servicio: Lunes a Domingo de 8:00 AM a 6:00PM
                        </p>
                    </td>
                    <td width="20%" align="" style="padding-left:10px;" rowspan="2" align="center">
                        <img src="<?php echo base_url(); ?>statics/inventario/imgs/logos/logoSohex.png" class="logo" style="width: 6.5cm;">
                    </td>
                </tr>
            </table>
        </div>
    </div>


    <br>
    <div class="row" style="margin-right: 15px;margin-left: 15px;">
        <div class="col-sm-12 table-responsive tabla_info" style="width: 100%;background-color: #dbf4b9;padding: 10px;" align="center">
            <table style="background-color: white; width: 100%;">
                <tr>
                    <td colspan="2">
                        <h4 style="text-align: left;">
                            Lavador:
                            <span style="color:darkblue;"><?php if(isset($n_lavador)) echo $n_lavador; ?></span>
                        </h4>
                        <input type="hidden" name="n_lavador" value="<?php if(isset($n_lavador)) echo $n_lavador; ?>">
                        <br>
                    </td>
                </tr>
                @if (isset($id))
                    <tr>
                        <td colspan="2">
                            <h4 style="text-align: left;">
                                Revisado por:
                                <span style="color:darkblue;"><?php if(isset($supervisor)) echo $n_supervisor; else echo "Diagnóstico sin revisar"?></span>
                            </h4>
                            <input type="hidden" name="supervisor" value="<?php if(isset($supervisor)) echo $supervisor; else echo ''; ?>">
                            <input type="hidden" name="n_supervisor" value="<?php if(isset($n_supervisor)) echo $n_supervisor; else echo ''; ?>">
                            <br>
                        </td>
                    </tr>
                @endif
                <tr>
                    <td style="width: 50%;">
                        <h6 style="text-align: left;">
                            Unidad:
                            <span style="color:darkblue;"><?php if(isset($unidad)) echo $unidad; ?></span>
                        </h6>
                    </td>
                    <td style="width: 50%;">
                        <h6 style="text-align: left;">
                            Marca:
                            <span style="color:darkblue;"><?php if(isset($marca)) echo $marca; ?></span>
                        </h6>
                    </td>
                </tr>
                <tr>
                    <td style="width: 50%;">
                        <h6 style="text-align: left;">
                            Placas:
                            <span style="color:darkblue;"><?php if(isset($placas)) echo $placas; ?></span>
                        </h6>
                    </td>
                    <td style="width: 50%;">
                        <h6 style="text-align: left;">
                            Serie:
                            <span style="color:darkblue;"><?php if(isset($placas)) echo $placas; ?></span>
                        </h6>
                    </td>
                </tr>
                <tr>
                    <td style="width: 50%;">
                        <h6 style="text-align: left;">
                            Color:
                            <span style="color:darkblue;"><?php if(isset($color)) echo $color; ?></span>
                        </h6>
                    </td>
                    <td style="width: 50%;">
                        <h6 style="text-align: left;">
                            Marca:
                            <span style="color:darkblue;"><?php if(isset($anio)) echo $anio; ?></span>
                        </h6>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <br>
    <div class="row" style="margin-right: 15px;margin-left: 15px;">
        <div class="panel-body titulo_pdf" align="center" style="width: 100%;">
            <h5>DIAGNÓSTICO MOTOCICLETA </h5>
        </div>
        <div class="table-responsive tabla_info" style="width: 100%;background-color: #dbf4b9;padding: 10px;" align="center">
            <table style="width: 100%;background-color: white;">
                <tr>
                    <td colspan="2" align="center">
                        <img src="<?php echo base_url().'statics/inventario/imgs/carroceria.png'; ?>" style="height:60px;width:295px;">

                        <br>
                        <?php if (isset($danosMarcas)): ?>
                            <?php if ($danosMarcas != ""): ?>
                                <img src="<?= $danosMarcas ?>" style="width:355px;margin-top: 20px;"> 
                            <?php else: ?> 
                                <img src="<?= base_url().'statics/inventario/imgs/diagrama_moto.jpeg'; ?>" style="width:355px;margin-top: 20px;">
                            <?php endif; ?>
                        <?php else: ?>
                            <img src="<?php echo base_url().'statics/inventario/imgs/diagrama_moto.jpeg'; ?>" style="width:355px;margin-top: 20px;">
                        <?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="right">
                        <br><hr>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <h4 style="color: #52bf66;font-weight: bold;">Nivel de Gasolina: </h4>
                        <div style="width: 100%;" align="center">
                            <?php if (isset($nivel_gasolina)): ?>
                                <?php if ($nivel_gasolina ==  "0"): ?>
                                    <img src="<?php echo base_url().'statics/inventario/imgs/gasolina_a.png'; ?>" style="height: 5cm;">
                                <?php elseif ($nivel_gasolina ==  "1"): ?>
                                    <img src="<?php echo base_url().'statics/inventario/imgs/gasolina_b.png'; ?>" style="height: 5cm;">
                                <?php elseif ($nivel_gasolina ==  "2"): ?>
                                    <img src="<?php echo base_url().'statics/inventario/imgs/gasolina_c.png'; ?>" style="height: 5cm;">
                                <?php elseif ($nivel_gasolina ==  "3"): ?>
                                    <img src="<?php echo base_url().'statics/inventario/imgs/gasolina_d.png'; ?>" style="height: 5cm;">
                                <?php elseif ($nivel_gasolina ==  "4"): ?>
                                    <img src="<?php echo base_url().'statics/inventario/imgs/gasolina_e.png'; ?>" style="height: 5cm;">
                                <?php elseif ($nivel_gasolina ==  "5"): ?>
                                    <img src="<?php echo base_url().'statics/inventario/imgs/gasolina_f.png'; ?>" style="height: 5cm;">
                                <?php elseif ($nivel_gasolina ==  "6"): ?>
                                    <img src="<?php echo base_url().'statics/inventario/imgs/gasolina_g.png'; ?>" style="height: 5cm;">
                                <?php elseif ($nivel_gasolina ==  "7"): ?>
                                    <img src="<?php echo base_url().'statics/inventario/imgs/gasolina_h.png'; ?>" style="height: 5cm;">
                                <?php elseif ($nivel_gasolina ==  "8"): ?>
                                    <img src="<?php echo base_url().'statics/inventario/imgs/gasolina_i.png'; ?>" style="height: 5cm;">
                                <?php elseif ($nivel_gasolina ==  "9"): ?>
                                    <img src="<?php echo base_url().'statics/inventario/imgs/gasolina_j.png'; ?>" style="height: 5cm;">
                                <?php elseif ($nivel_gasolina ==  "10"): ?>
                                    <img src="<?php echo base_url().'statics/inventario/imgs/gasolina_k.png'; ?>" style="height: 5cm;">
                                <?php elseif ($nivel_gasolina ==  "11"): ?>
                                    <img src="<?php echo base_url().'statics/inventario/imgs/gasolina_l.png'; ?>" style="height: 5cm;">
                                <?php elseif ($nivel_gasolina ==  "12"): ?>
                                    <img src="<?php echo base_url().'statics/inventario/imgs/gasolina_m.png'; ?>" style="height: 5cm;">
                                <?php elseif ($nivel_gasolina ==  "13"): ?>
                                    <img src="<?php echo base_url().'statics/inventario/imgs/gasolina_n.png'; ?>" style="height: 5cm;">
                                <?php elseif ($nivel_gasolina ==  "14"): ?>
                                    <img src="<?php echo base_url().'statics/inventario/imgs/gasolina_o.png'; ?>" style="height: 5cm;">
                                
                                <?php endif ?>
                            <?php else: ?>
                                <img src="<?php echo base_url().'statics/inventario/imgs/gasolina_1.png'; ?>" style="height: 5cm;">
                            <?php endif ?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="left">
                        <hr>
                        <h6 style="color:#52bf66;text-align: left;">Observaciones lavador: </h6>
                        <p align="justify" style="text-align: justify; width: 100%;font-size: 14px;color: darkblue;">
                           <?php if(isset($notas_lavador)) { if($notas_lavador != "") echo $notas_lavador; else echo "Sin observaciones"; } else {echo "Sin observaciones";} ?>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="right">
                        <hr>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table style="width: 90%;margin-left: 30px;">
                            <tr>
                                <td colspan="2">
                                    <h5 style="color:#52bf66;text-align: left;">Checklist suministros:</h5>
                                    <br>
                                </td>
                            </tr>
                            @if($checklist != NULL)
                                @foreach ($checklist as $serv)
                                    <tr>
                                        <td style="width: 1.5cm;">
                                            @if (isset($cheking))
                                                @if (in_array($serv->id,$cheking))
                                                    <img src="<?php echo base_url().'statics/inventario/imgs/check2_pdf.png'; ?>" style="width:15px;" >
                                                @else
                                                    <img src="<?php echo base_url().'statics/inventario/imgs/check_pdf.jpg'; ?>" style="width:15px;" >
                                                @endif
                                            @endif      
                                        </td>
                                        <td style="vertical-align: middle;width: auto;">
                                            &nbsp;&nbsp; <label for="">{{ $serv->nombre}}</label>   
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="right">
                        <hr>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="right">
                        <h6 style="text-align: right;">Evidencias</h6>
                        <br>
                        
                        <?php if (isset($evidencias)): ?>
                            <?php for ($x = 0 ; $x < count($evidencias) ; $x++): ?>
                                <a href="<?php echo (($origen_repo == '1') ? REPO_XEHOS : REPO_XEHOS_OP).$evidencias[$x]; ?>" style="font-size: 9px!important;margin: 5px;" class="btn btn-info" target="_blank">Archivo (<?php echo $x+1; ?>)</a>
                            <?php endfor ?>
                        <?php endif ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="right">
                        <br><hr>
                    </td>
                </tr>
                <tr>
                    <td align="center" style="width: 50%; color: black;">
                        <br><br>
                        <?php if (isset($firma_lavador)): ?>
                            <?php if ($firma_lavador != ""): ?>
                                <img src="<?= $firma_lavador ?>" style="height: 2.3cm;">
                            <?php endif; ?>
                        <?php endif; ?>
                        <br>
                        <?php if(isset($n_lavador)) echo $n_lavador; ?>
                    </td>
                    <td align="center" style="width: 50%; color: black;">
                        <br><br>
                        <?php if (isset($firma_supervisor)): ?>
                          <?php if ($firma_supervisor != ""): ?>
                              <img src="<?= $firma_supervisor ?>" style="height: 2.3cm;">
                          <?php endif; ?>
                        <?php endif; ?>
                        <br>
                        <?php if(isset($n_supervisor)) echo $n_supervisor; ?>
                    </td>
                </tr>
                <tr>
                    <td align="center" style="width: 50%; color: black;">
                        Firma del lavador
                    </td>
                    <td align="center" style="width: 50%; color: black;">
                        <?php if (isset($firma_supervisor)): ?>
                            <?php if (($firma_supervisor == "")||($firma_supervisor == NULL)): ?>
                                <a href="<?= base_url().'index.php/inventario_lavadores/inventario/generar_edicion/'.$id_firma ?>" class="btn btn-success" style="color:white;font-size: 12px!important;">
                                    Firmar revisión
                                </a>
                                <br>
                            <?php endif; ?>
                        <?php else: ?>
                            <a href="<?= base_url().'index.php/inventario_lavadores/inventario/generar_edicion/'.$id_firma ?>" class="btn btn-success" style="color:white;font-size: 12px!important;">
                                Firmar revisión
                            </a>
                            <br>
                        <?php endif; ?>
                        Firma del supervisor
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="right">
                        <br><hr>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="left">
                        <hr>
                        <h6 style="color:#52bf66;text-align: left;">Observaciones supervisor: </h6>
                        <p align="justify" style="text-align: justify; width: 100%;font-size: 14px;color: darkblue;">
                           <?php if(isset($notas_supervisor)) { if($notas_supervisor != "") echo $notas_supervisor; else echo "Sin observaciones"; } else {echo "Sin observaciones";} ?>
                        </p>
                    </td>
                </tr>
            </table>
        </div>
    </div>
@endsection
@section('included_js')
    @include('main/scripts_dt')
@endsection
