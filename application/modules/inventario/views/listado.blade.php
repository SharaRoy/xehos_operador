@layout('layout')
@section('contenido')
	<div class="row">
        <div class="col-sm-12" align="center">
        	<h5>Inventarios realizados</h5>
		</div>	
	</div>

	<br>
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
			            <table <?php if ($listado != NULL) echo 'id="bootstrap-data-table"'; ?> class="table table-striped table-bordered">
			                <thead>
			                    <tr>
			                    	<th>#</th>
			                    	<th>Folio</th>
			                    	<th>Cliente</th>
			                    	<th>Lavador</th>
			                    	<th>Fecha de alta</th>
			                    	<th>Ver</th>
			                    </tr>
			                </thead>
			                <tbody id="cuerpo">
			                	@if ($listado != NULL)
									@foreach ($listado as $i => $registro)
										<tr>
											<td style="vertical-align: middle;" align="center">
												<?= $i + 1;?>
											</td>
											<td style="vertical-align: middle;">
												<?= $registro->folio_info ?>
											</td>
											<td style="vertical-align: middle;">
												<?= $registro->nombre_cliente ?>
											</td>
											<td style="vertical-align: middle;" align="center">
												<?= $registro->nombre_asesor ?>
											</td>
											<td style="vertical-align: middle;" align="center">
												<?php 
													$fecha = new DateTime($registro->fecha_alta);
		        									echo $fecha->format('d-m-Y');
												?>
											</td>
											<td style="vertical-align: middle;" align="center">
												<a onclick="revisar_datos(<?= $registro->id_orden ?>)" class="btn btn-success" style="color:white;font-size: 10px;">
													Ver
			                                    </a>
											</td>
										</tr>
									@endforeach
			                	@else
			                		<tr>
			                			<td colspan="6" align="center">
			                				Sin cupones
			                			</td>
			                		</tr>
								@endif
			                </tbody>
			            </table>
		            </div>
		        </div>
		    </div>
		</div>
    </div><!-- .animated -->
@endsection
@section('included_js')
	@include('main/scripts_dt')
	<script>
		function revisar_datos(indice) {
    		var url_sis = "<?php echo base_url(); ?>index.php/inventario/redireccionar/"+indice;
	        $.ajax({
	            url: url_sis,
	            method: 'get',
	            data: {
	                
	            },
	            success:function(resp){
	              //console.log(resp);
	                if (resp.indexOf("handler			</p>")<1) {
	                    location.href = "<?php echo base_url(); ?>"+"index.php/inventario/generar_Revision/"+resp;
	                }
	            //Cierre de success
	            },
	            error:function(error){
	                console.log(error);
	            //Cierre del error
	            }
	        //Cierre del ajax
	        });
    	}
    </script>
@endsection