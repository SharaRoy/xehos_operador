<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Inventario extends MX_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Minventario', '', TRUE);
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'validation', 'url'));

        date_default_timezone_set(CONST_TIMEZONE);
        //ini_set('max_execution_time',300);
        //ini_set('set_time_limit',600);
        //ini_set('max_input_time',"40M");
        
        if($this->session->userdata('id_usuario')){}else{redirect('login/');}
    }

    public function index($servicio_ec='')
    {
        $id_orden = ((ctype_digit($servicio_ec)) ? $servicio_ec : $this->decrypt($servicio_ec));
        $data["servicioId"] = $id_orden;

        $data['vista'] = "3";
        $data['ruta_anterior'] = "operadores/inicia_servicio";
        $data['servicio_ruta'] = ((ctype_digit($servicio_ec)) ? $this->encrypt($servicio_ec) : $servicio_ec);

        $data["id_orden"] = $id_orden;

        //Comprobamos que exista el registro
        $revision = $this->Minventario->get_result("id_orden",$id_orden,"diagnostico");
        foreach ($revision as $row) {
            $verificacion = $row->id;
        }

        //Si existe el registro cargamos los datos
        if (isset($verificacion)) {
            $this->generar_Revision($servicio_ec,$data);
        //De lo contrario, se abre formulario de alta
        } else {
            $data['titulo'] = "Inventario";
            $data['titulo_dos'] = "Alta Inventario";
            $meses = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
            $data["mes"] = $meses[date("n")-1];

            //Precargamos los datos del formulario
            $datos = $this->Minventario->datos_servicio($data["id_orden"]);
            foreach ($datos as $row) {
                $data["folio_info"] = $row->folio_mostrar;
                $data["folio"] = $row->folio_mostrar;
                
                $data["nombre_asesor"] = $row->lavadorNombre;
                $data["cnombre_asesor"] = $row->lavadorNombre;
                
                $data["nombre_cliente"] = $row->nombre." ".$row->apellido_paterno." ".$row->apellido_materno;
                $data["cnombre_cliente"] = $row->nombre." ".$row->apellido_paterno." ".$row->apellido_materno;
                $data["cnombre_cliente"] = $row->nombre." ".$row->apellido_paterno." ".$row->apellido_materno;
            }

            $this->blade->render('formulario',$data);
        }
    }

    public function guardar_formulario()
    {
        if ($_POST["envio_formulario"] == "1") {
            $contenido["id"] = NULL;
            $contenido["origen_diagnostico"] = "2";
            $contenido["id_orden"] = $_POST["id_infop"];
            $contenido["folio_info"] = $_POST["orden"];
        }

        $contenido["marca_danos"] = $_POST["marca_danos"];
        $contenido["img_diagrama"] = $this->base64ToImage($_POST["danosMarcas"],"diagramas");

        $interiores = "";
        $interiores .= ((isset($_POST["interiores"])) ? $this->validateCheck($_POST["interiores"],"") : "");
        $interiores .= ((isset($_POST["interiores2"])) ? $this->validateCheck($_POST["interiores2"],"") : "");
        $interiores .= ((isset($_POST["interiores3"])) ? $this->validateCheck($_POST["interiores3"],"") : "");

        $contenido["interiores"] = $interiores; 
        //$this->validateCheck($_POST["interiores"],"");
        
        $contenido["cajuela"] = $this->validateCheck($_POST["cajuela"],"");
        $contenido["exteriores"] = $this->validateCheck($_POST["exteriores"],"");
        $contenido["documentacion"] = $this->validateCheck($_POST["documentacion"],"");
        $contenido["nivel_gasolina"] = $_POST["nivel_gasolina"];

        $contenido["articulos_vehiculo"] = $_POST["articulos_vehiculo"]; 
        $contenido["articulos"] = $_POST["articulos"];
        $contenido["reporte_articulos"] = $_POST["reporte_articulos"];

        $contenido["costo_diagnostico"] = $_POST["costo_diagnostico"];
        $contenido["dia_diagnostico"] = $_POST["dia_diagnostico"];
        $contenido["mes_diagnostico"] = $_POST["mes_diagnostico"];
        $contenido["anio_diagnostico"] = $_POST["anio_diagnostico"];

        $contenido["firma_asesor"] = ((isset($_POST["ruta_firma_asesor"])) ? $this->base64ToImage($_POST["ruta_firma_asesor"],"firmas") : "");
        $contenido["nombre_asesor"] = $_POST["nombre_asesor"];

        $contenido["firma_cliente"] = ((isset($_POST["ruta_firma_cliente"])) ? $this->base64ToImage($_POST["ruta_firma_cliente"],"firmas") : "");
        $contenido["nombre_cliente"] = $_POST["nombre_cliente"];

        if ($_POST["envio_formulario"] == "1") {
            $contenido["fecha_alta"] = date("Y-m-d H:i:s");
        }

        $contenido["fecha_actualiza"] = date("Y-m-d H:i:s");

        if ($_POST["envio_formulario"] == "1") {
            //Verificamos que no se haya guardado con errores
            $revision = $this->Minventario->get_result("folio_info",$_POST["orden"],"diagnostico");
            foreach ($revision as $row) {
                $verificacion = $row->id;
            }

            //Si no existe el registro guardamos el formulario
            if (!isset($verificacion)) {
                $servicio = $this->Minventario->save_register('diagnostico',$contenido);
                if ($servicio != 0) {
                    $archivos = $this->guardar_archivos($servicio);
                    $this->guardar_contrato($servicio);
                } else {
                    $respuesta = "NO SE PUDO GUARDAR EL FORMULARIO_".$_POST["id_infop"];
                    echo $respuesta;
                }

            //De lo contrario, se regresa un error
            } else {
                $respuesta = "YA EXISTE UN FORMULARIO CON ESE FOLIO_".$_POST["id_infop"];
                echo $respuesta;
            }
        }else{
            $actualizar = $this->Minventario->update_table_row('diagnostico',$contenido,'id_orden',$_POST["id_infop"]);
            if ($actualizar) {
                $archivos = $this->guardar_archivos($_POST["id_infop"]);
                //$this->guardar_contrato(0);
                $respuesta = "OK_".$this->encrypt($_POST["id_infop"]);
            } else {
                $respuesta = "NO SE PUDO GUARDAR EL FORMULARIO_".$_POST["id_infop"];
            }

            echo $respuesta;
        }
    }

    public function guardar_contrato($diagnostico='')
    {
        $registro = date("Y-m-d H:i:s");

        if ($_POST["envio_formulario"] == "1") {
            $contenido["id"] = NULL;
            $contenido["id_diagnostico"] = $diagnostico;
            $contenido["id_orden"] = $_POST["id_infop"];
        }

        $contenido["folio"] = $_POST["folio"];
        $contenido["fecha"] = $_POST["fecha_contrato"];
        $contenido["hora"] = $_POST["hora_contrato"];
        $contenido["empresa"] = $_POST["empresa_contrato"];
        $contenido["delegado"] = $_POST["autoriza_contrato"];

        $contenido["monto"] = $_POST["cantidad_contrato"];
        $contenido["compartir_datos"] = ((isset($_POST["publicidad_contrato"])) ? $_POST["publicidad_contrato"] : "0");
        $contenido["envio_publicidad"] = ((isset($_POST["envio_publicidad"])) ? $_POST["envio_publicidad"] : "0");

        $contenido["firma_cliente_publicidad"] = ((isset($_POST["ruta_firma_cliente_c1"])) ? $this->base64ToImage($_POST["ruta_firma_cliente_c1"],"firmas") : "");
        //$this->base64ToImage($_POST["ruta_firma_cliente_c1"],"firmas");

        $contenido["firma_asesor"] = ((isset($_POST["ruta_ccfirma_asesor"])) ? $this->base64ToImage($_POST["ruta_ccfirma_asesor"],"firmas") : "");
        //$this->base64ToImage($_POST["ruta_ccfirma_asesor"],"firmas");
        $contenido["nombre_asesor"] = $_POST["cnombre_asesor"];

        $contenido["firma_cliente"] = ((isset($_POST["ruta_ccfirma_cliente"])) ? $this->base64ToImage($_POST["ruta_ccfirma_cliente"],"firmas") : "");
        //$this->base64ToImage($_POST["ruta_ccfirma_cliente"],"firmas");
        $contenido["nombre_cliente"] = $_POST["cnombre_cliente"];

        $contenido["privacidad"] = ((isset($_POST["privacidad_contrato"])) ? $_POST["privacidad_contrato"] : "0");
        $contenido["registro_publico"] = $_POST["registro_publico"];
        $contenido["expediente_publico"] = $_POST["expediente_publico"];
        $contenido["d_expediente"] = $_POST["registro_publico_dia"];
        $contenido["m_expediente"] = $_POST["registro_publico_mes"];
        $contenido["a_expediente"] = $_POST["registro_publico_anio"];

        if ($_POST["envio_formulario"] == "1") {
            $contenido["fecha_alta"] = $registro;
        }

        $contenido["fecha_actualiza"] = $registro;

        if ($_POST["envio_formulario"] == "1") {
            $servicio = $this->Minventario->save_register('contrato',$contenido);
            if ($servicio != 0) {
                //Marcamos el paso
                $lavador = $this->session->userdata('id_usuario');
                $id_paso = $this->Minventario->id_paso_seguimiento($_POST["id_infop"],$lavador);

                $proceso = array(
                    "inventario" => 1,
                    "fecha_actualiza" => date("Y-m-d H:i:s")
                );

                $this->Minventario->update_table_row('proceso_servicio_lavado',$proceso,'id',$id_paso);

                //Guardamos el movimiento del servicio
                $this->guardar_movimiento($_POST["id_infop"],"Llenado de inventario");

                $respuesta = "OK_".$this->encrypt($_POST["id_infop"]);
            } else {
                $respuesta = "NO SE PUDO GUARDAR EL CONTRATO_".$_POST["orden"];
            }
            
        }else{
            $actualizar = $this->Minventario->update_table_row('contrato',$contenido,'id_orden',$_POST["id_infop"]);
            if ($actualizar) {
                $respuesta = "OK_".$this->encrypt($_POST["id_infop"]);
            } else {
                $respuesta = "NO SE PUDO GUARDAR EL CONTRATO_".$_POST["orden"];
            }
        }

        echo $respuesta;
    }

    public function guardar_diagrama()
    {
        $img_diagrama = $this->base64ToImage($_POST["diagrama"],"diagramas");
        echo "OK=".$img_diagrama;
    }

    public function descomprimir()
    {
        $respuesta = "";
        for ($i=1; $i <31 ; $i++) {
            if (isset($_POST["encript_".$i])) {
                $respuesta .= trim($_POST["encript_".$i]);
            }
        }
        return $respuesta;
    }

    public function guardar_archivos($diagnostico='')
    {
        ini_set('max_execution_time',1800);
        ini_set('set_time_limit',1800);

        if(count($_FILES['archivo']['tmp_name']) > 0){
            //Como el elemento es un arreglos utilizamos foreach para extraer todos los valores
            for ($i = 0; $i < count($_FILES['archivo']['tmp_name']); $i++) {
                $_FILES['tempFile']['name'] = $_FILES['archivo']['name'][$i];
                $_FILES['tempFile']['type'] = $_FILES['archivo']['type'][$i];
                $_FILES['tempFile']['tmp_name'] = $_FILES['archivo']['tmp_name'][$i];
                $_FILES['tempFile']['error'] = $_FILES['archivo']['error'][$i];
                $_FILES['tempFile']['size'] = $_FILES['archivo']['size'][$i];

                //Url donde se guardara la imagen
                $urlDoc = "videos";
                //Generamos un nombre random
                $str = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                $urlNombre = date("YmdHi")."_";
                for($j=0; $j<=7; $j++ ){
                    $urlNombre .= substr($str, rand(0,strlen($str)-1) ,1 );
                }

                //Validamos que exista la carpeta destino   base_url()
                if(!file_exists($urlDoc)){
                    mkdir($urlDoc, 0647, true);
                }

                //Configuramos las propiedades permitidas para la imagen
                $config['upload_path'] = $urlDoc;
                $config['file_name'] = $urlNombre;
                $config['allowed_types'] = "*";

                //Cargamos la libreria y la inicializamos
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('tempFile')) {
                    $data['uploadError'] = $this->upload->display_errors();
                    // echo $this->upload->display_errors();
                    //$respuesta = "NO SE PUDIERON CARGAR TODOS LOS ARCHIVOS";
                    $respuesta = "";
                }else{
                    //Si se pudo cargar la imagen la guardamos
                    $fileData = $this->upload->data();
                    $ext = explode(".",$_FILES['tempFile']['name']);
                    $path = $urlDoc."/".$urlNombre.".".$ext[count($ext)-1];

                    $evidencia = array(
                        "id" => NULL,
                        "id_orden" => $_POST["id_infop"],
                        "inventario" => "servicio",
                        "repositorio" => "2",
                        "evidencia" => $path,
                        "activo" => 1,
                        "fecha_alta" => date("Y-m-d H:i:s")
                    );

                    $servicio = $this->Minventario->save_register('evidencia_inventario',$evidencia);

                    $respuesta = "OK";
                }
            }
        }else{
            $respuesta = "OK";
        }

        return $respuesta;
    }

    function base64ToImage($imgBase64 = "",$direcctorio = "") {
        //Verificamos si se guarda la imagen o si se envio las url
        if (substr($imgBase64,0,8) != "statics/") {
            //Generamos un nombre random para la imagen
            $str = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            $urlNombre = date("YmdHi")."_";
            for($i=0; $i<=7; $i++ ){
                $urlNombre .= substr($str, rand(0,strlen($str)-1) ,1 );
            }

            $urlDoc = 'statics/inventario/'.$direcctorio;

            $data = explode(',', $imgBase64);
            //Creamos la ruta donde se guardara la firma y su extensión
            if ($data[0] == "data:image/png;base64") {
                $base ='statics/inventario/'.$direcctorio.'/'.$urlNombre.".png";
                $Base64Img = base64_decode($data[1]);
                file_put_contents($base, $Base64Img);
                
            }elseif ($data[0] == "data:image/jpeg;base64") {
                $base ='statics/inventario/'.$direcctorio.'/'.$urlNombre.".jpeg";

                $Base64Img = base64_decode($data[1]);
                file_put_contents($base, $Base64Img);
            }else {
                $base = "";
            }
        } else {
            $base = $imgBase64;
        }

        return $base;
    }

    public function validateCheck($campo = NULL,$elseValue = "")
    {
        if ($campo == NULL) {
            $respuesta = $elseValue;
        }else {
            $respuesta = "";
            if (count($campo) >1){
                //Separamos los index de los id de tipo de usuario
                for ($i=0; $i < count($campo) ; $i++) {
                    $respuesta .= $campo[$i]."_";
                }
            }
        }
        return $respuesta;
    }

    //Comprobamos que al final se genere el pdf
    public function generar_PDF($id_orden = 0)
    {
        $datos["destinoVista"] = "PDF";
        $servicio = $this->decrypt($id_orden);
        $this->recuperar_formulario($servicio,$datos);
    }

    //Comprobamos que al final se genera la vista de revision
    public function generar_revision($id_orden = 0)
    {
        $data['vista'] = "3";
        $data['ruta_anterior'] = "operadores/inicia_servicio";
        $data['servicio_ruta'] = $id_orden;

        $data["destinoVista"] = "Revisión";
        $servicio = ((ctype_digit($id_orden)) ? $id_orden : $this->decrypt($id_orden));
        $this->recuperar_formulario($servicio,$data);
    }

    //Comprobamos que al final se genere el pdf
    public function generar_edicion($id_orden = 0)
    {
        $datos["destinoVista"] = "Formulario";
        $servicio = ((ctype_digit($id_orden)) ? $id_orden : $this->decrypt($id_orden));
        $this->recuperar_formulario($servicio,$datos);
    }

    public function recuperar_formulario($id_orden='',$datos = NULL)
    {
        $datos['titulo'] = "Inventario";
        $datos['titulo_dos'] = "";

        //Verificamos si se imprimira en formulario o en pdf
        if (!isset($datos["destinoVista"])) {
            $datos["destinoVista"] = "Formulario";
        }

        if (!isset($data["servicioId"])) {
            $data["servicioId"] = $id_orden;
        }

        $query = $this->Minventario->get_result("id_orden",$id_orden,"diagnostico");

        foreach ($query as $row){
            $datos["id"] = $row->id;
            $datos["id_orden"] = $row->id_orden;
            $datos["origen_repo"] = $row->origen_diagnostico;
            $datos["folio_info"] = $row->folio_info;

            $datos["marca_danos"] = $row->marca_danos;
            $datos["interiores"] = explode("_",$row->interiores);
            $datos["cajuela"] = explode("_",$row->cajuela);
            $datos["exteriores"] = explode("_",$row->exteriores);
            $datos["documentacion"] = explode("_",$row->documentacion);
            $datos["nivel_gasolina"] = $row->nivel_gasolina;
            $datos["articulos_vehiculo"] = $row->articulos_vehiculo;
            $datos["articulos"] = $row->articulos;
            $datos["reporte_articulos"] = $row->reporte_articulos;
            $datos["costo_diagnostico"] = $row->costo_diagnostico;
            $datos["dia_diagnostico"] = $row->dia_diagnostico;
            $datos["mes_diagnostico"] = $row->mes_diagnostico;
            $datos["anio_diagnostico"] = $row->anio_diagnostico;
            $datos["nombre_asesor"] = $row->nombre_asesor;
            $datos["nombre_cliente"] = $row->nombre_cliente;

            //Validamos las firmas
            if ($datos["origen_repo"] == "1") { 
                $img_diagrama = REPO_XEHOS.$row->img_diagrama;
                $firma_asesor = REPO_XEHOS.$row->firma_asesor;
                $firma_cliente = REPO_XEHOS.$row->firma_cliente;
            }else{
                $img_diagrama = REPO_XEHOS_OP.$row->img_diagrama;
                $firma_asesor = REPO_XEHOS_OP.$row->firma_asesor;
                $firma_cliente = REPO_XEHOS_OP.$row->firma_cliente;
            }

            $datos["img_diagrama"] = (($this->url_exists($img_diagrama)) ? $img_diagrama : "") ;
            $datos["firma_asesor"] = (($this->url_exists($firma_asesor)) ? $firma_asesor : "") ;
            $datos["firma_cliente"] = (($this->url_exists($firma_cliente)) ? $firma_cliente : "") ;
        }

        //Si existe el diagnostico, cargamos el contrato
        if (isset($datos["origen_repo"])) {
            $contrato = $this->Minventario->get_result("id_orden",$id_orden,"contrato");
            foreach ($contrato as $row){
                $datos["folio"] = $row->folio;
                
                $datos["fecha_contrato"] = $row->fecha;
                $fecha = new DateTime($row->fecha.' 00:00:00');
                $datos["fecha_contrato_visual"] = $fecha->format('d-m-Y');

                $datos["hora_contrato"] = $row->hora;
                $datos["empresa_contrato"] = $row->empresa;
                $datos["autoriza_contrato"] = $row->delegado;
                $datos["cantidad_contrato"] = $row->monto;
                $datos["publicidad_contrato"] = $row->compartir_datos;
                $datos["envio_publicidad"] = $row->envio_publicidad;

                $datos["cnombre_asesor"] = $row->nombre_asesor;
                $datos["cnombre_cliente"] = $row->nombre_cliente;
                $datos["privacidad_contrato"] = $row->privacidad;
                $datos["registro_publico"] = $row->registro_publico;
                $datos["expediente_publico"] = $row->expediente_publico;
                $datos["registro_publico_dia"] = $row->d_expediente;
                $datos["registro_publico_mes"] = $row->m_expediente;
                $datos["registro_publico_anio"] = $row->a_expediente;

                //Validamos las firmas
                if ($datos["origen_repo"] == "1") {
                    $firma_cliente_c1 = REPO_XEHOS.$row->firma_cliente_publicidad;
                    $ccfirma_asesor = REPO_XEHOS.$row->firma_asesor;
                    $ccfirma_cliente = REPO_XEHOS.$row->firma_cliente;
                }else{
                    $firma_cliente_c1 = REPO_XEHOS_OP.$row->firma_cliente_publicidad;
                    $ccfirma_asesor = REPO_XEHOS_OP.$row->firma_asesor;
                    $ccfirma_cliente = REPO_XEHOS_OP.$row->firma_cliente;
                }

                $datos["firma_cliente_c1"] = (($this->url_exists($firma_cliente_c1)) ? $firma_cliente_c1 : "") ;
                $datos["ccfirma_asesor"] = (($this->url_exists($ccfirma_asesor)) ? $ccfirma_asesor : "") ;
                $datos["ccfirma_cliente"] = (($this->url_exists($ccfirma_cliente)) ? $ccfirma_cliente : "") ;
            }
        }

        //Cargamos las evidencias  
        $evidencias = $this->Minventario->get_result_field("id_orden",$id_orden,"inventario","servicio","evidencia_inventario");
        foreach ($evidencias as $row){
            if ($row->activo == "1") {
                $datos["evidencias"][] = $row->evidencia;
            }
        }

        $datos["totalRenglones"] = "0";

        if ($datos["destinoVista"] != "Formulario") {
            $this->cargar_servicio($id_orden,$datos);            
        }else{
            $this->blade->render('formulario',$datos);
        }
    }

    public function listado()
    {
        $data['titulo'] = "Inventario";
        $data['titulo_dos'] = "Listado";

        $data['listado'] = $this->Minventario->get_table('diagnostico');

        $this->blade->render('listado',$data);
    }

    public function cargar_servicio($id_orden='',$datos = NULL)
    {
        $vista_informativa = $this->Minventario->get_result("id",$id_orden,"v_info_servicios");
        foreach ($vista_informativa as $row){
            $datos["folio_mostrar"] = $row->folio_mostrar;
            $datos["orden_asesor"] = $row->lavadorNombre;
            $datos["orden_telefono_asesor"] = "3338352043";
            $datos["orden_datos_nombres"] = $row->nombre;
            $datos["orden_datos_apellido_paterno"] = $row->apellido_paterno;
            $datos["orden_datos_apellido_materno"] = $row->apellido_materno;
            $datos["orden_nombre_compania"] = $row->razon_social;

            $datos["orden_nombre_contacto_compania"] = "SN";
            $datos["orden_ap_contacto"] = "SN";
            $datos["orden_am_contacto"] = "SN";

            $datos["orden_datos_email"] = $row->email;
            $datos["orden_rfc"] = $row->rfc;
            $datos["orden_correo_compania"] = $row->email_facturacion;

            $datos["orden_domicilio"] = $row->domicilio;
            $datos["orden_municipio"] = $row->municipio;
            $datos["orden_cp"] = $row->cp;
            $datos["orden_estado"] = $row->estado;
            $datos["orden_telefono_movil"] = $row->telefono;
            $datos["orden_otro_telefono"] = $row->telefono;

            $datos["orden_vehiculo_placas"] = $row->placas;
            $datos["orden_vehiculo_identificacion"] = $row->numero_serie;
            $datos["orden_vehiculo_kilometraje"] = $row->kilometraje;
            $datos["orden_vehiculo_marca"] = $row->marca;
            $datos["orden_categoria"] = $row->modelo;
            $datos["orden_vehiculo_anio"] = $row->anio;
            $datos["color"] = $row->color;

            $datos["item_cantidad"][] = "1";
            $datos["item_descripcion"][] = $row->servicioNombre;
            
            $iva = (float)$row->precio * 0.16;
            $precio = (float)$row->precio - $iva;
            $precio_unitario = (float)$row->precio;

            $datos["item_precio_unitario"][] = $precio;
            $datos["item_descuento"][] = "0.00";
            $datos["item_total"][] = $precio;

            $datos["subtotal_items"] = $precio;

            $datos["iva_items"] = $iva;
            $datos["presupuesto_items"] = $row->precio;
        }

        //Recuperamos datos del cupon
        if (isset($precio_unitario)) {
            $id_cupon = $this->Minventario->buscar_cupon_servicio($id_orden);
            foreach ($id_cupon as $row){
                //sacamos el descuento del cupon
                $descuento = (float)$row->descuento / 100;
                $precio_descuento = $precio_unitario * $descuento;
                //al precio del servivio se le resta lo del descuento
                $precio_uni = $precio_unitario - $precio_descuento;
                //se sacan los nuevos totales
                $iva = $precio_uni * 0.16;
                $precio = $precio_uni - $iva;

                $datos["item_descripcion"][0] .= " (cupón: ".$row->cupon.")";
                $datos["item_precio_unitario"][0] = $precio_unitario;
                $datos["item_descuento"][0] = $precio_descuento;
                $datos["item_total"][0] = $precio_uni;

                $datos["subtotal_items"] = $precio;

                $datos["iva_items"] = $iva;
                $datos["presupuesto_items"] = $precio + $iva;
            }
        }

        if ($datos["destinoVista"] == "PDF") {
            $this->load->view("plantillapdf2", $datos);
        }elseif ($datos["destinoVista"] == "Revisión") {
            $this->blade->render('revision',$datos);
        }else{
            $this->blade->render('formulario',$datos);
        }
    }

    //Recuperar informacion basica
    public function recuperar_datos()
    {
        $orden = $_POST["orden"];
        $cadena = "";
        $datos = "";
        //buscamos la orden por numero de orden o placa
        $consulta = $this->Minventario->buscar_datos_orden($orden);

        foreach ($consulta as $row){
            $datos .= $row->id."=";
            $datos .= $row->lavadorNombre."=";
            //$datos .= $row->nombre_cliente."=";
            $datos .= $row->nombre." ".$row->apellido_paterno." ".$row->apellido_materno."=";
            $datos .= number_format($row->precio)."=";
            $datos .= $row->folio_mostrar."=";
            $id = $row->id;
        }

        $query = $this->Minventario->get_result("folio_info",$orden,"diagnostico");

        foreach ($query as $row){
            $id_orden = $row->id;
        }

        if (isset($id_orden)) {
            $cadena .= "EDITA=";
            $cadena .= ((isset($id)) ? $this->encrypt($id) : "0") ."=";
        }else{
            $cadena .= "ALTA=";
            $cadena .= ((isset($id)) ? $datos : "=");
        }

        echo $cadena;
    }

    public function guardar_movimiento($id_servicio='',$movimiento = '')
    {
        //Recuperamos los datos del servicio
        $datos_servicio = $this->Minventario->datos_base_servicio($id_servicio);

        if (count($datos_servicio)>0) {
            $contenedor = array(
                "id" => NULL,
                "id_servicio" => $id_servicio,
                "id_lavador" => $this->session->userdata('id_usuario'),
                "id_cliente" => $datos_servicio[0],
                "movimento" => $movimiento,
                "tipo_usuario" => $this->session->userdata('rol_usuario'),
                "fecha_alta" => date("Y-m-d H:i:s")
            );

            $this->Minventario->save_register('historial_procesos_servicio',$contenedor);
        }

        return TRUE;
    }

    public function redireccionar($id='')
    {
        $registro = $this->encrypt($id);
        echo $registro;
    }

    public function url_exists($url) {
        $context = stream_context_create( [
            'ssl' => [
                'verify_peer' => false,
                'verify_peer_name' => false,
            ],
        ]);
        
        $h = get_headers($url, 0, $context);
        
        $status = array();
        preg_match('/HTTP\/.* ([0-9]+) .*/', $h[0] , $status);
        return ($status[1] == 200);
    }

    function encrypt($data){
        $id = (double)$data*CONST_ENCRYPT;
        $url_id = base64_encode($id);
        $url = str_replace("=", "" ,$url_id);
        return $url;
        //return $data;
    }

    function decrypt($data){
        $url_id = base64_decode($data);
        $id = (double)$url_id/CONST_ENCRYPT;
        return $id;
        //return $data;
    }


  }
