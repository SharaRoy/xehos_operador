<?php
class Minventario extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_result($campo,$value,$tabla){
        $result = $this->db->where($campo,$value)->limit(100)->get($tabla)->result();
        return $result;
  	}

    public function get_table_limit($campo,$tabla){
        $result = $this->db->order_by($campo,"DESC")->limit(500)->get($tabla)->result();
        return $result;
    }

    public function get_result_field($campo_1 = "",$value_1 = "",$campo_2 = "",$value_2 = "",$tabla = ""){
        $result = $this->db->where($campo_1,$value_1)->where($campo_2,$value_2)->get($tabla)->result();
        return $result;
    }

  	public function save_register($table, $data){
        $result = $this->db->insert($table, $data);
        //Comprobamos que se guarden correctamente el registro
        if ($result) {
            $result = $this->db->insert_id();
        }else {
            $result = 0;
        }
        return $result;
    }

    public function update_table_row($table,$data,$id_table,$id){
    		$result = $this->db->update($table, $data, array($id_table=>$id));
        return $result;
  	}

  	public function buscar_datos_orden($dato_bq ='')
  	{
  		$q = $this->db->like('id',$dato_bq)
            ->or_like('placas',$dato_bq)
            ->or_like('folio_mostrar',$dato_bq)
            ->select('id,nombre_cliente,lavadorNombre,id,precio,nombre,apellido_paterno,apellido_materno,folio_mostrar')
            ->order_by("id", "DESC")
            ->limit(1)
            ->get('v_info_servicios')->result();
        
        return $q;
  	}

    public function buscar_cupon_servicio($id_servicio ='')
    {
        $result = $this->db->where('serv.id ',$id_servicio)
            ->join('cupones AS cupon','cupon.id = serv.id_cupon_servicio')
            ->select('cupon.cupon, cupon.descuento')
            ->order_by('serv.id','DESC')
            ->get('servicio_lavado AS serv')
            ->result();
        
        return $result;
    }

    public function datos_servicio($servicio='')
    {
        $result = $this->db->where('serv.id ',$servicio)
            ->or_where('serv.folio_mostrar',$servicio)

            //->join('horarios_lavadores AS h','serv.id_horario = h.id')
            ->join('lavadores AS l','l.lavadorId = serv.id_lavador')
            //->join('cat_estatus_lavado AS cat_st','cat_st.id = serv.id_estatus_lavado')
            ->join('servicios AS s','s.servicioId = serv.id_servicio')
            
            ->join('usuarios AS u','u.id = serv.id_usuario')
            //->join('ubicacion_usuario AS ub','ub.id = serv.id_ubicacion')
            //->join('cat_municipios AS cat_m','cat_m.id = ub.id_municipio')
            //->join('estados AS edo','edo.id = ub.id_estado')
            
            //Recuperamos el auto y sus especificaciones por catalo
            ->join('autos AS auto','auto.id = serv.id_auto')
            //->join('cat_colores AS color','color.id = auto.id_color')
            //->join('cat_marcas AS marca','marca.id = auto.id_marca')
            ->join('cat_modelos AS modelo','modelo.id = auto.id_modelo')
            //->join('cat_tipo_autos AS tipo_a','tipo_a.id = modelo.id_tipo_auto')

            //Precio del paquete
            ->join('precios_servicios AS pqt','pqt.id_tipo_auto = modelo.id_tipo_auto')
            ->join('precios_servicios AS pqt2','pqt2.id_servicio = serv.id_servicio')

            ->select('u.nombre, u.email, u.telefono, u.apellido_paterno, u.apellido_materno, auto.placas, auto.numero_serie, auto.kilometraje, l.lavadorNombre, s.servicioNombre, serv.folio_mostrar, pqt.precio') // h.fecha, h.hora,cat_st.estatus, ub.latitud, ub.longitud,  ub.numero_ext, ub.numero_int, ub.colonia, ub.calle, cat_m.municipio, edo.estado, color.color, marca.marca, modelo.modelo, tipo_a.tipo_auto,
            ->order_by('serv.id','DESC')
            ->limit(1)
            ->get('servicio_lavado AS serv')
            ->result();
            //echo $this->db->last_query();die();
        return $result;
    }

    public function costo_servicio($id_servicio='')
    {
        $sql = "SELECT o.id,o.nombre_compania,c.asesor,c.vehiculo_modelo, c.vehiculo_numero_serie, o.numero_interno AS notorre, o.color,c.datos_nombres, c.datos_apellido_paterno, c.datos_apellido_materno, c.datos_email ".
        "FROM citas AS c ".
        "INNER JOIN ordenservicio AS o ON o.id_cita = c.id_cita ".
        "WHERE c.id_cita = ?";
        $query = $this->db->query($sql,array($id));
        $data = $query->result();
            return $data;
    }

    public function listado_inventarios()
    {
        $result = $this->db->select('folio_info, nombre_cliente, nombre_asesor, fecha_alta, id_orden, id')
            ->order_by('id',"DESC")
            ->limit(500)
            ->get('diagnostico')
            ->result();
    }

    public function get_table($table){
        $data = $this->db->get($table)->result();
        return $data;
    }

    public function datos_base_servicio($servicio='')
    {
        $q = $this->db->where('id ',$servicio)       
        ->select('id_usuario,id_servicio')
        ->get('servicio_lavado');

        if($q->num_rows()==1){
            $retorno = [$q->row()->id_usuario,$q->row()->id_servicio];
        }else{
            $retorno = [];
        }
        
        return $retorno;
    }

    public function id_paso_seguimiento($id_servicio,$id_lavador = ''){
        $q = $this->db->where('id_lavador',$id_lavador)
        ->where('id_servicio',$id_servicio)
        ->select('id')
        ->get('proceso_servicio_lavado');

        if($q->num_rows()==1){
            $retorno = $q->row()->id;
        }else{
            $retorno = '0';
        }
        return $retorno;
    }


}