<?php
class M_Apis extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_result($campo,$value,$tabla){
        $result = $this->db->where($campo,$value)->order_by($campo, "DESC")->get($tabla)->result();
        return $result;
  	}

    public function get_result_id($campo,$value,$tabla){
        $result = $this->db->where($campo,$value)->order_by("id", "DESC")->get($tabla)->result();
        return $result;
    }

  	public function get_table($table){
		$data = $this->db->get($table)->result();
		return $data;
	}

  	public function save_register($table, $data){
        $result = $this->db->insert($table, $data);
        //Comprobamos que se guarden correctamente el registro
        if ($result) {
            $result = $this->db->insert_id();
        }else {
            $result = 0;
        }
        return $result;
    }

    public function update_table_row($table,$data,$id_table,$id){
    		$result = $this->db->update($table, $data, array($id_table=>$id));
        return $result;
  	}

    public function get_result_field($campo_1 = "",$value_1 = "",$campo_2 = "",$value_2 = "",$tabla = ""){
        $result = $this->db->where($campo_1,$value_1)->where($campo_2,$value_2)->get($tabla)->result();
        return $result;
    }

    public function get_result_one($campo,$value,$tabla){
        $result = $this->db->where($campo,$value)->order_by("id","DESC")->limit(1)->get($tabla)->result();
        return $result;
    }

    public function nombre_servicio($id=''){
        $q = $this->db->where('servicioId',$id)->select('servicioNombre')->get('servicios');
        if($q->num_rows()==1){
            $retorno = $q->row()->servicioNombre;
        }else{
            $retorno = '';
        }
        return $retorno;
    }

    public function nombre_lavador($id=''){
        $q = $this->db->where('lavadorId',$id)->select('lavadorNombre')->get('lavadores');
        if($q->num_rows()==1){
            $retorno = $q->row()->lavadorNombre;
        }else{
            $retorno = 'No encontrado';
        }
        return $retorno;
    }

    public function telefono_en_uso($campo = "",$valor = "",$tabla = ""){
        $q = $this->db->where($campo,$valor)
            ->select($campo)
            ->get($tabla);

        if($q->num_rows()==1){
            $retorno = '1';
        }else{
            $retorno = '0';
        }
        return $retorno;
    }

    public function servicios_lavador($id_lavador='')
    {
        $result = $this->db->where('l.lavadorId ',$id_lavador)
            //->join('proceso_servicio_lavado AS paso','paso.id_servicio = sl.id_servicio','left')
            ->join('horarios_lavadores AS h','sl.id_horario = h.id')
            ->join('lavadores AS l','sl.id_lavador = l.lavadorId')
            ->join('servicios AS s','s.servicioId = sl.id_servicio')
            ->join('cat_estatus_lavado AS cat_st','cat_st.id = sl.id_estatus_lavado')

            ->select('l.lavadorNombre, h.fecha, h.hora, cat_st.estatus, s.servicioNombre, sl.id, cat_st.color, cat_st.color_letra, sl.folio_mostrar, sl.id_estatus_lavado, sl.cancelado, sl.reagendado, sl.no_contactar')
            ->order_by('h.fecha DESC , h.hora DESC')
            ->limit(100)
            ->get('servicio_lavado AS sl')
            ->result();
            //echo $this->db->last_query();die();
        return $result;
    }

    public function datos_servicio($servicio='')
    {
        $result = $this->db->where('serv.id ',$servicio)
            ->join('horarios_lavadores AS h','serv.id_horario = h.id','left')
            ->join('lavadores AS l','l.lavadorId = serv.id_lavador','left')
            ->join('servicios AS s','s.servicioId = serv.id_servicio','left')
            ->join('cat_estatus_lavado AS cat_st','cat_st.id = serv.id_estatus_lavado','left')
            ->join('cat_metodo_pago AS pago','pago.id = serv.id_tipo_pago','left')

            
            ->join('usuarios AS u','u.id = serv.id_usuario','left')
            ->join('ubicacion_servicio AS ub','ub.id = serv.id_ubicacion','left')
            ->join('cat_municipios AS cat_m','cat_m.id = ub.id_municipio','left')
            ->join('estados AS edo','edo.id = ub.id_estado','left')
            
            //Recuperamos el auto y sus especificaciones por catalo
            ->join('autos AS auto','auto.id = serv.id_auto','left')
            ->join('cat_colores AS color','color.id = auto.id_color','left')
            ->join('cat_marcas AS marca','marca.id = auto.id_marca','left')
            ->join('cat_modelos AS modelo','modelo.id = auto.id_modelo','left')
            ->join('cat_tipo_autos AS tipo_a','tipo_a.id = modelo.id_tipo_auto','left')

            ->select('u.nombre, u.email, u.telefono, u.apellido_paterno, u.apellido_materno, ub.latitud, ub.longitud,  ub.numero_ext, ub.numero_int, ub.colonia, ub.calle, cat_m.municipio, edo.estado, auto.placas, auto.numero_serie, auto.kilometraje, color.color, marca.marca, modelo.modelo, tipo_a.tipo_auto, l.lavadorNombre, h.fecha, h.hora, cat_st.estatus, s.servicioNombre, serv.folio_mostrar, serv.id_auto, serv.cancelado, serv.reagendado, pago.metodo_pago, serv.pago, serv.total')
            ->order_by('h.fecha DESC , h.hora DESC')
            ->get('servicio_lavado AS serv')
            ->result();
            //echo $this->db->last_query();die();
        return $result;
    }

    public function datos_base_servicio($servicio='')
    {
        $q = $this->db->where('id ',$servicio)       
        ->select('id_usuario,id_servicio,id_lavador')
        ->get('servicio_lavado');

        if($q->num_rows()==1){
            $retorno = [$q->row()->id_usuario,$q->row()->id_servicio,$q->row()->id_lavador];
        }else{
            $retorno = [];
        }
        
        return $retorno;
    }


    public function buscar_inventario($id_lavador='')
    {
        $q = $this->db->where('id_lavador',$id_lavador)
            ->where('fecha_llenado',date("Y-m-d"))
            ->select('id')
            ->order_by("id","DESC")
            ->limit(1)
            ->get('diag_unidades_lavadores');

        if($q->num_rows()==1){
            $retorno = $q->row()->id;
        }else{
            $retorno = '';
        }
        return $retorno;
    }

    public function recuperar_cliente($id_lavador = ''){
        $q = $this->db->where('id_lavador',$id_lavador)->select('id_usuario')->get('servicio_lavado');
        if($q->num_rows()==1){
            $retorno = $q->row()->id_usuario;
        }else{
            $retorno = '0';
        }
        return $retorno;
    }

    public function pasos_proceso($id_servicio = '')
    {
        $q = $this->db->where('id_servicio ',$id_servicio)       
        ->select('*')
        ->get('proceso_servicio_lavado')
        ->result();

        return $q;
    }

    public function id_paso_seguimiento($id_servicio,$id_lavador = ''){
        $q = $this->db->where('id_lavador',$id_lavador)
        ->where('id_servicio',$id_servicio)
        ->select('id')
        ->get('proceso_servicio_lavado');

        if($q->num_rows()==1){
            $retorno = $q->row()->id;
        }else{
            $retorno = '0';
        }
        return $retorno;
    }

    public function evidencia_servicio($id_servicio = '',$id_lavador = '')
    {
        $q = $this->db->where('id_servicio ',$id_servicio)
        ->where('id_lavador ',$id_lavador)
        ->where('activo ','1')
        ->select('*')
        ->get('evidencia_proceso_servicio')
        ->result();

        return $q;
    }

    public function servicio_activo($id_lavador='')
    {
        $q = $this->db->where('id_lavador',$id_lavador)
        ->where('id_estatus_lavado !=',"1")
        ->where('id_estatus_lavado !=',"6")
        ->where('cancelado',"0")
        ->where('reagendado',"0")
        ->select('id')
        ->limit(1)
        ->get('servicio_lavado');

        if($q->num_rows()==1){
            $retorno = '1';
        }else{
            $retorno = '0';
        }
        return $retorno;
    }

    public function tipo_pago($id_servicio = '')
    {
        $q = $this->db->where('id ',$id_servicio)       
        ->select('pago, id_tipo_pago')
        ->limit(1)
        ->get('servicio_lavado');

        if($q->num_rows()==1){
            $retorno = [$q->row()->id_tipo_pago,$q->row()->pago];
        }else{
            $retorno = [NULL,NULL];
        }
        
        return $retorno;
    }

    public function datos_facturacion($id_servicio = '')
    {
        $result = $this->db->where('serv.id ',$id_servicio)
            ->join('lavadores AS l','l.lavadorId = serv.id_lavador')
            ->join('sucursales AS scu','scu.id = l.id_sucursal')
            ->join('servicios AS s','s.servicioId = serv.id_servicio')
            ->join('cupones AS cup','cup.id = serv.id_cupon_servicio','left')

            ->join('usuarios AS u','u.id = serv.id_usuario')
            ->join('datos_facturacion AS df','df.id_usuario = serv.id_usuario')
            ->join('cat_cfdi AS cfdi','cfdi.id = df.id_cfdi')

            ->select('u.nombre, u.apellido_paterno, u.apellido_materno, df.email_facturacion, df.rfc, df.cp, df.id_usuario, df.domicilio, df.razon_social, scu.nomenclatura, serv.id_usuario, cfdi.clave, serv.total, serv.id, s.servicioNombre, serv.id_servicio, serv.id_cupon_servicio, cup.cupon, cup.descuento')

            ->order_by('serv.id',"DESC")
            ->limit(1)
            ->get('servicio_lavado AS serv')
            ->result();
            //echo $this->db->last_query();die();
        return $result;
    }

    public function datos_factura_gen($id_servicio = '')
    {
        $result = $this->db->where('serv.id ',$id_servicio)
            ->join('lavadores AS l','l.lavadorId = serv.id_lavador','left')
            ->join('sucursales AS scu','scu.id = l.id_sucursal','left')
            ->join('servicios AS s','s.servicioId = serv.id_servicio','left')
            ->join('usuarios AS u','u.id = serv.id_usuario','left')
            ->join('cupones AS cup','cup.id = serv.id_cupon_servicio','left')

            ->select('u.nombre, u.apellido_paterno, u.apellido_materno, scu.nomenclatura, serv.id_usuario, serv.total, serv.id, s.servicioNombre, serv.id_servicio, serv.id_cupon_servicio, cup.cupon, cup.descuento ')

            ->order_by('serv.id',"DESC")
            ->limit(1)
            ->get('servicio_lavado AS serv')
            ->result();
            //echo $this->db->last_query();die();
        return $result;
    }

    public function existencia_evidencia($id_servicio='')
    {
        $q = $this->db->where('id_servicio',$id_servicio)
        ->like('titulo',"Evidencia de pago")
        ->select('id')
        ->limit(1)
        ->get('evidencia_proceso_servicio');

        if($q->num_rows()==1){
            $retorno = '1';
        }else{
            $retorno = '0';
        }
        return $retorno;
    }

}