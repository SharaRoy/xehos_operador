<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class ApiChat extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mgeneral', '', TRUE);
        $this->load->helper(array('form', 'html', 'validation', 'url'));

        date_default_timezone_set(CONST_TIMEZONE);
    }

    public function index()
    {
        $data['titulo'] = "Panel";
        $data['titulo_dos'] = "Usuarios del sistema";
        $data['usuarios'] = $this->db->where('telefono !=', null)->get('admin')->result();

        $data['id'] = $this->input->get('id');
        $data['datos']  = $this->db->where('id', $data['id'])->get('usuarios')->row_array();

        $this->blade->render('sistema/panel', $data);
    }

    public function  chat()
    {
        $data['titulo'] = "Sistema";
        $data['titulo_dos'] = "Chat";
        $data['telefono_cliente'] = $this->input->get('telefono_cliente');
        $data['telefono_destinatario'] = $this->input->get('telefono_destinatario');

        $this->blade->render('sistema/chat_cliente', $data);
    }

    public function getTotalNotificaciones()
    {
        try {

            $telefono = $this->input->get('telefono');
            $dataNotificacion = $this->curlGet('https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/contador_notificaciones/' . $telefono, false, false);
            $notificaciones = json_decode($dataNotificacion);
         
            return print_r(json_encode($notificaciones));
        } catch (Exception $e) {
            return $e;
        }
    }

    public function getUltimaNotificacion()
    {
        try {

            $telefono = $this->input->get('telefono');
            $usuarios_admin = $this->db->select('adminNombre as nombre, telefono, "Operador" as tipo')->where('telefono !=', null)->get('admin')->result_array();
            $usuarios_cliente = $this->db->select('nombre, telefono, "Cliente" as tipo')->where('telefono !=', null)->get('usuarios')->result_array();
            $usuarios_recomendador = $this->db->select('nombre, celular as telefono, "Recomendador" as tipo')->where('celular !=', null)->get('recomendadores')->result_array();
            $usuarios_lavadores = $this->db->select('lavadorNombre as nombre, lavadorTelefono as telefono, "Lavador" as tipo')->where('lavadorTelefono !=', null)->get('lavadores')->result_array();
            $usuarios = array_merge($usuarios_admin, $usuarios_cliente, $usuarios_recomendador, $usuarios_lavadores);
         
            $dataNotificacion = $this->curlGet('https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/ultima_notificacion/' . $telefono, false, false);
            $notificaciones = json_decode($dataNotificacion);

            foreach($usuarios as $user) {
                if ($user['telefono'] == $notificaciones->celular2){
                    $data = [
                        'usuario' => $user['nombre'],
                        'tipo' => $user['tipo'],
                        'mensaje' => $notificaciones->mensaje
                    ];
                }
            }
     
            return print_r(json_encode($data));
        } catch (Exception $e) {
            return $e;
        }
    }

    public function getNotificaciones()
    {
        try {
            $telefono = $this->input->get('telefono');

            $usuarios_admin = $this->db->select('adminNombre as nombre, telefono, "Operador" as tipo')->where('telefono !=', null)->get('admin')->result_array();
            $usuarios_cliente = $this->db->select('nombre, telefono, "Cliente" as tipo')->where('telefono !=', null)->get('usuarios')->result_array();
            $usuarios_recomendador = $this->db->select('nombre, celular as telefono, "Recomendador" as tipo')->where('celular !=', null)->get('recomendadores')->result_array();
            $usuarios_lavadores = $this->db->select('lavadorNombre as nombre, lavadorTelefono as telefono, "Lavador" as tipo')->where('lavadorTelefono !=', null)->get('lavadores')->result_array();
            $usuarios = array_merge($usuarios_admin, $usuarios_cliente, $usuarios_recomendador, $usuarios_lavadores);
            // echo '<pre>'; print_r($usuarios); exit();

            $dataNotificacion = $this->curlGet('https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/lista_notificaciones/' . $telefono, false, false);
            $notificaciones = json_decode($dataNotificacion)->data;
            $listado = [];
            foreach ($notificaciones as $notify) {
                //if ($notify->status != 1) {
                $notify->usuario = null;
                foreach ($usuarios as $user) {
                    if ($notify->celular2 == $user['telefono']) {
                        $notify->usuario = ($user['nombre']);
                        $notify->tipoUsuario = ($user['tipo']);
                    }
                }
                array_push($listado, $notify);
                //}
            }
            return print_r(json_encode($listado));
        } catch (Exception $e) {
            return $e;
        }
    }

    public function getPrueba() {
        return print_r(json_encode("op"));
    }

    public function getchat()
    {
        try {
            $telefono1 = $this->input->get('telefono1');
            $telefono2 = $this->input->get('telefono2');

            $dataNotificacion = $this->curlPost('https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/notificaciones_busqueda', [
                'telefono' => $telefono1,
                'celular2' => $telefono2
            ], false);
            $notificaciones = json_decode($dataNotificacion)->data;
            $convArrayKardex = json_decode(json_encode($notificaciones), true);
            usort($convArrayKardex, function ($b, $a) {
                return strcmp($b["fecha_creacion"], $a["fecha_creacion"]);
            });

            return print_r(json_encode($convArrayKardex));
        } catch (Exception $e) {
            return $e;
        }
    }

    public function apiNotificacion()
    {
        try {
          //echo '<pre>'; print_r($_GET); exit();
            $dataNotificacion = $this->curlPost('https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/enviar_notificacion', [
                'celular' => $this->input->get('telefono'),
                'mensaje' => $this->eliminar_tildes($this->input->get('mensaje')),
                'sucursal' => 'M2137',
                'celular2' => $this->input->get('celular2'),
                'mensaje_respuesta' => $this->input->get('mensaje_respuesta')
            ], false);

            return print_r(json_encode($dataNotificacion));
        } catch (Exception $e) {
            return $e;
        }
    }

    private function curlPost($url = '', $data, $is_json_request = false)
    {
        $curl = curl_init();
        $final_url =  $url;
        $parametros = is_array($data) && !$is_json_request  ? http_build_query($data) : json_encode($data);

        curl_setopt_array($curl, array(
            CURLOPT_URL => $final_url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_HEADER => true,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $parametros,
        ));

        $body = curl_exec($curl);
        // extract header
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        $headerSize = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
        $header = substr($body, 0, $headerSize);
        $header = @$this->getHeaders($header);

        // extract body
        $body = substr($body, $headerSize);
        curl_close($curl);

        if ($httpcode == 400 && isset($header) && count($header) > 0) {
            return [
                'status_code' => $httpcode,
                'data' => $header['X-Message']
            ];
        }

        return $body;
    }

    public function curlGet($url = '', $data, $is_json_request = false)
    {
        $curl = curl_init();
        $final_url =  $url;
        $parametros = is_array($data) && !$is_json_request  ? http_build_query($data) : json_encode($data);
        curl_setopt_array($curl, array(
            CURLOPT_URL => $final_url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_HEADER => true,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => $parametros,
        ));
        $body = curl_exec($curl);
        // extract header
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        $headerSize = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
        $header = substr($body, 0, $headerSize);
        $header = @$this->getHeaders($header);

        // extract body
        $body = substr($body, $headerSize);
        curl_close($curl);

        if ($httpcode == 400 && isset($header) && count($header) > 0) {
            return [
                'status_code' => $httpcode,
                'data' => $header['X-Message']
            ];
        }

        return $body;
    }


    function getHeaders($respHeaders)
    {
        $headers = array();
        $headerText = substr($respHeaders, 0, strpos($respHeaders, "\r\n\r\n"));


        foreach (explode("\r\n", $headerText) as $i => $line) {
            if ($i === 0) {
                $headers['http_code'] = $line;
            } else {
                list($key, $value) = explode(': ', $line);
                if ($key == 'X-Message') {
                    $headers[$key] = $value;
                }
            }
        }

        return $headers;
    }

    private function eliminar_tildes($cadena)
    {

        $cadena = ($cadena);

        $cadena = str_replace(
            array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
            array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
            $cadena
        );

        $cadena = str_replace(
            array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
            array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
            $cadena
        );

        $cadena = str_replace(
            array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
            array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
            $cadena
        );

        $cadena = str_replace(
            array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
            array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
            $cadena
        );

        $cadena = str_replace(
            array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
            array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
            $cadena
        );

        $cadena = str_replace(
            array('ñ', 'Ñ', 'ç', 'Ç'),
            array('n', 'N', 'c', 'C'),
            $cadena
        );

        return $cadena;
    }
}
