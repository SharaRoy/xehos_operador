<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class ChatLavador extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mgeneral', '', TRUE);
        $this->load->helper(array('form', 'html', 'validation', 'url'));

        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'validation', 'url'));

        date_default_timezone_set(CONST_TIMEZONE);

        if ($this->session->userdata('id_usuario')) {
        } else {
            redirect('login/');
        }
    }

    public function index()
    {
        $data['titulo'] = "Chat";
        $data['titulo_dos'] = "Usuarios del sistema";
        $data['vista'] = "b";

        if ($this->session->userdata('id_usuario')) {
            $data['id'] = $this->session->userdata('id_usuario');
            $telefono = $this->session->userdata('telefono');

            $operadores = $this->db->select('admin.*, ca_cargos.nombre as cargo')->where('telefono !=', null)->join('ca_cargos', 'admin.id_cargo = ca_cargos.id')->get('admin')->result();

            $clientes = $this->db
                ->select('usuarios.*, servicio_lavado.id as servicio_id, servicio_lavado.id_estatus_lavado, servicio_lavado.id_lavador')
                ->join('servicio_lavado', 'usuarios.id = servicio_lavado.id_usuario')
                ->where('usuarios.telefono !=', null)
                ->where_in('servicio_lavado.id_estatus_lavado', [2, 3, 4, 5])
                ->where('servicio_lavado.id_lavador', $data['id'])
                ->get('usuarios')->result();

            $data['datos']  = $this->db->where('lavadorId', $data['id'])->get('lavadores')->row_array();

            $dataNotificacion = $this->curlGet('https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/lista_notificaciones/' . $telefono, false, false);
            $notificaciones = json_decode($dataNotificacion)->data;
            // echo '<pre>'; print_r($notificaciones); exit();
            $listadooperadores = [];
            if (is_array($operadores)) {
                foreach ($operadores as $operador) {
                    $operador->notificacion = false;
                    foreach ($notificaciones as $notify) {
                        if ($notify->status != 1) {
                            if ($notify->celular2 == $operador->telefono) {
                                $operador->notificacion = true;
                            }
                        }
                    }
                    array_push($listadooperadores, $operador);
                }
            }

            $listadoclientes = [];
            if (is_array($clientes)) {
                foreach ($clientes as $cliente) {
                    $cliente->notificacion = false;
                    foreach ($notificaciones as $notify) {
                        if ($notify->status != 1) {
                            if ($notify->celular2 == $cliente->telefono) {
                                $cliente->notificacion = true;
                            }
                        }
                    }
                    array_push($listadoclientes, $cliente);
                }
            }

        }

        $data['operadores'] = $listadooperadores;
        $data['clientes'] = $listadoclientes;


        $this->blade->render('webview/panel_chat', $data);
    }

    public function curlGet($url = '', $data, $is_json_request = false)
    {
        $curl = curl_init();
        $final_url =  $url;
        $parametros = is_array($data) && !$is_json_request  ? http_build_query($data) : json_encode($data);
        curl_setopt_array($curl, array(
            CURLOPT_URL => $final_url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_HEADER => true,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => $parametros,
        ));
        $body = curl_exec($curl);
        // extract header
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        $headerSize = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
        $header = substr($body, 0, $headerSize);
        $header = @$this->getHeaders($header);

        // extract body
        $body = substr($body, $headerSize);
        curl_close($curl);

        if ($httpcode == 400 && isset($header) && count($header) > 0) {
            return [
                'status_code' => $httpcode,
                'data' => $header['X-Message']
            ];
        }

        return $body;
    }


    function getHeaders($respHeaders)
    {
        $headers = array();
        $headerText = substr($respHeaders, 0, strpos($respHeaders, "\r\n\r\n"));


        foreach (explode("\r\n", $headerText) as $i => $line) {
            if ($i === 0) {
                $headers['http_code'] = $line;
            } else {
                list($key, $value) = explode(': ', $line);
                if ($key == 'X-Message') {
                    $headers[$key] = $value;
                }
            }
        }

        return $headers;
    }
    public function  chat()
    {
        $data['titulo'] = "Chat";
        $data['titulo_dos'] = "Conversación";

        $data['vista'] = "3";
        $data['ruta_anterior'] = "operadores/chatlavador/index";
        $data['servicio_ruta'] = "";

        $telefono1 = $this->input->get('telefono_remitente');
        $telefono2 = $this->input->get('telefono_destinatario');

        $data['telefono_remitente'] = $telefono1;
        $data['telefono_destinatario'] = $telefono2;
        $data['remitente'] = $this->getUserTelefono($telefono1);
        $data['destinatario'] = $this->getUserTelefono($telefono2);
        $data['template2'] = "layoutuser";

        $this->blade->render('webview/chat_webView', $data);
    }

    public function getUserTelefono($telefono)
    {
        $usuarios_admin = $this->db->select('adminNombre as nombre, telefono')->where('telefono', $telefono)->get('admin')->result_array();
        $usuarios_cliente = $this->db->select('nombre, telefono')->where('telefono', $telefono)->get('usuarios')->result_array();
        $usuarios_recomendador = $this->db->select('nombre, celular as telefono')->where('celular', $telefono)->get('recomendadores')->result_array();
        $usuarios_lavadores = $this->db->select('lavadorNombre as nombre, lavadorTelefono as telefono, "Lavador" as tipo')->where('lavadorTelefono', $telefono)->get('lavadores')->result_array();
        $usuarios = array_merge($usuarios_admin, $usuarios_cliente, $usuarios_recomendador, $usuarios_lavadores);

        if (is_array($usuarios)) {
            return current($usuarios)['nombre'];
        }
    }
}
