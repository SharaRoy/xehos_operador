<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Operadores extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_Operador', 'consulta', TRUE);
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'validation', 'url'));

        date_default_timezone_set(CONST_TIMEZONE);

        if ($this->session->userdata('id_usuario')) {
        } else {
            redirect('login/');
        }
    }

    #----------------------------
    public function index()
    {
        $data['titulo'] = "";
        $data['titulo_dos'] = "";

        $data['vista'] = "1";

        if ($this->session->userdata('id_usuario')) {
            $lavador = $this->session->userdata('id_usuario');
            //Revisamos si ya se realizo el inventario de la unidad del lavador 
            $data["inventario"] = $this->consulta->buscar_inventario($lavador);            
            $data["servicios"] = $this->consulta->servicios_lavador($lavador);
            $data["serv_activo"] = $this->consulta->servicio_activo($lavador);
        } 
        
        $this->blade->render('principal',$data);
    }

    public function ver_servicio($servicio_ec='')
    {
        $data["servicioId"] = ((ctype_digit($servicio_ec)) ? $servicio_ec : $this->decrypt($servicio_ec));

        $data['titulo'] = "";
        $data['titulo_dos'] = "";
        $data['vista'] = "2";

        if ($this->session->userdata('id_usuario')) {
            $servicio = $this->consulta->datos_servicio($data["servicioId"]);
            $data["servicio"] = ((count($servicio)>0) ? $servicio[0] : $servicio);

            $avance_proceso = $this->consulta->pasos_proceso($data["servicioId"]);

            if ($avance_proceso != NULL) {
                $data["paso"] = $avance_proceso[0];
            }
        } 

        $this->blade->render('info_servicio',$data);
    }

    public function editar_vehiculo($servicio_ec='')
    {
        $data["servicioId"] = ((ctype_digit($servicio_ec)) ? $servicio_ec : $this->decrypt($servicio_ec));

        $data['titulo'] = "";
        $data['titulo_dos'] = "";
        $data['vista'] = "3";
        $data['ruta_anterior'] = "operadores/ver_servicio";
        $data['servicio_ruta'] = $servicio_ec;

        if ($this->session->userdata('id_usuario')) { 
            //Recuperamos los datos del vehiculo
            $data["servicio"] = $this->consulta->datos_servicio($data["servicioId"]); 

            $avance_proceso = $this->consulta->pasos_proceso($data["servicioId"]);

            if ($avance_proceso != NULL) {
                $data["paso"] = $avance_proceso[0];
            }
        } 

        $this->blade->render('datos_vehiculo',$data);
    }

    public function act_datos_vehiculo() 
    {
        $contenido = array(
            "placas" => strtoupper($_POST["placas"]),
            "numero_serie" => strtoupper($_POST["serie"]),
        );

        $actualizar = $this->consulta->update_table_row('autos',$contenido,'id',$_POST["auto"]);
        if ($actualizar) {
            $respuesta = "OK=".$this->encrypt($_POST["servicio"]);
        } else {
            $respuesta = "NO SE PUDO GUARDAR EL FORMULARIO=".$this->encrypt($_POST["servicio"]);
        }

        echo $respuesta;
    }

    public function cancelar_servicio($servicio_ec='') 
    {
        $data["servicioId"] = ((ctype_digit($servicio_ec)) ? $servicio_ec : $this->decrypt($servicio_ec));

        $data['titulo'] = "";
        $data['titulo_dos'] = "";
        $data['vista'] = "3";
        $data['ruta_anterior'] = "operadores/ver_servicio";
        $data['servicio_ruta'] = $servicio_ec;

        if ($this->session->userdata('id_usuario')) { 
            $avance_proceso = $this->consulta->pasos_proceso($data["servicioId"]);

            if ($avance_proceso != NULL) {
                $data["paso"] = $avance_proceso[0];
            }

            //Recuperamos los datos del vehiculo
            $servicio = $this->consulta->datos_servicio($data["servicioId"]); 
            $data["servicio"] = ((count($servicio)>0) ? $servicio[0] : $servicio);
        } 

        $this->blade->render('cancelar_servicio',$data);
    }

    public function act_cancelar_servivio()
    {
        $motivo = $_POST["motivos"];
        $lavador = $this->session->userdata('id_usuario');

        //Actualizamos en la tabla de servicio
        $cancelado = array(
            "cancelado" => 1
        );
        $this->consulta->update_table_row('servicio_lavado',$cancelado,'id',$_POST["servicio"]);

        //Marcamos el paso
        $id_paso = $this->consulta->id_paso_seguimiento($_POST["servicio"],$lavador);

        $proceso = array(
            "servicio_cancelado" => 1,
            "fecha_actualiza" => date("Y-m-d H:i:s")
        );

        $this->consulta->update_table_row('proceso_servicio_lavado',$proceso,'id',$id_paso);

        $this->guardar_movimiento($_POST["servicio"],"Servicio cancelado: ".$motivo);

        echo "OK";
    }

    public function inicar_ruta($servicio_ec='')
    {
        $data["servicioId"] = ((ctype_digit($servicio_ec)) ? $servicio_ec : $this->decrypt($servicio_ec));
        $data['titulo'] = "";
        $data['titulo_dos'] = "";
        
        $data['vista'] = "3";
        $data['ruta_anterior'] = "operadores/ver_servicio";
        $data['servicio_ruta'] = $servicio_ec;

        if ($this->session->userdata('id_usuario')) {
            $data["lavador"] = $this->session->userdata('id_usuario');
            $avance_proceso = $this->consulta->pasos_proceso($data["servicioId"]);

            if ($avance_proceso != NULL) {
                $data["paso"] = $avance_proceso[0];
            }

            if (!isset($data["paso"]->inicia_ruta)) {
                //Verificamos si el servicio esta pagado
                $tipo_pago = $this->consulta->tipo_pago($data["servicioId"]);

                $validar_seguro = $this->validar_seguro_lluvia($data["servicioId"]);
                
                //Si el servicio ya esta pagado y no es pago por terminal
                $pago = 0;
                $seguro_lluvia = 0;
                if (($tipo_pago[0] != NULL)&&($tipo_pago[0] != 2)) {
                    $pago = 1;
                    if ($validar_seguro === TRUE) {
                        $seguro_lluvia = 1;
                    }
                }

                $proceso = array(
                    "id" => NULL,
                    "id_servicio" => $data["servicioId"],
                    "id_lavador" => $data["lavador"],
                    "inicia_ruta" => 1,
                    "pago" => $pago,
                    "evidencia_pago" => $pago,
                    "seguro_lluvia" => $seguro_lluvia,
                    "fecha_alta" => date("Y-m-d H:i:s"),
                    "fecha_actualiza" => date("Y-m-d H:i:s")
                );

                $this->consulta->save_register('proceso_servicio_lavado',$proceso);

                $this->guardar_movimiento($data["servicioId"],"Se inicia la ruta");

                //Cambiamos el estatus
                $this->cambio_status('2',$data["servicioId"]);
                
                //Recargamos los valores
                $avance_proceso = $this->consulta->pasos_proceso($data["servicioId"]);

                if ($avance_proceso != NULL) {
                    $data["paso"] = $avance_proceso[0];
                }
            } 

            $data["servicio"] = $this->consulta->datos_servicio($data["servicioId"]);
        } 

        $this->blade->render('info_ruta',$data);
    }

    public function seguimiento_ruta()
    {
        $id_servicio = $_POST["indice"];
        $movimiento = $_POST["tipo"];
        $lavador = $this->session->userdata('id_usuario');
        $id_paso = $this->consulta->id_paso_seguimiento($id_servicio,$lavador);

        if ($movimiento == "1") {
            $proceso = array(
                "termina_ruta" => 1,
                "fecha_actualiza" => date("Y-m-d H:i:s")
            );

            $this->consulta->update_table_row('proceso_servicio_lavado',$proceso,'id',$id_paso);

            //Guardamos el movimiento del servicio
            $this->guardar_movimiento($id_servicio,"Finaliza la ruta / Se llego al sitio");

            //Cambiamos el estatus
            $this->cambio_status('3',$id_servicio);

            echo $this->encrypt($id_servicio);

        } elseif ($movimiento == "2") {
            $proceso = array(
                "pausar_ruta" => 1,
                "fecha_actualiza" => date("Y-m-d H:i:s")
            );

            $this->consulta->update_table_row('proceso_servicio_lavado',$proceso,'id',$id_paso);

            //Guardamos el movimiento del servicio
            $this->guardar_movimiento($id_servicio,"Ruta pausada");

            echo "OK";

        } elseif ($movimiento == "4") {
            $proceso = array(
                "reanudar_ruta" => 1,
                "fecha_actualiza" => date("Y-m-d H:i:s")
            );

            $this->consulta->update_table_row('proceso_servicio_lavado',$proceso,'id',$id_paso);

            //Guardamos el movimiento del servicio
            $this->guardar_movimiento($id_servicio,"Ruta Reanudada");
            echo "OK";

        } else {
            $proceso = array(
                "servicio_cancelado" => 1,
                "fecha_actualiza" => date("Y-m-d H:i:s")
            );

            $this->consulta->update_table_row('proceso_servicio_lavado',$proceso,'id',$id_paso);

            //Guardamos el movimiento del servicio
            $this->guardar_movimiento($id_servicio,"Se cancelo la ruta / servicio");
            echo "OK";
        }
    }

    public function grabar_posicion()
    {
        $id_servicio = $_POST["indice"];
        $lat_3 = $_POST["lat_3"];
        $lng_3 = $_POST["lng_3"];
        $lavador = $this->session->userdata('id_usuario');

        //Gradamos la coordenada
        $proceso = array(
            "id" => NULL,
            "id_lavador" => $lavador,
            "latitud" => $lat_3,
            "longitud" => $lng_3,
            "id_servicio" => $id_servicio,
            "fecha_creacion" => date("Y-m-d H:i:s")
        );

        $this->consulta->save_register('coordenadas_lavador',$proceso);

        //Actualizamos la coordenada del lavador
        $posicion_lavador = array(
            "latitud_lavador" => $lat_3,
            "longitud_lavador" => $lng_3,
        );

        $this->consulta->update_table_row('servicio_lavado',$posicion_lavador,'id',$id_servicio);

        echo "OK";
    }

    public function inicia_servicio($servicio_ec='')
    {
        $data["servicioId"] = ((ctype_digit($servicio_ec)) ? $servicio_ec : $this->decrypt($servicio_ec));

        $data['titulo'] = "";
        $data['titulo_dos'] = "";
        
        $data['vista'] = "3";
        $data['ruta_anterior'] = "operadores/ver_servicio";
        $data['servicio_ruta'] = $servicio_ec;

        if ($this->session->userdata('id_usuario')) {
            //Recuperamos los pasos del proceso completados
            $avance_proceso = $this->consulta->pasos_proceso($data["servicioId"]);
            if ($avance_proceso != NULL) {
                $data["paso"] = $avance_proceso[0];
            }

            //Datos adicionales para la distribucion
            $lavador = $this->session->userdata('id_usuario');
            $tipo_pago = $this->consulta->tipo_pago($data["servicioId"]);

            //Identificamos el tipo de evidencia a guardar
            if ($data["paso"]->evidencia_pago == "0") {
                $data['evidencia'] = "Evidencia: Comprobante de pago";
                $data['titulo'] = "Evidencia de pago";
                $data['paso_evidencia'] = "4";

                $data["evidencia_prev"] = $this->consulta->existencia_evidencia($data["servicioId"]);

                //Recuperamos datos de facturacion
                $facturacion = $this->consulta->datos_facturacion($data["servicioId"]);
                
                //if ($facturacion != NULL) {
                if (count($facturacion) > 0) {
                    $data["facturacion"] = $facturacion[0];
                }else{
                    $data["facturacion"] = $this->facturacion_generica($data["servicioId"]);
                }

            }elseif (($data["paso"]->evidencia_pago == "1")&&($data["paso"]->evidencia_inicio == "0")) {
                $data['evidencia'] = "Evidencia: Estado inicial de vehículo";
                $data['titulo'] = "Estado inicial del vehículo";
                $data['paso_evidencia'] = "1";

                //Cambiamos el estatus
                $this->cambio_status('4',$data["servicioId"]);

            }elseif (($data["paso"]->evidencia_inicio == "1")&&($data["paso"]->evidencia_lavado == "0")) {
                $data['evidencia'] = "Evidencia: Proceso de lavado";
                $data['titulo'] = "Proceso de lavado";
                $data['paso_evidencia'] = "2";

            }elseif (($data["paso"]->evidencia_lavado == "1")&&($data["paso"]->evidencia_finaliza == "0")) {
                $data['evidencia'] = "Evidencia: Pre-entrega";
                $data['titulo'] = "Lavado terminado";
                $data['paso_evidencia'] = "3";
            
            }elseif (($data["paso"]->evidencia_finaliza == "1")&&($data["paso"]->evidencia_pago == "0")) {
                $data['evidencia'] = "Evidencia: Comprobante de pago";
                $data['titulo'] = "Evidencia de pago";
                $data['paso_evidencia'] = "4";

                $data["evidencia_prev"] = $this->consulta->existencia_evidencia($data["servicioId"]);

                //Recuperamos datos de facturacion
                $facturacion = $this->consulta->datos_facturacion($data["servicioId"]);
                if ($facturacion != NULL) {
                    $data["facturacion"] = $facturacion[0];
                }else{
                    $data["facturacion"] = $this->facturacion_generica($data["servicioId"]);
                }
            
            }elseif (($data["paso"]->evidencia_pago == "1")&&($data["paso"]->termina_servicio == "0")) {
                $data['evidencia'] = "Evidencia completada";
                $data['titulo'] = "Estado final del vehículo";
                $data['paso_evidencia'] = "5";

                //Cambiamos el estatus
                $this->cambio_status('5',$data["servicioId"]);

            }elseif (($data["paso"]->termina_servicio == "1")&&($data["paso"]->finalizado == "0")) {
                $data['vista'] = "2";
                $data['evidencia'] = "Servicio completado";
                $data['titulo'] = "Servicio terminado";
                $data['paso_evidencia'] = "6";

                //Cambiamos el estatus
                $this->cambio_status('6',$data["servicioId"]);

                $id_paso = $this->consulta->id_paso_seguimiento($data["servicioId"],$lavador);
                $proceso = array(
                    "finalizado" => 1,
                    "fecha_actualiza" => date("Y-m-d H:i:s")
                );

                $this->consulta->update_table_row('proceso_servicio_lavado',$proceso,'id',$id_paso);

                //Guardamos el movimiento del servicio
                $this->guardar_movimiento($data["servicioId"],"Finaliza la entrega de la unidad");
            
            }else{
                $data['vista'] = "2";
                $data['evidencia'] = "Servicio completado";
                $data['titulo'] = "Servicio terminado";
                $data['paso_evidencia'] = "6";
            }

            $data["servicio"] = $this->consulta->datos_servicio($data["servicioId"]);
        } 

        $this->blade->render('evidencia_servicio',$data);
    }

    public function facturacion_generica($servicioId = 0)
    {
        //Recuperamos algunos datos de facturacion 
        $facturacion = $this->consulta->datos_factura_gen($servicioId);
        if ($facturacion != NULL) {
            $datos = array(
                "razon_social" => "General",
                "id_usuario" => $facturacion[0]->id_usuario,
                "cp" => "28983",
                "email_facturacion" => "jalomo@hotmail.es",
                "nomenclatura" => $facturacion[0]->nomenclatura,
                "id" => $facturacion[0]->id,
                "domicilio" => "colonia lejos",
                /*"factura_formaPago" => "04",
                "factura_medotoPago" => "PUE",*/
                "rfc" => "EKU9003173C9",
                "clave" => "G01",
                //"concepto_NoIdentificacion" => "1",
                "servicioNombre" => $facturacion[0]->servicioNombre,
                "total" => $facturacion[0]->total,
                "id_servicio" => $facturacion[0]->id_servicio,
                "id_cupon_servicio" => $facturacion[0]->id_cupon_servicio,
                "cupon" => $facturacion[0]->cupon,
                "descuento" => $facturacion[0]->descuento,
            );
        }else{
            $datos = NULL;
        }
        return (object)$datos;
    }

    /********** Proceso de evidencia *******************/
    public function guardar_evidencia()
    {
        $lavador = $this->session->userdata('id_usuario');

        if(count($_FILES['archivo']['tmp_name']) > 0){
            //Como el elemento es un arreglos utilizamos foreach para extraer todos los valores
            for ($i = 0; $i < count($_FILES['archivo']['tmp_name']); $i++) {
                $_FILES['tempFile']['name'] = $_FILES['archivo']['name'][$i];
                $_FILES['tempFile']['type'] = $_FILES['archivo']['type'][$i];
                $_FILES['tempFile']['tmp_name'] = $_FILES['archivo']['tmp_name'][$i];
                $_FILES['tempFile']['error'] = $_FILES['archivo']['error'][$i];
                $_FILES['tempFile']['size'] = $_FILES['archivo']['size'][$i];

                //Url donde se guardara la imagen
                $urlDoc = "videos";
                //Generamos un nombre random
                $str = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                $urlNombre = date("YmdHi")."_";
                for($j=0; $j<=7; $j++ ){
                    $urlNombre .= substr($str, rand(0,strlen($str)-1) ,1 );
                }

                //Validamos que exista la carpeta destino   base_url()
                if(!file_exists($urlDoc)){
                    mkdir($urlDoc, 0647, true);
                }

                //Configuramos las propiedades permitidas para la imagen
                $config['upload_path'] = $urlDoc;
                $config['file_name'] = $urlNombre;
                $config['allowed_types'] = "*";

                //Cargamos la libreria y la inicializamos
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('tempFile')) {
                    $data['uploadError'] = $this->upload->display_errors();
                    // echo $this->upload->display_errors();
                    //$respuesta = "NO SE PUDIERON CARGAR TODOS LOS ARCHIVOS";
                    $respuesta = "";
                }else{
                    //Si se pudo cargar la imagen la guardamos
                    $fileData = $this->upload->data();
                    $ext = explode(".",$_FILES['tempFile']['name']);
                    $path = $urlDoc."/".$urlNombre.".".$ext[count($ext)-1];

                    $evidencia = array(
                        "id" => NULL,
                        "id_servicio" => $_POST["servicio"],
                        "id_lavador" => $lavador,
                        "repositorio" => "2",
                        "titulo" => ((isset($_POST["tt_evidencia"])) ? $_POST["tt_evidencia"] : "Evidencia Lavado"),
                        "comentarios" => $_POST["comentarios"],
                        "url" => $path,
                        "activo" => 1,
                        "fecha_alta" => date("Y-m-d H:i:s")
                    );

                    $servicio = $this->consulta->save_register('evidencia_proceso_servicio',$evidencia);

                    $respuesta = "OK";
                }
            }
        }else{
            $respuesta = "SIN ARCHIVOS";
        }

        if (($respuesta == "OK")&&($_POST["paso_evidencia"] == "1")) {
            $id_paso = $this->consulta->id_paso_seguimiento($_POST["servicio"],$lavador);

            $proceso = array(
                "evidencia_inicio" => 1,
                "fecha_actualiza" => date("Y-m-d H:i:s")
            );

            $this->consulta->update_table_row('proceso_servicio_lavado',$proceso,'id',$id_paso);

            $this->guardar_movimiento($_POST["servicio"],"Evidencia: ".$_POST["tt_evidencia"]);
        }

        echo $respuesta;
    } 

    public function avance_evidencia()
    {
        $paso_evidencia = $_POST["evidencia"];
        $servicio = $_POST["indice"];
        $lavador = $this->session->userdata('id_usuario');
        $id_paso = $this->consulta->id_paso_seguimiento($servicio,$lavador);

        if ($paso_evidencia == "2") {
            $proceso = array(
                "evidencia_lavado" => 1,
                "fecha_actualiza" => date("Y-m-d H:i:s")
            );

            $this->consulta->update_table_row('proceso_servicio_lavado',$proceso,'id',$id_paso);

            //Guardamos el movimiento del servicio
            $this->guardar_movimiento($servicio,"Finaliza la carga de evidencias del proceso de lavado");

        }elseif ($paso_evidencia == "3") {
            $proceso = array(
                "evidencia_finaliza" => 1,
                "fecha_actualiza" => date("Y-m-d H:i:s")
            );

            $this->consulta->update_table_row('proceso_servicio_lavado',$proceso,'id',$id_paso);

            //Guardamos el movimiento del servicio
            $this->guardar_movimiento($servicio,"Finaliza la carga de evidencias de entrega de unidad");
        }elseif ($paso_evidencia == "4") {
            $proceso = array(
                "pago" => 1,
                "evidencia_pago" => 1,
                "fecha_actualiza" => date("Y-m-d H:i:s")
            );

            $this->consulta->update_table_row('proceso_servicio_lavado',$proceso,'id',$id_paso);

            //Guardamos el movimiento del servicio
            $this->guardar_movimiento($servicio,"Finaliza toma de evidencia del comprobante de pago");

            //Actualizamos el historial de pagos
            $this->actualizar_pago($servicio);

        }elseif ($paso_evidencia == "5") {
            $proceso = array(
                "termina_servicio" => 1,
                "fecha_actualiza" => date("Y-m-d H:i:s")
            );

            $this->consulta->update_table_row('proceso_servicio_lavado',$proceso,'id',$id_paso);

            //Guardamos el movimiento del servicio
            $this->guardar_movimiento($servicio,"Finaliza el servicio");
        }

        echo "OK";
    }

    public function listar_evidencia($servicio_ec='')
    {
        $data["servicioId"] = ((ctype_digit($servicio_ec)) ? $servicio_ec : $this->decrypt($servicio_ec));

        $data['titulo'] = "";
        $data['titulo_dos'] = "";
        
        $data['vista'] = "3";
        $data['ruta_anterior'] = "operadores/inicia_servicio";
        $data['servicio_ruta'] = $servicio_ec;

        if ($this->session->userdata('id_usuario')) {
            $avance_proceso = $this->consulta->pasos_proceso($data["servicioId"]);

            if ($avance_proceso != NULL) {
                $data["paso"] = $avance_proceso[0];
            }

            $lavador = $this->session->userdata('id_usuario');

            $data["evidencias"] = $this->consulta->evidencia_servicio($data["servicioId"],$lavador);
            //$data["array_imagen"] = ['jpg','jpeg','gif','png','tiff','tif','raw','bmp','psd','ico'];
        } 
        
        $this->blade->render('lista_evidencia',$data);
    }

    public function ver_evidencia($id_evidencia='')
    {
        $data["evidencia_id"] = ((ctype_digit($id_evidencia)) ? $id_evidencia : $this->decrypt($id_evidencia));

        //Recuperamos los datos de la evidencia
        $revision = $this->consulta->get_result("id",$data["evidencia_id"],"evidencia_proceso_servicio");
        foreach ($revision as $row) {
            $data["evidencia_titulo"] = $row->titulo;
            $data["evidencia_comentarios"] = $row->comentarios;
            $data["evidencia_url"] = $row->url;
            $extension = explode(".", $row->url);
            $data["extension"] = strtoupper($extension[count($extension)-1]);
            $data["evidencia_fecha_alta"] = $row->fecha_alta;
            $servicio_ec = $this->encrypt($row->id_servicio);
        }

        $data["array_imagen"] = ["JPEG","JPG","PNG","BMP","ICO","SVG","WEBP","GIF","PSD","HEIC","NEF/CRW","AI","ID","PSD","RAW","TIF","TIFF"];
        $data["array_doc"] = ["TXT","DOC","DOCX","DOCM","ODT","PDF","RTF","CSV","XLS","XLSX","XLSM","ODS","PPS","PPT","PPSX","PPTX","PPSM","PPTM","POTX","ODP"];
        $data["array_audio"] = ["MP3","WMA","WAV","FLAC","MIDI","OGG","M3U"];
        $data["array_video"] = ["AVI","DIVX","MOV","MP4","MPG","MKV","WMV","WPL"];

        $data['titulo'] = "";
        $data['titulo_dos'] = "";
        
        $data['vista'] = "3";
        $data['ruta_anterior'] = "operadores/listar_evidencia";
        $data['servicio_ruta'] = $servicio_ec;

        $avance_proceso = $this->consulta->pasos_proceso($servicio_ec);

        if ($avance_proceso != NULL) {
            $data["paso"] = $avance_proceso[0];
        }

        $this->blade->render('ver_evidencia_pg',$data);
    }

    /********** Fin Proceso de evidencia *******************/

    #----------------------------Vistas y procesos del menu de webview
    public function webviewlist()
    {
        $data['titulo'] = "";
        $data['titulo_dos'] = "";
        $data['vista'] = "2";
        
        $this->blade->render('webview/webviewlist',$data);
    }

    public function chat()
    {
        $data['titulo'] = "";
        $data['titulo_dos'] = "";
        $data['vista'] = "b";

        if ($this->session->userdata('id_usuario')) {
            //Cambiamos el estatus
            //  Pendiente
            $data["lavador"] = $this->session->userdata('id_usuario');
            $data["id_cliente"] = $this->consulta->recuperar_cliente($data["lavador"]);
        } 

        $this->blade->render('webview/chat',$data);
    }

    public function agendar_cita()
    {
        $data['titulo'] = "";
        $data['titulo_dos'] = "";
        $data['vista'] = "b";

        if ($this->session->userdata('id_usuario')) {
            $data["lavador"] = $this->session->userdata('id_usuario');
            $data["id_cliente"] = $this->consulta->recuperar_cliente($data["lavador"]);
        } 

        $this->blade->render('webview/generar_cita',$data);
    }

    public function combustible()
    {
        $data['titulo'] = "";
        $data['titulo_dos'] = "";
        $data['vista'] = "b";

        if ($this->session->userdata('id_usuario')) {
            $data["lavador"] = $this->session->userdata('id_usuario');
            $data["id_cliente"] = $this->consulta->recuperar_cliente($data["lavador"]);
        } 

        $this->blade->render('webview/combustible',$data);
    }

    public function proactivo_agencias()
    {
        $data['titulo'] = "";
        $data['titulo_dos'] = "";
        $data['vista'] = "b";

        if ($this->session->userdata('id_usuario')) {
            $data["agencias"] = $this->consulta->get_result_id("activo","1","catalogo_agencias");

            $data["lavador"] = $this->session->userdata('id_usuario');
            $data["id_cliente"] = $this->consulta->recuperar_cliente($data["lavador"]);
        } 

        $this->blade->render('webview/proactivo_agencias',$data);
    }

    public function redireccionar($id_servicio='')
    {
        echo $this->encrypt($id_servicio);
    }

    #----------------------------Encriptado de rutas y otras rutas
    function encrypt($data)
    {
        $id = (float)$data * CONST_ENCRYPT;
        $url_id = base64_encode($id);
        $url = str_replace("=", "", $url_id);
        return $url;
        //return $data;
    }

    function decrypt($data)
    {
        $url_id = base64_decode($data);
        $id = (float)$url_id / CONST_ENCRYPT;
        return $id;
        //return $data;
    }

    public function guardar_movimiento($id_servicio='',$movimiento = '')
    {
        //Recuperamos los datos del servicio
        $datos_servicio = $this->consulta->datos_base_servicio($id_servicio);

        if (count($datos_servicio)>0) {
            $contenedor = array(
                "id" => NULL,
                "id_servicio" => $id_servicio,
                "id_lavador" => $this->session->userdata('id_usuario'),
                "id_cliente" => $datos_servicio[0],
                "movimento" => $movimiento,
                "tipo_usuario" => $this->session->userdata('rol_usuario'),
                "fecha_alta" => date("Y-m-d H:i:s")
            );

            $this->consulta->save_register('historial_procesos_servicio',$contenedor);
        }

        return TRUE;
    }

    public function actualizar_pago($id_servicio = '')
    {
        //Actualizamos el cambo en la tabla de servicio lavado
        $proceso = array(
            "pago" => 1
        );

        $this->consulta->update_table_row('servicio_lavado',$proceso,'id',$id_servicio);

        //Registramos el movimiento en historial de pagos
        $contenedor = array(
            "id" => NULL,
            "id_servicio" => $id_servicio,
            "usuario" => $this->session->userdata('usuario'),
            "created_at" => date("Y-m-d H:i:s")
        );

        $this->consulta->save_register('historial_pago',$contenedor);
    }

    public function sms_general_texto($celular = '', $mensaje = '')
    {
        $sucursal = "XEHON";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/enviar_mensaje_xehos");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt(
            $ch,
            CURLOPT_POSTFIELDS,
            "celular=" . $celular . "&mensaje=" . $mensaje . "&sucursal=" . $sucursal . ""
        );
        // Receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        //var_dump($server_output );
        curl_close($ch);
    }

    public function cambio_status($status = '',$id_servicio = '')
    {
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://hexadms.com/xehos/index.php/api/cambiarStatus");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt(
            $ch,
            CURLOPT_POSTFIELDS,
            "id_status_actual=" . ((int)$status - 1) . 
            "&usuario=" . $this->session->userdata('usuario') . 
            "&id_status_nuevo=" . $status .
            "&id_servicio=" . $id_servicio . ""
        );
        // Receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        //var_dump($server_output );
        curl_close($ch);
    }

    public function validar_seguro_lluvia($id_servicio = '')
    {
        $data = json_decode(file_get_contents('php://input'), true);
        // $curl is the handle of the resource
        $curl = curl_init();
        // set the URL and other options
        curl_setopt($curl, CURLOPT_URL, "https://xehos.com/xehos_web/api/segurolluvia?servicio_id=".$id_servicio);
        //set the content type to application/json
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        //return response instead of outputting
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        // execute and pass the result to browser
        $server_output = curl_exec($curl);
        // close the cURL resource
        curl_close($curl);
        //var_dump($server_output );
        $respuesta = json_decode($server_output);
        //var_dump($respuesta->seguro_lluvia);
        return ((isset($respuesta->seguro_lluvia)) ? $respuesta->seguro_lluvia : FALSE);
    }

    public function envio_factura()
    {
        //var_dump($_REQUEST);
        $url = "https://sohexs.com/facturacion/index.php/api_factura/crear_factura";

        $curl = curl_init($url);
        //Para que la peticion no imprima el resultado como una cadena, y podamos manipularlo
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        //Adjuntamos el json a nuestra petición
        curl_setopt($curl, CURLOPT_POSTFIELDS,$_REQUEST);
        //return response instead of outputting
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        //Obtenemos la respuesta
        $response = curl_exec($curl);

        // Se cierra el recurso CURL y se liberan los recursos del sistema
        curl_close($curl);
        $result = json_decode($response,TRUE);
        //echo $response;
        //if($response) {
            $lavador = $this->session->userdata('id_usuario');
            $id_paso = $this->consulta->id_paso_seguimiento($_POST["factura_serie"],$lavador);

            $proceso = array(
                "pago" => 1,
                "evidencia_pago" => 1
            );

            if ($result["error"] == "0") {
                $proceso["error"] = $result["error_mensaje"];
                $proceso["timbre_factura"] = $result["factura"];

                $this->guardar_movimiento($_POST["factura_serie"],"Envio de facturación. ".$result["error_mensaje"]);
            }

            $proceso["fecha_actualiza"] = date("Y-m-d H:i:s");

            $this->consulta->update_table_row('proceso_servicio_lavado',$proceso,'id',$id_paso);

            //Guardamos el movimiento del servicio
            $this->guardar_movimiento($_POST["factura_serie"],"Finaliza toma de evidencia del comprobante de pago");

            //Actualizamos el historial de pagos
            $this->actualizar_pago($_POST["factura_serie"]);

            echo $result["error_mensaje"];
        //}else{
            //echo "ERROR";
        //}
    }

}
