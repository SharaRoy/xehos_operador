<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class ApiAppk extends MX_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_Apis', 'consulta', TRUE);
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'validation', 'url'));

        date_default_timezone_set(CONST_TIMEZONE);
        
    }

    public function index()
    {
        
    }
// ----------------------------------------------------------------------------------------------------//
// ---------------------Catalogos de inventarios e informacion del servicio ---------------------------//
// ----------------------------------------------------------------------------------------------------//
    public function list_service()
    {
        $_POST = json_decode(file_get_contents('php://input'), TRUE);
        $contenido = [];

        try {
            $id_lavador = $_POST["lavador"];

            $inventario = $this->consulta->buscar_inventario($id_lavador);
            $contenido["inventario_lavador"] = (($inventario != "") ? 1 : 0);
            $contenido["servicio_activo"] = $this->consulta->servicio_activo($id_lavador);
            
            $servicios = $this->consulta->servicios_lavador($id_lavador);
            $lista_servicios = [];
            foreach ($servicios as $i => $registro) {
                $proceso = "";
                if (($registro->cancelado == '0')&&($registro->reagendado == '0')) {
                    if ($contenido["servicio_activo"] == 1) {
                        if (($registro->id_estatus_lavado != '1')&&($registro->id_estatus_lavado != '6')) {
                            $proceso = "Continuar";
                        } elseif ($registro->id_estatus_lavado == '6') {
                            $proceso = "Ver";
                        }
                    } else{
                        if ($registro->id_estatus_lavado == '6') {
                            $proceso = "Ver";
                        } else {
                            $proceso = "Iniciar";
                        }
                    }
                }
                
                $lista_servicios[] = array(
                    "no_servicio" => $registro->id,
                    "cat_status_servicio" => $registro->id_estatus_lavado,
                    "status_servicio" => (($registro->cancelado == '1') ? "CANCELADO" : (($registro->reagendado == '1') ? "REAGENDADO" : $registro->estatus)),
                    "color_servicio" => (($registro->cancelado == '1') ? 'antiquewhite': (($registro->id_estatus_lavado == '6') ? 'aliceblue' : 'white')),
                    "folio_servicio" => $registro->folio_mostrar,
                    "servicio" => $registro->servicioNombre,
                    "fecha_programada" => $registro->fecha,
                    "hora_programada" => $registro->hora,
                    "btn_proceso" => $proceso
                );
            }

            $contenido["lista_Servicios"] = $lista_servicios;

            $contenido["status"] = "OK";
            $contenido["message"] = "OK";
        } catch (Exception $e) {
            $contenido["status"] = "ERROR";
            $contenido["message"] = "ERROR";
        }

        echo json_encode($contenido);
    }

    public function general_data_service()
    {
        $_POST = json_decode(file_get_contents('php://input'), TRUE);
        $contenido = [];

        try {
            $id_servicio = $_POST["no_servicio"];

            $servicio_info = $this->consulta->datos_servicio($id_servicio);
            $servicio = ((count($servicio_info)>0) ? $servicio_info[0] : $servicio_info);

            if (isset($servicio->fecha)) {
                $fecha = new DateTime($servicio->fecha);
                $fecha_p = $fecha->format('d-m-Y');
            }else{
                $fecha_p = NULL;
            }

            $contenido["informacion_servicio"] = array(
                "no_servicio" => $id_servicio,
                "servicio" => ((isset($servicio->servicioNombre) ? $servicio->servicioNombre : NULL)),
                "operador" => ((isset($servicio->lavadorNombre) ? $servicio->lavadorNombre : NULL)),
                "servicio_cancelado" => ((isset($servicio->cancelado) ? $servicio->cancelado : NULL)),
                "servicio_reagendado" => ((isset($servicio->reagendado) ? $servicio->reagendado : NULL)),
                "seguro_lluvia" => ((isset($paso->seguro_lluvia) ? (($paso->seguro_lluvia == TRUE) ? "SI" : "NO") : "NO")),
                "metodo_pago" => ((isset($servicio->metodo_pago) ? $servicio->metodo_pago : NULL)),
                "pago" => ((isset($servicio->pago) ? $servicio->pago : NULL)),
                "costo_servicio" => ((isset($servicio->total) ? $servicio->total : NULL)),
                "fecha_programada" => $fecha_p,
                "hora_programada" => ((isset($servicio->hora) ? $servicio->hora : NULL)),
                "status_servicio" => ((isset($servicio->estatus) ? $servicio->estatus : NULL)),
                "folio" => ((isset($servicio->folio_mostrar) ? $servicio->folio_mostrar : NULL)),
                "nombre_cliente" => ((isset($servicio->nombre) ? $servicio->nombre : NULL)),
                "ap_cliente" => ((isset($servicio->apellido_paterno) ? $servicio->apellido_paterno : NULL)),
                "am_cliente" => ((isset($servicio->apellido_materno) ? $servicio->apellido_materno : NULL)),
                /*"calle" => ((isset($servicio->calle) ? $servicio->calle : NULL)),
                "num_int" => ((isset($servicio->numero_int) ? $servicio->numero_int : NULL)),                
                "num_ext" => ((isset($servicio->numero_ext) ? $servicio->numero_ext : NULL)),
                "colonia" => ((isset($servicio->colonia) ? $servicio->colonia : NULL)),
                "municipio" => ((isset($servicio->municipio) ? $servicio->municipio : NULL)),
                "estado" => ((isset($servicio->estado) ? $servicio->estado : NULL)),
                "cp" => ((isset($servicio->cp) ? $servicio->cp : NULL)),*/
                "domicilio_cliente" => ((isset($servicio->calle) ? $servicio->calle. " int. " . $servicio->numero_int . " ext. " . $servicio->numero_ext . " Col. " . $servicio->colonia . " " . $servicio->municipio . " " . $servicio->estado : NULL)),
                "correo_cliente" => ((isset($servicio->email) ? $servicio->email : NULL)),
                "telefono_cliente" => ((isset($servicio->telefono) ? $servicio->telefono : NULL)),
                "vehiculo_no_catalogo" => ((isset($servicio->id_auto) ? $servicio->id_auto : NULL)),
                "vehiculo_color" => ((isset($servicio->color) ? $servicio->color : NULL)),
                "vehiculo_marca" => ((isset($servicio->marca) ? $servicio->marca : NULL)),
                "vehiculo_modelo" => ((isset($servicio->modelo) ? $servicio->modelo : NULL)),
                "vehiculo_tipo" => ((isset($servicio->tipo_auto) ? $servicio->tipo_auto : NULL)),
                "vehiculo_kilometraje" => ((isset($servicio->kilometraje) ? $servicio->kilometraje : NULL)),
                "vehiculo_placas" => ((isset($servicio->placas) ? strtoupper($servicio->placas) : "XXXXXXXX")),
                "vehiculo_serie" => ((isset($servicio->numero_serie) ? strtoupper($servicio->numero_serie) : "XXXXXXXXXXXXXX")),
            );

            $avance_proceso = $this->consulta->pasos_proceso($id_servicio);

            if ($avance_proceso != NULL) {
                $paso = $avance_proceso[0];
            }

            $contenido["proceso"] = array(
                "inicia_ruta" => ((isset($paso->inicia_ruta)) ? $paso->inicia_ruta : 0),
                "termina_ruta" => ((isset($paso->termina_ruta)) ? $paso->termina_ruta : 0),
                "evidencia_pago" => ((isset($paso->evidencia_pago)) ? $paso->evidencia_pago : 0),
                "evidencia_inicio" => ((isset($paso->evidencia_inicio)) ? $paso->evidencia_inicio : 0),
                "inventario" => ((isset($paso->inventario)) ? $paso->inventario : 0),
                "evidencia_lavado" => ((isset($paso->evidencia_lavado)) ? $paso->evidencia_lavado : 0),
                "evidencia_finaliza" => ((isset($paso->evidencia_finaliza)) ? $paso->evidencia_finaliza : 0),
                "termina_servicio" => ((isset($paso->termina_servicio)) ? $paso->termina_servicio : 0),
            );

            $contenido["status"] = "OK";
            $contenido["message"] = "OK";
        } catch (Exception $e) {
            $contenido["status"] = "ERROR";
            $contenido["message"] = "ERROR";
        }

        echo json_encode($contenido);
    }

    public function data_vehicle()
    {
        $_POST = json_decode(file_get_contents('php://input'), TRUE);
        $contenido = [];

        try {
            $id_servicio = $_POST["no_servicio"];

            $servicio_info = $this->consulta->datos_servicio($id_servicio);
            $servicio = ((count($servicio_info)>0) ? $servicio_info[0] : $servicio_info);

            $contenido["informacion_vehiculo"] = array(
                "vehiculo_no_catalogo" => ((isset($servicio->id_auto) ? $servicio->id_auto : NULL)),
                "vehiculo_color" => ((isset($servicio->color) ? $servicio->color : NULL)),
                "vehiculo_marca" => ((isset($servicio->marca) ? $servicio->marca : NULL)),
                "vehiculo_modelo" => ((isset($servicio->modelo) ? $servicio->modelo : NULL)),
                "vehiculo_tipo" => ((isset($servicio->tipo_auto) ? $servicio->tipo_auto : NULL)),
                "vehiculo_kilometraje" => ((isset($servicio->kilometraje) ? $servicio->kilometraje : NULL)),
                "vehiculo_placas" => ((isset($servicio->placas) ? strtoupper($servicio->placas) : "XXXXXXXX")),
                "vehiculo_serie" => ((isset($servicio->numero_serie) ? strtoupper($servicio->numero_serie) : "XXXXXXXXXXXXXX")),
            );

            $contenido["status"] = "OK";
            $contenido["message"] = "OK";
        } catch (Exception $e) {
            $contenido["status"] = "ERROR";
            $contenido["message"] = "ERROR";
        }

        echo json_encode($contenido);
    }

    public function data_operator_unit()
    {
        $_POST = json_decode(file_get_contents('php://input'), TRUE);
        $contenido = [];

        try {
            $id_lavador = $_POST["lavador"];

            $validar = $this->consulta->buscar_inventario($id_lavador);
            $contenido["inventario_lleno"] = (($validar != "") ? 1 : 0);
            
            $contenido["nombre_lavador"] = $this->consulta->nombre_lavador($id_lavador);

            $datos_unidad = $this->consulta->get_result_one('id_lavador',$id_lavador,'unidades');
            foreach ($datos_unidad as $row){
                $contenido["datos_unidad"] = array(
                    "cat_unidad" => $row->id,
                    "marca" => $row->marca,
                    "placas" => $row->placas,
                    "color" => $row->color,
                    "serie" => $row->n_serie,
                    "kilometros" => $row->kilometros,
                    "anio" => $row->anio,
                    "unidad" => $row->unidad,
                );
            }

            $contenido["status"] = "OK";
            $contenido["message"] = "OK";
        } catch (Exception $e) {
            $contenido["status"] = "ERROR";
            $contenido["message"] = "ERROR";
        }

        echo json_encode($contenido);
    }

    public function status_route()
    {
        $_POST = json_decode(file_get_contents('php://input'), TRUE);
        $contenido = [];

        try {
            $id_servicio = $_POST["no_servicio"];

            $contenido["status_ruta"] = "";
            $avance_proceso = $this->consulta->pasos_proceso($id_servicio);

            if ($avance_proceso != NULL) {
                $paso = $avance_proceso[0];

                if($paso->termina_ruta == "1"){
                    $contenido["status_ruta"] = "RUTA TERMINADA";
                    $contenido["color_fondo"] = "aliceblue";
                    $contenido["color_letras"] = "darkblue";

                }elseif (($paso->pausar_ruta == "0")&&($paso->reanudar_ruta == "0")) {
                    $contenido["status_ruta"] = "RUTA EN PROCESO";
                    $contenido["color_fondo"] = "aliceblue";
                    $contenido["color_letras"] = "darkblue";
                }elseif (($paso->pausar_ruta == "1")&&($paso->reanudar_ruta == "0")) {
                    $contenido["status_ruta"] = "RUTA PAUSADA";
                    $contenido["color_fondo"] = "antiquewhite";
                    $contenido["color_letras"] = "darkblue";
                }elseif (($paso->pausar_ruta == "1")&&($paso->reanudar_ruta == "1")) {
                    $contenido["status_ruta"] = "RUTA REANUDADA";
                    $contenido["color_fondo"] = "aliceblue";
                    $contenido["color_letras"] = "darkblue";
                }else{
                    $contenido["status_ruta"] = "RUTA EN PROCESO";
                    $contenido["color_fondo"] = "aliceblue";
                    $contenido["color_letras"] = "darkblue";
                }
            }

            $contenido["status"] = "OK";
            $contenido["message"] = "OK";
        } catch (Exception $e) {
            $contenido["status"] = "ERROR";
            $contenido["message"] = "ERROR";
        }

        echo json_encode($contenido);
    }

    public function status_evidence()
    {
        $_POST = json_decode(file_get_contents('php://input'), TRUE);
        $contenido = [];

        try {
            $id_servicio = $_POST["no_servicio"];
            $id_lavador = $_POST["lavador"];

            $contenido["evidencia_solicitada"] = "";
            $avance_proceso = $this->consulta->pasos_proceso($id_servicio);

            if ($avance_proceso != NULL) {
                $paso = $avance_proceso[0];

                if ($paso->evidencia_pago == "0") {
                    $contenido["evidencia_solicitada"] = "Evidencia: Comprobante de pago";
                    $contenido['titulo'] = "Evidencia de pago";
                    $contenido['paso_evidencia'] = "4";

                }elseif (($paso->evidencia_pago == "1")&&($paso->evidencia_inicio == "0")) {
                    $contenido["evidencia_solicitada"] = "Evidencia: Estado inicial de vehículo";
                    $contenido['titulo'] = "Estado inicial del vehículo";
                    $contenido['paso_evidencia'] = "1";

                    //Cambiamos el estatus
                    $this->cambio_status('4',$id_servicio,$id_lavador);

                }elseif (($paso->evidencia_inicio == "1")&&($paso->evidencia_lavado == "0")) {
                    $contenido["evidencia_solicitada"] = "Evidencia: Proceso de lavado";
                    $contenido['titulo'] = "Proceso de lavado";
                    $contenido['paso_evidencia'] = "2";

                }elseif (($paso->evidencia_lavado == "1")&&($paso->evidencia_finaliza == "0")) {
                    $contenido["evidencia_solicitada"] = "Evidencia: Pre-entrega";
                    $contenido['titulo'] = "Lavado terminado";
                    $contenido['paso_evidencia'] = "3";
                
                }elseif (($paso->evidencia_finaliza == "1")&&($paso->evidencia_pago == "0")) {
                    $contenido["evidencia_solicitada"] = "Evidencia: Comprobante de pago";
                    $contenido['titulo'] = "Evidencia de pago";
                    $contenido['paso_evidencia'] = "4";
                
                }elseif (($paso->evidencia_pago == "1")&&($paso->termina_servicio == "0")) {
                    $contenido["evidencia_solicitada"] = "Evidencia completada";
                    $contenido['titulo'] = "Estado final del vehículo";
                    $contenido['paso_evidencia'] = "5";

                    //Cambiamos el estatus
                    $this->cambio_status('5',$id_servicio,$id_lavador);

                }elseif (($paso->termina_servicio == "1")&&($paso->finalizado == "0")) {
                    $contenido["evidencia_solicitada"] = "Servicio completado";
                    $contenido['titulo'] = "Servicio terminado";
                    $contenido['paso_evidencia'] = "6";

                    //Cambiamos el estatus
                    $this->cambio_status('6',$id_servicio,$id_lavador);

                    $id_paso = $this->consulta->id_paso_seguimiento($id_servicio,$id_lavador);
                    $proceso = array(
                        "finalizado" => 1,
                        "fecha_actualiza" => date("Y-m-d H:i:s")
                    );

                    $this->consulta->update_table_row('proceso_servicio_lavado',$proceso,'id',$id_paso);

                    //Guardamos el movimiento del servicio
                    $this->guardar_movimiento($id_servicio,"Finaliza la entrega de la unidad",$id_lavador);
                
                }else{
                    $contenido["evidencia_solicitada"] = "Servicio completado";
                    $contenido['titulo'] = "Servicio terminado";
                    $contenido['paso_evidencia'] = "6";
                }
            }

            $contenido["status"] = "OK";
            $contenido["message"] = "OK";
        } catch (Exception $e) {
            $contenido["status"] = "ERROR";
            $contenido["message"] = "ERROR";
        }

        echo json_encode($contenido);
    }

    public function evidence_list()
    {
        $_POST = json_decode(file_get_contents('php://input'), TRUE);
        $contenido = [];

        try {
            $id_servicio = $_POST["no_servicio"];
            $id_lavador = $_POST["lavador"];

            $lista_evicendia = [];

            $array_imagen = ["JPEG","JPG","PNG","BMP","ICO","SVG","WEBP","GIF","PSD","HEIC","NEF/CRW","AI","ID","PSD","RAW","TIF","TIFF"];
            $array_doc = ["TXT","DOC","DOCX","DOCM","ODT","PDF","RTF","CSV","XLS","XLSX","XLSM","ODS","PPS","PPT","PPSX","PPTX","PPSM","PPTM","POTX","ODP"];
            $array_audio = ["MP3","WMA","WAV","FLAC","MIDI","OGG","M3U"];
            $array_video = ["AVI","DIVX","MOV","MP4","MPG","MKV","WMV","WPL"];

            $evidencias = $this->consulta->evidencia_servicio($id_servicio,$id_lavador);
            foreach ($evidencias as $i => $registro) {
                $fecha = new DateTime($registro->fecha_alta);
                $partes_url = explode(".", $registro->url);
                $extension = strtoupper($partes_url[count($partes_url)-1]);

                if (in_array($extension, $array_imagen)) {
                    $formato = "IMAGEN";
                } elseif (in_array($extension, $array_doc)) {
                    $formato = "DOCUMENTO";
                } elseif (in_array($extension, $array_audio)) {
                    $formato = "AUDIO";
                } elseif (in_array($extension, $array_video)) {
                    $formato = "VIDEO";
                } else{
                    $formato = "SIN IDENTIFICAR";
                }
                
                $lista_evicendia[] = array(
                    "titulo" => $registro->titulo,
                    "comentario" => $registro->comentarios,
                    "fecha_registro" => $fecha->format('d-m-Y H:i'),
                    "tipo_archivo" => $formato,
                    "ruta_evidencia" => base_url().$registro->url,
                );
            }

            $contenido["evidencias"] = $lista_evicendia;

            $contenido["status"] = "OK";
            $contenido["message"] = "OK";
        } catch (Exception $e) {
            $contenido["status"] = "ERROR";
            $contenido["message"] = "ERROR";
        }

        echo json_encode($contenido);
    }

    public function get_customer_diagnosis()
    {
        $_POST = json_decode(file_get_contents('php://input'), TRUE);
        $contenido = [];

        try {
            $id_servicio = $_POST["no_servicio"];
            $diagnostico = NULL;
            $evidencias = NULL;
            $contrato = NULL;
            $info_gral = NULL;

            $query = $this->consulta->get_result("id_orden",$id_servicio,"diagnostico");

            foreach ($query as $row){
                $diagnostico["id_diagnostico"] = $row->id;
                $diagnostico["no_servicio"] = $row->id_orden;
                $diagnostico["origen_repo"] = $row->origen_diagnostico;
                $diagnostico["folio_info"] = $row->folio_info;

                $diagnostico["marca_danos"] = $row->marca_danos;
                $diagnostico["interiores"] = explode("_",$row->interiores);
                $diagnostico["cajuela"] = explode("_",$row->cajuela);
                $diagnostico["exteriores"] = explode("_",$row->exteriores);
                $diagnostico["documentacion"] = explode("_",$row->documentacion);
                $diagnostico["nivel_gasolina"] = $row->nivel_gasolina;
                $diagnostico["articulos_vehiculo"] = $row->articulos_vehiculo;
                $diagnostico["articulos"] = $row->articulos;
                $diagnostico["reporte_articulos"] = $row->reporte_articulos;
                $diagnostico["costo_diagnostico"] = $row->costo_diagnostico;
                $diagnostico["dia_diagnostico"] = $row->dia_diagnostico;
                $diagnostico["mes_diagnostico"] = $row->mes_diagnostico;
                $diagnostico["anio_diagnostico"] = $row->anio_diagnostico;
                $diagnostico["nombre_asesor"] = $row->nombre_asesor;
                $diagnostico["nombre_cliente"] = $row->nombre_cliente;

                //Validamos las firmas
                if ($diagnostico["origen_repo"] == "1") { 
                    $img_diagrama = REPO_XEHOS.$row->img_diagrama;
                    $firma_asesor = REPO_XEHOS.$row->firma_asesor;
                    $firma_cliente = REPO_XEHOS.$row->firma_cliente;
                }else{
                    $img_diagrama = REPO_XEHOS_OP.$row->img_diagrama;
                    $firma_asesor = REPO_XEHOS_OP.$row->firma_asesor;
                    $firma_cliente = REPO_XEHOS_OP.$row->firma_cliente;
                }

                $diagnostico["img_diagrama"] = (($this->url_exists($img_diagrama)) ? $img_diagrama : "") ;
                $diagnostico["firma_asesor"] = (($this->url_exists($firma_asesor)) ? $firma_asesor : "") ;
                $diagnostico["firma_cliente"] = (($this->url_exists($firma_cliente)) ? $firma_cliente : "") ;
            }

            //Si existe el diagnostico, cargamos el contrato
            if (isset($diagnostico["origen_repo"])) {
                $contrato_info = $this->consulta->get_result("id_orden",$id_servicio,"contrato");
                foreach ($contrato_info as $row){
                    $contrato["folio"] = $row->folio;
                    
                    $contrato["fecha_contrato"] = $row->fecha;
                    $fecha = new DateTime($row->fecha.' 00:00:00');
                    $contrato["fecha_contrato_visual"] = $fecha->format('d-m-Y');

                    $contrato["hora_contrato"] = $row->hora;
                    $contrato["empresa_contrato"] = $row->empresa;
                    $contrato["autoriza_contrato"] = $row->delegado;
                    $contrato["cantidad_contrato"] = $row->monto;
                    $contrato["publicidad_contrato"] = $row->compartir_datos;
                    $contrato["envio_publicidad"] = $row->envio_publicidad;

                    $contrato["cnombre_asesor"] = $row->nombre_asesor;
                    $contrato["cnombre_cliente"] = $row->nombre_cliente;
                    $contrato["privacidad_contrato"] = $row->privacidad;
                    $contrato["registro_publico"] = $row->registro_publico;
                    $contrato["expediente_publico"] = $row->expediente_publico;
                    $contrato["registro_publico_dia"] = $row->d_expediente;
                    $contrato["registro_publico_mes"] = $row->m_expediente;
                    $contrato["registro_publico_anio"] = $row->a_expediente;

                    //Validamos las firmas
                    if ($diagnostico["origen_repo"] == "1") {
                        $firma_cliente_c1 = REPO_XEHOS.$row->firma_cliente_publicidad;
                        $ccfirma_asesor = REPO_XEHOS.$row->firma_asesor;
                        $ccfirma_cliente = REPO_XEHOS.$row->firma_cliente;
                    }else{
                        $firma_cliente_c1 = REPO_XEHOS_OP.$row->firma_cliente_publicidad;
                        $ccfirma_asesor = REPO_XEHOS_OP.$row->firma_asesor;
                        $ccfirma_cliente = REPO_XEHOS_OP.$row->firma_cliente;
                    }

                    $contrato["firma_cliente_c1"] = (($this->url_exists($firma_cliente_c1)) ? $firma_cliente_c1 : "") ;
                    $contrato["ccfirma_asesor"] = (($this->url_exists($ccfirma_asesor)) ? $ccfirma_asesor : "") ;
                    $contrato["ccfirma_cliente"] = (($this->url_exists($ccfirma_cliente)) ? $ccfirma_cliente : "") ;
                }

                //Cargamos las evidencias  
                $lista = $this->consulta->get_result_field("id_orden",$id_servicio,"inventario","servicio","evidencia_inventario");
                foreach ($lista as $row){
                    if ($row->activo == "1") {
                        $evidencias[] = $row->evidencia;
                    }
                }

                $vista_informativa = $this->consulta->get_result("id",$id_servicio,"v_info_servicios");
                foreach ($vista_informativa as $row){
                    $info_gral["folio_mostrar"] = $row->folio_mostrar;
                    $info_gral["orden_asesor"] = $row->lavadorNombre;
                    $info_gral["orden_telefono_asesor"] = "3338352043";
                    $info_gral["orden_datos_nombres"] = $row->nombre;
                    $info_gral["orden_datos_apellido_paterno"] = $row->apellido_paterno;
                    $info_gral["orden_datos_apellido_materno"] = $row->apellido_materno;
                    $info_gral["orden_nombre_compania"] = $row->razon_social;

                    $info_gral["orden_nombre_contacto_compania"] = "SN";
                    $info_gral["orden_ap_contacto"] = "SN";
                    $info_gral["orden_am_contacto"] = "SN";

                    $info_gral["orden_datos_email"] = $row->email;
                    $info_gral["orden_rfc"] = $row->rfc;
                    $info_gral["orden_correo_compania"] = $row->email_facturacion;

                    $info_gral["orden_domicilio"] = $row->domicilio;
                    $info_gral["orden_municipio"] = $row->municipio;
                    $info_gral["orden_cp"] = $row->cp;
                    $info_gral["orden_estado"] = $row->estado;
                    $info_gral["orden_telefono_movil"] = $row->telefono;
                    $info_gral["orden_otro_telefono"] = $row->telefono;

                    $info_gral["orden_vehiculo_placas"] = $row->placas;
                    $info_gral["orden_vehiculo_identificacion"] = $row->numero_serie;
                    $info_gral["orden_vehiculo_kilometraje"] = $row->kilometraje;
                    $info_gral["orden_vehiculo_marca"] = $row->marca;
                    $info_gral["orden_categoria"] = $row->modelo;
                    $info_gral["orden_vehiculo_anio"] = $row->anio;
                    $info_gral["color"] = $row->color;

                    $info_gral["item_cantidad"][] = "1";
                    $info_gral["item_descripcion"][] = $row->servicioNombre;
                    
                    $iva = (float)$row->precio * 0.16;
                    $precio = (float)$row->precio - $iva;
                    $precio_unitario = (float)$row->precio;

                    $info_gral["item_precio_unitario"] = $precio;
                    $info_gral["item_descuento"] = "0.00";
                    $info_gral["item_total"] = $precio;

                    $info_gral["subtotal_items"] = $precio;

                    $info_gral["iva_items"] = $iva;
                    $info_gral["presupuesto_items"] = $row->precio;
                }

                $mensaje = "";
            } else {
                $mensaje = "NO SE ENCONTRÓ UN INVENTARIO CON ESTE NÚMERO DE SERVICIO";
            }

            $contenido["info_servicio"] = $info_gral;
            $contenido["info_diagnostico"] = $diagnostico;
            $contenido["info_contrato"] = $contrato;
            $contenido["evidencias"] = $evidencias;

            $contenido["status"] = "OK";
            $contenido["message"] = "OK";
        } catch (Exception $e) {
            $contenido["status"] = "ERROR";
            $contenido["message"] = "ERROR";
        }

        echo json_encode($contenido);
    }

    public function get_operator_diagnostics()
    {
        $_POST = json_decode(file_get_contents('php://input'), TRUE);
        $contenido = [];

        try {
            $id_lavador = $_POST["lavador"];
            $diagnostico = NULL;
            $evidendia = NULL;
            $info_unidad = NULL;
            //$contenido["id_lavador"] = $id_lavador;

            $id_servicio = $this->consulta->buscar_inventario($id_lavador);
            //$contenido["id_servicio"] = $id_servicio;
            if ($id_servicio != "") {
                //Datos del inventario
                $datos_unidad = $this->consulta->get_result_one('id',$id_servicio,'diag_unidades_lavadores');
                foreach ($datos_unidad as $row){
                    $diagnostico["id_diagnostico"] = $row->id;
                    $diagnostico["origen_repo"] = $row->origen_diagnostico;
                    $diagnostico["cat_unidad"] = $row->id_unidad;
                    $diagnostico["lavador"] = $row->id_lavador;
                    $diagnostico["supervisor"] = $row->id_supervisor;
                    $diagnostico["nombre_lavador"] = $row->lavador;
                    $diagnostico["nombre_supervisor"] = $row->supervisor;
                    $diagnostico["marca_danos"] = $row->marca_danos;
                    $diagnostico["nivel_gasolina"] = $row->nivel_gasolina;
                    $diagnostico["notas_lavador"] = $row->comentarios_lavador;
                    $diagnostico["notas_supervisor"] = $row->comentarios_supervisor;

                    $diagnostico["cheking"] = explode(",",$row->checklist_topicos);

                    //Verificamos las rutas de las firmas
                    if ($diagnostico["origen_repo"] == "1") {
                        $firma_lavador = REPO_XEHOS.$row->firma_lavador;
                        $firma_supervisor = (($row->firma_supervisor != "") ? REPO_XEHOS.$row->firma_supervisor : "");
                        $danosMarcas = REPO_XEHOS.$row->diagrama;
                    }else{
                        $firma_lavador = REPO_XEHOS_OP.$row->firma_lavador;
                        $firma_supervisor = (($row->firma_supervisor != "") ? REPO_XEHOS_OP.$row->firma_supervisor : "");
                        $danosMarcas = REPO_XEHOS_OP.$row->diagrama;
                    }

                    $diagnostico["firma_lavador"] = (($this->url_exists($firma_lavador)) ? $firma_lavador : "") ;
                    if ($firma_supervisor != "") {
                        $diagnostico["firma_supervisor"] = (($this->url_exists($firma_supervisor)) ? $firma_supervisor : "") ;
                    } else {
                        $diagnostico["firma_supervisor"] = "";
                    }

                    $diagnostico["danosMarcas"] = (($this->url_exists($danosMarcas)) ? $danosMarcas : "") ;
                }

                if (isset($diagnostico["id_diagnostico"])) {
                    //Datos generales de la unidad
                    $datos_unidad = $this->consulta->get_result_one('id',$diagnostico["cat_unidad"],'unidades');
                    foreach ($datos_unidad as $row){
                        $info_unidad["marca"] = $row->marca;
                        $info_unidad["placas"] = $row->placas;
                        $info_unidad["color"] = $row->color;
                        $info_unidad["serie"] = $row->n_serie;
                        $info_unidad["kilometros"] = $row->kilometros;
                        $info_unidad["anio"] = $row->anio;
                        $info_unidad["unidad"] = $row->unidad;
                    }

                    //Cargamos las evidencias  
                    $evidencias = $this->consulta->get_result_field("id_orden",$diagnostico["id_diagnostico"],"inventario","lavadores","evidencia_inventario");
                    foreach ($evidencias as $row){
                        if ($row->activo == "1") {
                            $evidendia[] = $row->evidencia;
                        }
                    }

                    $mensaje = "";
                } else{
                    $mensaje = "NO SE ENCONTRÓ UN INVENTARIO COMPLETO DE ESTE OPERADOR";
                }
            } else {
                $mensaje = "NO SE ENCONTRÓ UN INVENTARIO DE ESTE OPERADOR PARA EL DÍA DE HOY";
            }

            $contenido["datos_unidad"] = $info_unidad;
            $contenido["inventario"] = $diagnostico;
            $contenido["evidnecias"] = $evidendia;

            $contenido["status"] = (($diagnostico != NULL) ? "OK" : "ERROR");
            $contenido["message"] = (($diagnostico != NULL) ? "OK" : "ERROR");
        } catch (Exception $e) {
            $contenido["status"] = "ERROR";
            $contenido["message"] = "ERROR";
        }

        echo json_encode($contenido);
    }


// ----------------------------------------------------------------------------------------------------//
// --------------------------Procesos del servicio y avance--------------------------------------------//
// ----------------------------------------------------------------------------------------------------//
    public function update_vehicle_data()
    {
        $_POST = json_decode(file_get_contents('php://input'), TRUE);
        $contenido = [];

        try {
            $id_auto = $_POST["vehiculo_no_catalogo"];

            $campos = array(
                "placas" => strtoupper($_POST["placas"]),
                "numero_serie" => strtoupper($_POST["serie"]),
            );

            $actualizar = $this->consulta->update_table_row('autos',$campos,'id',$id_auto);

            if ($actualizar) {
                $contenido["status"] = "OK";
                $contenido["message"] = "OK";
            } else {
                $contenido["status"] = "ERROR";
                $contenido["message"] = "ERROR";
            }
        } catch (Exception $e) {
            $contenido["status"] = "ERROR";
            $contenido["message"] = "ERROR";
        }

        echo json_encode($contenido);
    }

    public function cancel_service()
    {
        $_POST = json_decode(file_get_contents('php://input'), TRUE);
        $contenido = [];

        try {
            $id_servicio = $_POST["no_servicio"];
            $id_lavador = $_POST["lavador"];
            $motivo = $_POST["motivo_cancelacion"];

            //Actualizamos en la tabla de servicio
            $cancelado = array(
                "cancelado" => 1
            );
            
            $this->consulta->update_table_row('servicio_lavado',$cancelado,'id',$id_servicio);

            //Marcamos el paso
            $id_paso = $this->consulta->id_paso_seguimiento($id_servicio,$id_lavador);

            $proceso = array(
                "servicio_cancelado" => 1,
                "fecha_actualiza" => date("Y-m-d H:i:s")
            );

            $actualizar = $this->consulta->update_table_row('proceso_servicio_lavado',$proceso,'id',$id_paso);

            $this->guardar_movimiento($id_servicio,"Servicio cancelado: ".$motivo, $id_lavador);

            if ($actualizar) {
                $contenido["status"] = "OK";
                $contenido["message"] = "OK";
            } else {
                $contenido["status"] = "ERROR";
                $contenido["message"] = "ERROR";
            }
        } catch (Exception $e) {
            $contenido["status"] = "ERROR";
            $contenido["message"] = "ERROR";
        }

        echo json_encode($contenido);
    }

    public function save_coordinates()
    {
        $_POST = json_decode(file_get_contents('php://input'), TRUE);
        $contenido = [];

        try {
            $id_servicio = $_POST["no_servicio"];
            $id_lavador = $_POST["lavador"];
            $lat_3 = $_POST["latitud"];
            $lng_3 = $_POST["longitud"];

            //Gradamos la coordenada
            $proceso = array(
                "id" => NULL,
                "id_lavador" => $id_lavador,
                "latitud" => $lat_3,
                "longitud" => $lng_3,
                "id_servicio" => $id_servicio,
                "fecha_creacion" => date("Y-m-d H:i:s")
            );

            $this->consulta->save_register('coordenadas_lavador',$proceso);

            //Actualizamos la coordenada del lavador
            $posicion_lavador = array(
                "latitud_lavador" => $lat_3,
                "longitud_lavador" => $lng_3,
            );

            $actualizar = $this->consulta->update_table_row('servicio_lavado',$posicion_lavador,'id',$id_servicio);

            if ($actualizar) {
                $contenido["status"] = "OK";
                $contenido["message"] = "OK";
            } else {
                $contenido["status"] = "ERROR";
                $contenido["message"] = "ERROR";
            }
        } catch (Exception $e) {
            $contenido["status"] = "ERROR";
            $contenido["message"] = "ERROR";
        }

        echo json_encode($contenido);
    }

    public function start_route()
    {
        $_POST = json_decode(file_get_contents('php://input'), TRUE);
        $contenido = [];

        try {
            $id_servicio = $_POST["no_servicio"];
            $id_lavador = $_POST["lavador"];

            $avance_proceso = $this->consulta->pasos_proceso($id_servicio);

            if ($avance_proceso != NULL) {
                $paso = $avance_proceso[0];
            }

            if (!isset($paso->inicia_ruta)) {
                //Verificamos si el servicio esta pagado
                $tipo_pago = $this->consulta->tipo_pago($id_servicio);

                $validar_seguro = $this->validar_seguro_lluvia($id_servicio);

                //Si el servicio ya esta pagado y no es pago por terminal
                $pago = 0;
                $seguro_lluvia = 0;
                if (($tipo_pago[0] != NULL)&&($tipo_pago[0] != 2)) {
                    $pago = 1;
                    if ($validar_seguro === TRUE) {
                        $seguro_lluvia = 1;
                    }
                }

                $proceso = array(
                    "id" => NULL,
                    "id_servicio" => $id_servicio,
                    "id_lavador" => $id_lavador,
                    "inicia_ruta" => 1,
                    "pago" => $pago,
                    "evidencia_pago" => $pago,
                    "seguro_lluvia" => $seguro_lluvia,
                    "fecha_alta" => date("Y-m-d H:i:s"),
                    "fecha_actualiza" => date("Y-m-d H:i:s")
                );

                $this->consulta->save_register('proceso_servicio_lavado',$proceso);

                $this->guardar_movimiento($id_servicio,"Se inicia la ruta",$id_lavador);

                //Cambiamos el estatus
                $this->cambio_status('2',$id_servicio,$id_lavador);
                
                $mensaje = "RUTA DEL SERVICIO INICIADA";
            }else{
                $mensaje = "RUTA DEL SERVICIO EN PROCESO";
            }

            $contenido["status"] = "OK";
            $contenido["message"] = "OK";
        } catch (Exception $e) {
            $contenido["status"] = "ERROR";
            $contenido["message"] = "ERROR";
        }

        echo json_encode($contenido);
    }

    public function route_tracking()
    {
        $_POST = json_decode(file_get_contents('php://input'), TRUE);
        $contenido = [];

        try {
            $id_servicio = $_POST["no_servicio"];
            $id_lavador = $_POST["lavador"];
            $movimiento = $_POST["tipo_movimiento"];

            $id_paso = $this->consulta->id_paso_seguimiento($id_servicio,$id_lavador);

            if ($movimiento == "1") {
                $proceso = array(
                    "termina_ruta" => 1,
                    "fecha_actualiza" => date("Y-m-d H:i:s")
                );

                $this->consulta->update_table_row('proceso_servicio_lavado',$proceso,'id',$id_paso);

                //Guardamos el movimiento del servicio
                $this->guardar_movimiento($id_servicio,"Finaliza la ruta / Se llego al sitio",$id_lavador);

                //Cambiamos el estatus
                $this->cambio_status('3',$id_servicio,$id_lavador);

                $mensaje = "RUTA FINALIZADA";

            } elseif ($movimiento == "2") {
                $proceso = array(
                    "pausar_ruta" => 1,
                    "fecha_actualiza" => date("Y-m-d H:i:s")
                );

                $this->consulta->update_table_row('proceso_servicio_lavado',$proceso,'id',$id_paso);

                //Guardamos el movimiento del servicio
                $this->guardar_movimiento($id_servicio,"Ruta pausada",$id_lavador);

                $mensaje = "RUTA PAUSADA";

            } elseif ($movimiento == "4") {
                $proceso = array(
                    "reanudar_ruta" => 1,
                    "fecha_actualiza" => date("Y-m-d H:i:s")
                );

                $this->consulta->update_table_row('proceso_servicio_lavado',$proceso,'id',$id_paso);

                //Guardamos el movimiento del servicio
                $this->guardar_movimiento($id_servicio,"Ruta Reanudada",$id_lavador);

                $mensaje = "RUTA REANUDADA";

            } else {
                $proceso = array(
                    "servicio_cancelado" => 1,
                    "fecha_actualiza" => date("Y-m-d H:i:s")
                );

                $this->consulta->update_table_row('proceso_servicio_lavado',$proceso,'id',$id_paso);

                //Guardamos el movimiento del servicio
                $this->guardar_movimiento($id_servicio,"Se cancelo la ruta / servicio",$id_lavador);

                $mensaje = "SERVICIO CANCELADO DESDE LA RUTA";
            }

            $contenido["status"] = "OK";
            $contenido["message"] = "OK";
        } catch (Exception $e) {
            $contenido["status"] = "ERROR";
            $contenido["message"] = "ERROR";
        }

        echo json_encode($contenido);
    }

    public function advance_evidence()
    {
        $_POST = json_decode(file_get_contents('php://input'), TRUE);
        $contenido = [];

        try {
            $id_servicio = $_POST["no_servicio"];
            $id_lavador = $_POST["lavador"];
            $paso_evidencia = $_POST["paso_evidencia"];

            $id_paso = $this->consulta->id_paso_seguimiento($id_servicio,$id_lavador);

            if ($paso_evidencia == "2") {
                $proceso = array(
                    "evidencia_lavado" => 1,
                    "fecha_actualiza" => date("Y-m-d H:i:s")
                );

                $this->consulta->update_table_row('proceso_servicio_lavado',$proceso,'id',$id_paso);

                //Guardamos el movimiento del servicio
                $this->guardar_movimiento($id_servicio,"Finaliza la carga de evidencias del proceso de lavado",$id_lavador);

            }elseif ($paso_evidencia == "3") {
                $proceso = array(
                    "evidencia_finaliza" => 1,
                    "fecha_actualiza" => date("Y-m-d H:i:s")
                );

                $this->consulta->update_table_row('proceso_servicio_lavado',$proceso,'id',$id_paso);

                //Guardamos el movimiento del servicio
                $this->guardar_movimiento($id_servicio,"Finaliza la carga de evidencias de entrega de unidad",$id_lavador);
            }elseif ($paso_evidencia == "4") {
                $proceso = array(
                    "pago" => 1,
                    "evidencia_pago" => 1,
                    "fecha_actualiza" => date("Y-m-d H:i:s")
                );

                $this->consulta->update_table_row('proceso_servicio_lavado',$proceso,'id',$id_paso);

                //Guardamos el movimiento del servicio
                $this->guardar_movimiento($id_servicio,"Finaliza toma de evidencia del comprobante de pago",$id_lavador);

                //Actualizamos el historial de pagos
                $this->actualizar_pago($id_servicio,$id_lavador);

            }elseif ($paso_evidencia == "5") {
                $proceso = array(
                    "termina_servicio" => 1,
                    "fecha_actualiza" => date("Y-m-d H:i:s")
                );

                $this->consulta->update_table_row('proceso_servicio_lavado',$proceso,'id',$id_paso);

                //Guardamos el movimiento del id_servicio
                $this->guardar_movimiento($id_servicio,"Finaliza el servicio",$id_lavador);
            }

            $contenido["status"] = "OK";
            $contenido["message"] = "OK";
        } catch (Exception $e) {
            $contenido["status"] = "ERROR";
            $contenido["message"] = "ERROR";
        }

        echo json_encode($contenido);
    }

    public function save_evidence()
    {
        //$_POST = json_decode(file_get_contents('php://input'), TRUE);
        $contenido = [];

        try {
            $id_servicio = $_POST["no_servicio"];
            $id_lavador = $_POST["lavador"];
            $paso_evidencia = $_POST["paso_evidencia"];
            $titulo_evidencia = $_POST["titulo_evidencia"];
            $respuesta = "";

            if(isset($_FILES['archivo']['tmp_name'])){
                //Como el elemento es un arreglos utilizamos foreach para extraer todos los valores
                //for ($i = 0; $i < count($_FILES['archivo']['tmp_name']); $i++) {
                    $_FILES['tempFile']['name'] = $_FILES['archivo']['name'];   //[$i]
                    $_FILES['tempFile']['type'] = $_FILES['archivo']['type'];   //[$i]
                    $_FILES['tempFile']['tmp_name'] = $_FILES['archivo']['tmp_name'];   //[$i]
                    $_FILES['tempFile']['error'] = $_FILES['archivo']['error']; //[$i]
                    $_FILES['tempFile']['size'] = $_FILES['archivo']['size'];   //[$i]

                    //Url donde se guardara la imagen
                    $urlDoc = "videos";
                    //Generamos un nombre random
                    $str = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                    $urlNombre = date("YmdHi")."_APPK";
                    for($j=0; $j<=5; $j++ ){
                        $urlNombre .= substr($str, rand(0,strlen($str)-1) ,1 );
                    }

                    //Validamos que exista la carpeta destino   base_url()
                    if(!file_exists($urlDoc)){
                        mkdir($urlDoc, 0647, true);
                    }

                    //Configuramos las propiedades permitidas para la imagen
                    $config['upload_path'] = $urlDoc;
                    $config['file_name'] = $urlNombre;
                    $config['allowed_types'] = "*";

                    //Cargamos la libreria y la inicializamos
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    if (!$this->upload->do_upload('tempFile')) {
                        $data['uploadError'] = $this->upload->display_errors();
                        // echo $this->upload->display_errors();
                        //$respuesta = "NO SE PUDIERON CARGAR TODOS LOS ARCHIVOS";
                        $respuesta = "";
                    }else{
                        //Si se pudo cargar la imagen la guardamos
                        $fileData = $this->upload->data();
                        $ext = explode(".",$_FILES['tempFile']['name']);
                        $path = $urlDoc."/".$urlNombre.".".$ext[count($ext)-1];

                        $evidencia = array(
                            "id" => NULL,
                            "id_servicio" => $id_servicio,
                            "id_lavador" => $id_lavador,
                            "repositorio" => "2",
                            "titulo" => ((isset($titulo_evidencia)) ? $titulo_evidencia : "Evidencia Lavado"),
                            "comentarios" => $_POST["comentarios"],
                            "url" => $path,
                            "activo" => 1,
                            "fecha_alta" => date("Y-m-d H:i:s")
                        );

                        $servicio = $this->consulta->save_register('evidencia_proceso_servicio',$evidencia);

                        $respuesta = "OK";
                    }
                //}
            }else{
                $respuesta = "SIN ARCHIVOS";
            }

            if (($respuesta == "OK")&&($_POST["evidencia"] == "1")) {
                $id_paso = $this->consulta->id_paso_seguimiento($id_servicio,$id_lavador);

                $proceso = array(
                    "evidencia_inicio" => 1,
                    "fecha_actualiza" => date("Y-m-d H:i:s")
                );

                $this->consulta->update_table_row('proceso_servicio_lavado',$proceso,'id',$id_paso);

                $this->guardar_movimiento($id_servicio,"Evidencia: ".$titulo_evidencia,$id_lavador);

                $respuesta .= " Inicio";
            }

            $contenido["status"] = "OK";
            $contenido["message"] = "OK";
            //$contenido["url"] = $path;
        } catch (Exception $e) {
            $contenido["status"] = "ERROR";
            $contenido["message"] = "ERROR";
        }

        echo json_encode($contenido);
    }

    public function send_invoice()
    {
        $_POST = json_decode(file_get_contents('php://input'), TRUE);
        $contenido = [];

        try {
            $id_servicio = $_POST["no_servicio"];
            $id_lavador = $_POST["lavador"];

            //Recuperamos datos de facturacion
            $facturacion = $this->consulta->datos_facturacion($id_servicio);
            
            //if ($facturacion != NULL) {
            if (count($facturacion) > 0) {
                //$facturacion = $facturacion[0];

                if ($facturacion != NULL) {
                    if ((float)$facturacion[0]->total > 0) {
                        $total_neto = (float)$facturacion[0]->total;
                        $total_sinva = $total_neto/1.16;
                        $iva = $total_neto - $total_sinva;
                    } else {
                        $total_sinva = 0;
                        $iva = 0;
                    }

                    $cuerpo_facturacion = array(
                        "receptor_nombre" => trim($facturacion[0]->razon_social),
                        "receptor_id_cliente" => $facturacion[0]->id_usuario,
                        "fatura_lugarExpedicion" => ((strlen($facturacion[0]->cp) > 4) ? $facturacion[0]->cp : "28983"),
                        "receptor_email" => $facturacion[0]->email_facturacion,
                        "factura_folio" => $facturacion[0]->nomenclatura.'-'.$facturacion[0]->id,
                        "factura_serie" => $facturacion[0]->id,
                        "receptor_direccion" => $facturacion[0]->domicilio,
                        "factura_formaPago" => "04",
                        "factura_medotoPago" => "PUE",
                        "receptor_RFC" => "EKU9003173C9",
                        "receptor_uso_CFDI" => $facturacion[0]->clave,    //"G01",
                        "concepto_NoIdentificacion" => "1",
                        "concepto_nombre" => $facturacion[0]->servicioNombre,
                        "concepto_precio" => $total_sinva,
                        "concepto_importe" => $total_sinva,
                        "id_producto_servicio_interno" => $facturacion[0]->id_servicio ,
                        "concepto_cantidad" => 1,
                        "importe_iva" => $iva,
                    );
                } else{
                    $cuerpo_facturacion = NULL;
                }
            }else{
                $facturacion_2 = $this->consulta->datos_factura_gen($id_servicio);

                if ($facturacion_2 != NULL) {
                    if ((float)$facturacion_2[0]->total > 0) {
                        $total_neto = (float)$facturacion_2[0]->total;
                        $total_sinva = $total_neto/1.16;
                        $iva = $total_neto - $total_sinva;
                    } else {
                        $total_sinva = 0;
                        $iva = 0;
                    }

                    $cuerpo_facturacion = array(
                        "receptor_nombre" => "General",
                        "receptor_id_cliente" => $facturacion_2[0]->id_usuario,
                        "fatura_lugarExpedicion" => "28983",
                        "receptor_email" => "jalomo@hotmail.es",
                        "factura_folio" => $facturacion_2[0]->nomenclatura.'-'.$facturacion_2[0]->id,
                        "factura_serie" => $facturacion_2[0]->id,
                        "receptor_direccion" => "Calle generica #4, Col. algun lugar",
                        "factura_formaPago" => "04",
                        "factura_medotoPago" => "PUE",
                        "receptor_RFC" => "EKU9003173C9",
                        "receptor_uso_CFDI" => "G01",
                        "concepto_NoIdentificacion" => "1",
                        "concepto_nombre" => $facturacion_2[0]->servicioNombre,
                        "concepto_precio" => $total_sinva,
                        "concepto_importe" => $total_sinva,
                        "id_producto_servicio_interno" => $facturacion_2[0]->id_servicio,
                        "concepto_cantidad" => 1,
                        "importe_iva" => $iva,
                    );
                } else{
                    $cuerpo_facturacion = NULL;
                }
            }

            if($cuerpo_facturacion != NULL){
                if($id_lavador == NULL){
                    $datos_servicio = $this->datos_base_servicio($id_servicio);
                    if (count($datos_servicio)>0) {
                        $id_lavador = $datos_servicio[count($datos_servicio)-1];
                    }
                }

                $mensaje = $this->envio_factura($cuerpo_facturacion,$id_lavador);
            }else{
                $mensaje = "NO SE ENCONTRARÓN DATOS PARA LA FACTURA";
            }

            //$contenido["datos_factura"] = $cuerpo_facturacion;
            $contenido["status"] = "OK";
            $contenido["message"] = "OK";

        } catch (Exception $e) {
            $contenido["status"] = "ERROR";
            $contenido["message"] = "ERROR";
        }

        echo json_encode($contenido);
    }

// ----------------------------------------------------------------------------------------------------//
// --------------------------Catalogo para inventarios ------------------------------------------------//
// ----------------------------------------------------------------------------------------------------//
    public function cat_supplies_operator()
    {
        //$_POST = json_decode(file_get_contents('php://input'), TRUE);
        $contenido = [];

        try {

            $checklist = $this->consulta->get_result("activo","1","inventario");

            foreach ($checklist as $serv) {
                $contenido["insumo"][] = array(
                    "id" => $serv->id,
                    "item" => $serv->nombre
                );
            }

            $contenido["status"] = "OK";
            $contenido["message"] = "OK";
        } catch (Exception $e) {
            $contenido["status"] = "ERROR";
            $contenido["message"] = "ERROR";
        }

        echo json_encode($contenido);
    }

    public function cat_indicators()
    {
        //$_POST = json_decode(file_get_contents('php://input'), TRUE);
        $contenido = [];

        try {
            
            $indicadores = $this->consulta->get_result("activo","1","cat_indicadores_diagnostico_opc");

            foreach ($indicadores as $ind) {
                if(($ind->id_cat == "1")||($ind->id_cat == "5")){
                    $contenido["interiores"][] = array(
                        "item" => $ind->leyenda,
                        "valor_si" => $ind->valor_si,
                        "valor_no" => $ind->valor_no,
                        "valor_nc" => $ind->valor_nc,
                        "img_ref" => $ind->ref_img
                    );
                } elseif ($ind->id_cat == "2") {
                    $contenido["cajuela"][] = array(
                        "item" => $ind->leyenda,
                        "valor_si" => $ind->valor_si,
                        "valor_no" => $ind->valor_no,
                        "valor_nc" => $ind->valor_nc,
                        "img_ref" => $ind->ref_img
                    );
                } elseif ($ind->id_cat == "3") {
                    $contenido["exteriores"][] = array(
                        "item" => $ind->leyenda,
                        "valor_si" => $ind->valor_si,
                        "valor_no" => $ind->valor_no,
                        "valor_nc" => $ind->valor_nc,
                        "img_ref" => $ind->ref_img
                    );
                } elseif ($ind->id_cat == "4") {
                    $contenido["documentacion"][] = array(
                        "item" => $ind->leyenda,
                        "valor_si" => $ind->valor_si,
                        "valor_no" => $ind->valor_no,
                        "valor_nc" => $ind->valor_nc,
                        "img_ref" => $ind->ref_img
                    );
                }
            }

            $contenido["status"] = "OK";
            $contenido["message"] = "OK";
        } catch (Exception $e) {
            $contenido["status"] = "ERROR";
            $contenido["message"] = "ERROR";
        }

        echo json_encode($contenido);
    }

    public function cat_evidence()
    {
        //$_POST = json_decode(file_get_contents('php://input'), TRUE);
        $contenido = [];

        try {
            $contenido["titulos_evidencia"][] = array(
                "id" => 1,
                "tipo_evidencia" => "Evidencia: Estado inicial de vehículo",
                "titulo" => "Estado inicial del vehículo",
                "paso_evidencia" => 1
            );
            $contenido["titulos_evidencia"][] = array(
                "id" => 2,
                "tipo_evidencia" => "Evidencia: Proceso de lavado",
                "titulo" => "Proceso de lavado",
                "paso_evidencia" => 2
            );
            $contenido["titulos_evidencia"][] = array(
                "id" => 3,
                "tipo_evidencia" => "Evidencia: Pre-entrega",
                "titulo" => "Lavado terminado",
                "paso_evidencia" => 3
            );
            $contenido["titulos_evidencia"][] = array(
                "id" => 4,
                "tipo_evidencia" => "Evidencia: Comprobante de pago",
                "titulo" => "Evidencia de pago",
                "paso_evidencia" => 4
            );
            $contenido["titulos_evidencia"][] = array(
                "id" => 5,
                "tipo_evidencia" => "Evidencia completada",
                "titulo" => "Estado final del vehículo",
                "paso_evidencia" => 5
            );
            $contenido["titulos_evidencia"][] = array(
                "id" => 6,
                "tipo_evidencia" => "Servicio completado",
                "titulo" => "Servicio terminado",
                "paso_evidencia" => 6
            );

            $contenido["status"] = "OK";
            $contenido["message"] = "OK";
        } catch (Exception $e) {
            $contenido["status"] = "ERROR";
            $contenido["message"] = "ERROR";
        }

        echo json_encode($contenido);
    }

// ----------------------------------------------------------------------------------------------------//
// -------------------------------  Inventarios -------------------------------------------------------//
// ----------------------------------------------------------------------------------------------------//

// --------------------------Inventario para el cliente------------------------------------------------//
    public function save_customer_diagnosis()
    {
        $_POST = json_decode(file_get_contents('php://input'), TRUE);

        try {
            if ($_POST["envio_formulario"] == "1") { 
                $contenido["id"] = NULL;
                $contenido["origen_diagnostico"] = "2";
                $contenido["id_orden"] = $_POST["id_orden"];
                $contenido["folio_info"] = $_POST["folio_info"];
            }

            $contenido["marca_danos"] = $_POST["marca_danos"];
            $contenido["img_diagrama"] = $this->base64ToImage($_POST["img_diagrama"],"diagramas");

            $contenido["interiores"] = $this->validateCheck($_POST["interiores"],"","_");
            $contenido["cajuela"] = $this->validateCheck($_POST["cajuela"],"","_");
            $contenido["exteriores"] = $this->validateCheck($_POST["exteriores"],"","_");
            $contenido["documentacion"] = $this->validateCheck($_POST["documentacion"],"","_");
            $contenido["nivel_gasolina"] = $_POST["nivel_gasolina"];

            $contenido["articulos_vehiculo"] = $_POST["articulos_vehiculo"]; 
            $contenido["articulos"] = $_POST["articulos"];
            $contenido["reporte_articulos"] = $_POST["reporte_articulos"];

            $contenido["costo_diagnostico"] = $_POST["costo_diagnostico"];
            $contenido["dia_diagnostico"] = $_POST["dia_diagnostico"];
            $contenido["mes_diagnostico"] = $_POST["mes_diagnostico"];
            $contenido["anio_diagnostico"] = $_POST["anio_diagnostico"];

            $contenido["firma_asesor"] = ((isset($_POST["firma_asesor"])) ? $this->base64ToImage($_POST["firma_asesor"],"firmas") : "");
            $contenido["nombre_asesor"] = $_POST["nombre_asesor"];

            $contenido["firma_cliente"] = ((isset($_POST["firma_cliente"])) ? $this->base64ToImage($_POST["firma_cliente"],"firmas") : "");
            $contenido["nombre_cliente"] = $_POST["nombre_cliente"];

            if ($_POST["envio_formulario"] == "1") {
                $contenido["fecha_alta"] = date("Y-m-d H:i:s");
            }

            $contenido["fecha_actualiza"] = date("Y-m-d H:i:s");

            if ($_POST["envio_formulario"] == "1") {
                //Verificamos que no se haya guardado con errores
                $revision = $this->consulta->get_result("folio_info",$_POST["folio_info"],"diagnostico");
                foreach ($revision as $row) {
                    $verificacion = $row->id;
                }

                //Si no existe el registro guardamos el formulario
                if (!isset($verificacion)) {
                    $servicio = $this->consulta->save_register('diagnostico',$contenido);
                    if ($servicio != 0) {
                        //$archivos = $this->guardar_archivos($servicio);
                        $this->save_contract($servicio);
                    } else {
                        $respuesta["status"] = "ERROR";
                        $respuesta["message"] = "ERROR";
                        //$respuesta["message"] = "OCURRIÓ UN PROBLEMA AL INTENTAR GUARDAR EL FORMULARIO";
                        //echo $respuesta;
                        echo json_encode($respuesta);
                    }

                //De lo contrario, se regresa un error
                } else {
                    $respuesta["status"] = "ERROR";
                    $respuesta["message"] = "ERROR";
                    //$respuesta["message"] = "YA HAY UN FORMULARIO REGISTRADO CON ESTE FOLIO";
                    //echo $respuesta;

                    echo json_encode($respuesta);
                }
            }else{
                $actualizar = $this->consulta->update_table_row('diagnostico',$contenido,'id_orden',$_POST["id_orden"]);
                if ($actualizar) {
                    $archivos = $this->guardar_archivos($_POST["id_orden"]);
                    //$this->save_contract(0);
                    $respuesta["status"] = "OK";
                    $respuesta["message"] = "OK";
                } else {
                    $respuesta["status"] = "ERROR";
                    $respuesta["message"] = "ERROR";
                    //$respuesta["message"] = "OCURRIÓ UN PROBLEMA AL INTENTAR GUARDAR EL FORMULARIO";
                }

                echo json_encode($respuesta);
            }
        } catch (Exception $e) {
            $respuesta["status"] = "ERROR";
            $respuesta["message"] = "ERROR";
            //$respuesta["message"] = "OCURRIDO UN ERROR MIENTRAS SE RECIBÍA LA INFORMACIÓN";
            echo json_encode($respuesta);
        }
    }

    public function save_contract($diagnostico='')
    {
        $registro = date("Y-m-d H:i:s");

        if ($_POST["envio_formulario"] == "1") {
            $contenido["id"] = NULL;
            $contenido["id_diagnostico"] = $diagnostico;
            $contenido["id_orden"] = $_POST["id_orden"];
        }

        $contenido["folio"] = $_POST["folio_info"];
        $contenido["fecha"] = $_POST["fecha_contrato"];
        $contenido["hora"] = $_POST["hora_contrato"];
        $contenido["empresa"] = $_POST["empresa_contrato"];
        $contenido["delegado"] = $_POST["delegado"];

        $contenido["monto"] = $_POST["cantidad_contrato"];
        $contenido["compartir_datos"] = ((isset($_POST["compartir_datos"])) ? $_POST["compartir_datos"] : "0");
        $contenido["envio_publicidad"] = ((isset($_POST["envio_publicidad"])) ? $_POST["envio_publicidad"] : "0");

        $contenido["firma_cliente_publicidad"] = ((isset($_POST["firma_cliente_1_contrato"])) ? $this->base64ToImage($_POST["firma_cliente_1_contrato"],"firmas") : "");
        //$this->base64ToImage($_POST["ruta_firma_cliente_c1"],"firmas");

        $contenido["firma_asesor"] = ((isset($_POST["firma_asesor_contrato"])) ? $this->base64ToImage($_POST["firma_asesor_contrato"],"firmas") : "");
        //$this->base64ToImage($_POST["ruta_ccfirma_asesor"],"firmas");
        $contenido["nombre_asesor"] = $_POST["nombre_asesor"];

        $contenido["firma_cliente"] = ((isset($_POST["firma_cliente_2_contrato"])) ? $this->base64ToImage($_POST["firma_cliente_2_contrato"],"firmas") : "");
        //$this->base64ToImage($_POST["ruta_ccfirma_cliente"],"firmas");
        $contenido["nombre_cliente"] = $_POST["nombre_cliente"];

        $contenido["privacidad"] = ((isset($_POST["privacidad"])) ? $_POST["privacidad"] : "0");

        $contenido["registro_publico"] = "XX-XXXX";
        $contenido["expediente_publico"] = "XXX.XX.X.X-XXXXX-XXXX";
        $contenido["d_expediente"] = "01";
        $contenido["m_expediente"] = "Octubre";
        $contenido["a_expediente"] = "2020";

        if ($_POST["envio_formulario"] == "1") {
            $contenido["fecha_alta"] = date("Y-m-d H:i:s");
        }

        $contenido["fecha_actualiza"] = date("Y-m-d H:i:s");

        if ($_POST["envio_formulario"] == "1") {
            $servicio = $this->consulta->save_register('contrato',$contenido);
            if ($servicio != 0) {
                //Marcamos el paso
                //$lavador = $this->session->userdata('id_usuario');
                $id_paso = $this->consulta->id_paso_seguimiento($_POST["id_orden"],$_POST["id_lavador"]);

                $proceso = array(
                    "inventario" => 1,
                    "fecha_actualiza" => date("Y-m-d H:i:s")
                );

                $this->consulta->update_table_row('proceso_servicio_lavado',$proceso,'id',$id_paso);

                //Guardamos el movimiento del servicio
                $this->guardar_movimiento($_POST["id_orden"],"Llenado de inventario",$_POST["id_lavador"]);

                $respuesta["status"] = "OK";
                $respuesta["message"] = "OK";
            } else {
                $respuesta["status"] = "ERROR";
                $respuesta["message"] = "ERROR";
            }
            
        }else{
            $actualizar = $this->consulta->update_table_row('contrato',$contenido,'id_orden',$_POST["id_orden"]);
            if ($actualizar) {
                $respuesta["status"] = "OK";
                $respuesta["message"] = "OK";
            } else {
                $respuesta["status"] = "ERROR";
                $respuesta["message"] = "ERROR";
            }
        }

        echo json_encode($respuesta);
    }
// --------------------------Inventario para lavadores ------------------------------------------------//
    public function save_operator_diagnostics()
    {
        $_POST = json_decode(file_get_contents('php://input'), TRUE);

        try {
            $validar = $this->validar_campos_operador();

            if ($validar[0]) {
                if ($_POST["envio_formulario"] == "1") {
                    $contenido["id"] = NULL;
                    $contenido["origen_diagnostico"] = "2";
                    $contenido["id_lavador"] = $_POST["id_lavador"];
                    $contenido["id_unidad"] = $_POST["id_unidad"];
                }

                if ($_POST["supervisor"] != NULL) {
                    $contenido["id_supervisor"] = $_POST["id_supervisor"];
                }

                if ($_POST["envio_formulario"] == "1") {
                    $contenido["fecha_llenado"] = $_POST["fecha_llenado"];
                }

                $contenido["lavador"] = $_POST["lavador"];
                
                if ($_POST["envio_formulario"] == "1") {
                    $contenido["firma_lavador"] = $this->base64ToImage($_POST["firma_lavador"],"firmas");
                }

                if ($_POST["supervisor"] != NULL) {
                    $contenido["supervisor"] = $_POST["supervisor"];
                    $contenido["firma_supervisor"] = $this->base64ToImage($_POST["firma_supervisor"],"firmas");
                }

                $contenido["marca_danos"] = $_POST["marca_danos"];
                //$contenido["lavador"] = $_POST["n_lavador"];
                if ($_POST["envio_formulario"] == "1") {
                    $contenido["diagrama"] = $this->base64ToImage($_POST["diagrama"],"diagramas");
                }
                $contenido["nivel_gasolina"] = $_POST["nivel_gasolina"];

                if ($_POST["comentarios_lavador"] != NULL) {
                    $contenido["comentarios_lavador"] = $_POST["comentarios_lavador"];
                }

                if ($_POST["comentarios_supervisor"] != NULL) {
                    $contenido["comentarios_supervisor"] = $_POST["comentarios_supervisor"];
                }

                $contenido["checklist_topicos"] = $this->validateCheck($_POST["checklist_topicos"],"",",");
                
                if ($_POST["envio_formulario"] == "1") {
                    $contenido["fecha_alta"] = date("Y-m-d H:i:s");
                }

                if ($_POST["supervisor"] != NULL) {
                    if ($contenido["firma_supervisor"] != "") {
                        $contenido["fecha_revision"] = $_POST["fecha_revision"];
                    }
                }

                $contenido["fecha_actualiza"] = date("Y-m-d H:i:s");

                if ($_POST["envio_formulario"] == "1") {
                    $diag = $this->consulta->save_register('diag_unidades_lavadores',$contenido);
                    if ($diag != 0) {
                        //$archivos = $this->guardar_archivos($diag);
                        $respuesta["status"] = "OK";
                        $respuesta["message"] = "OK";
                    } else {
                        $respuesta["status"] = "ERROR";
                        $respuesta["message"] = "ERROR";
                    }
                    
                }else{
                    $actualizar = $this->consulta->update_table_row('diag_unidades_lavadores',$contenido,'id',$_POST["id_diagnostico"]);
                    if ($actualizar) {
                        //$archivos = $this->guardar_archivos($_POST["id_diagnostico"]);
                        $respuesta["status"] = "OK";
                        $respuesta["message"] = "OK";
                    } else {
                        $respuesta["status"] = "ERROR";
                        $respuesta["message"] = "ERROR";
                    }
                }
            } else {
                $respuesta["status"] = "ERROR";
                $respuesta["message"] = $validar[1];
            }
                
        } catch (Exception $e) {
            $respuesta["status"] = "ERROR";
            $respuesta["message"] = "ERROR";
        }

        echo json_encode($respuesta);
    }

// ----------------------------------------------------------------------------------------------------//
// --------------------------Procesos en genral para guardado------------------------------------------//
// ----------------------------------------------------------------------------------------------------//
    
    function validar_campos_operador()
    {
        $retorno = TRUE;
        $errores = [];

        if(($_POST["origen_diagnostico"] == "") || ($_POST["origen_diagnostico"] == NULL)){
            $retorno = FALSE;
            $errores[] = "origen_diagnostico required field";
        }
        
        if(($_POST["id_lavador"] == "") || ($_POST["id_lavador"] == NULL)){
            $retorno = FALSE;
            $errores[] = "id_lavador required field";
        }

        if(($_POST["id_unidad"] == "") || ($_POST["id_unidad"] == NULL)){
            $retorno = FALSE;
            $errores[] = "id_unidad required field";
        }

        /*if(($_POST["id_supervisor"] == "") || ($_POST["id_supervisor"] == NULL)){
            $retorno = FALSE;
            $errores[] = "id_supervisor required field";
        }*/

        if(($_POST["fecha_llenado"] == "") || ($_POST["fecha_llenado"] == NULL)){
            $retorno = FALSE;
            $errores[] = "fecha_llenado required field";
        }

        if(($_POST["lavador"] == "") || ($_POST["lavador"] == NULL)){
            $retorno = FALSE;
            $errores[] = "lavador required field";
        }

        if(($_POST["firma_lavador"] == "") || ($_POST["firma_lavador"] == NULL)){
            $retorno = FALSE;
            $errores[] = "firma_lavador required field";
        }

        if(($_POST["marca_danos"] == "") || ($_POST["marca_danos"] == NULL)){
            $retorno = FALSE;
            $errores[] = "marca_danos required field";
        }

        if(($_POST["diagrama"] == "") || ($_POST["diagrama"] == NULL)){
            $retorno = FALSE;
            $errores[] = "diagrama required field";
        }

        if(($_POST["nivel_gasolina"] == "") || ($_POST["nivel_gasolina"] == NULL)){
            $retorno = FALSE;
            $errores[] = "nivel_gasolina required field";
        }

        if(($_POST["comentarios_lavador"] == "") || ($_POST["comentarios_lavador"] == NULL)){
            $retorno = FALSE;
            $errores[] = "comentarios_lavador required field";
        }

        if (isset($_POST["checklist_topicos"])) {
            if(count($_POST["checklist_topicos"]) == 0){
                $retorno = FALSE;
                $errores[] = "checklist_topicos required field";
            }
        } else {
            $retorno = FALSE;
            $errores[] = "checklist_topicos required field";
        }

        if(($_POST["envio_formulario"] == "") || ($_POST["envio_formulario"] == NULL)){
            $retorno = FALSE;
            $errores[] = "envio_formulario required field";
        }

        return [$retorno,$errores];
    }


    public function guardar_diagrama()
    {
        $img_diagrama = $this->base64ToImage($_POST["diagrama"],"diagramas");
        echo "OK=".$img_diagrama;
    }

    public function descomprimir()
    {
        $respuesta = "";
        for ($i=1; $i <31 ; $i++) {
            if (isset($_POST["encript_".$i])) {
                $respuesta .= trim($_POST["encript_".$i]);
            }
        }
        return $respuesta;
    }

    /*public function guardar_evidencia()
    {
        $lavador = $this->session->userdata('id_usuario');

        if(count($_FILES['archivo']['tmp_name']) > 0){
            //Como el elemento es un arreglos utilizamos foreach para extraer todos los valores
            for ($i = 0; $i < count($_FILES['archivo']['tmp_name']); $i++) {
                $_FILES['tempFile']['name'] = $_FILES['archivo']['name'][$i];
                $_FILES['tempFile']['type'] = $_FILES['archivo']['type'][$i];
                $_FILES['tempFile']['tmp_name'] = $_FILES['archivo']['tmp_name'][$i];
                $_FILES['tempFile']['error'] = $_FILES['archivo']['error'][$i];
                $_FILES['tempFile']['size'] = $_FILES['archivo']['size'][$i];

                //Url donde se guardara la imagen
                $urlDoc = "videos";
                //Generamos un nombre random
                $str = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                $urlNombre = date("YmdHi")."_";
                for($j=0; $j<=7; $j++ ){
                    $urlNombre .= substr($str, rand(0,strlen($str)-1) ,1 );
                }

                //Validamos que exista la carpeta destino   base_url()
                if(!file_exists($urlDoc)){
                    mkdir($urlDoc, 0647, true);
                }

                //Configuramos las propiedades permitidas para la imagen
                $config['upload_path'] = $urlDoc;
                $config['file_name'] = $urlNombre;
                $config['allowed_types'] = "*";

                //Cargamos la libreria y la inicializamos
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('tempFile')) {
                    $data['uploadError'] = $this->upload->display_errors();
                    // echo $this->upload->display_errors();
                    //$respuesta = "NO SE PUDIERON CARGAR TODOS LOS ARCHIVOS";
                    $respuesta = "";
                }else{
                    //Si se pudo cargar la imagen la guardamos
                    $fileData = $this->upload->data();
                    $ext = explode(".",$_FILES['tempFile']['name']);
                    $path = $urlDoc."/".$urlNombre.".".$ext[count($ext)-1];

                    $evidencia = array(
                        "id" => NULL,
                        "id_servicio" => $_POST["servicio"],
                        "id_lavador" => $lavador,
                        "repositorio" => "2",
                        "titulo" => ((isset($_POST["tt_evidencia"])) ? $_POST["tt_evidencia"] : "Evidencia Lavado"),
                        "comentarios" => $_POST["comentarios"],
                        "url" => $path,
                        "activo" => 1,
                        "fecha_alta" => date("Y-m-d H:i:s")
                    );

                    $servicio = $this->consulta->save_register('evidencia_proceso_servicio',$evidencia);

                    $respuesta = "OK";
                }
            }
        }else{
            $respuesta = "SIN ARCHIVOS";
        }

        if (($respuesta == "OK")&&($_POST["paso_evidencia"] == "1")) {
            $id_paso = $this->consulta->id_paso_seguimiento($_POST["servicio"],$lavador);

            $proceso = array(
                "evidencia_inicio" => 1,
                "fecha_actualiza" => date("Y-m-d H:i:s")
            );

            $this->consulta->update_table_row('proceso_servicio_lavado',$proceso,'id',$id_paso);

            $this->guardar_movimiento($_POST["servicio"],"Evidencia: ".$_POST["tt_evidencia"]);
        }

        echo $respuesta;
    }*/

    public function guardar_archivos($diagnostico='')
    {
        ini_set('max_execution_time',1800);
        ini_set('set_time_limit',1800);

        if(count($_FILES['archivo']['tmp_name']) > 0){
            //Como el elemento es un arreglos utilizamos foreach para extraer todos los valores
            for ($i = 0; $i < count($_FILES['archivo']['tmp_name']); $i++) {
                $_FILES['tempFile']['name'] = $_FILES['archivo']['name'][$i];
                $_FILES['tempFile']['type'] = $_FILES['archivo']['type'][$i];
                $_FILES['tempFile']['tmp_name'] = $_FILES['archivo']['tmp_name'][$i];
                $_FILES['tempFile']['error'] = $_FILES['archivo']['error'][$i];
                $_FILES['tempFile']['size'] = $_FILES['archivo']['size'][$i];

                //Url donde se guardara la imagen
                $urlDoc = "videos";
                //Generamos un nombre random
                $str = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                $urlNombre = date("YmdHi")."_";
                for($j=0; $j<=7; $j++ ){
                    $urlNombre .= substr($str, rand(0,strlen($str)-1) ,1 );
                }

                //Validamos que exista la carpeta destino   base_url()
                if(!file_exists($urlDoc)){
                    mkdir($urlDoc, 0647, true);
                }

                //Configuramos las propiedades permitidas para la imagen
                $config['upload_path'] = $urlDoc;
                $config['file_name'] = $urlNombre;
                $config['allowed_types'] = "*";

                //Cargamos la libreria y la inicializamos
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('tempFile')) {
                    $data['uploadError'] = $this->upload->display_errors();
                    // echo $this->upload->display_errors();
                    //$respuesta = "NO SE PUDIERON CARGAR TODOS LOS ARCHIVOS";
                    $respuesta = "";
                }else{
                    //Si se pudo cargar la imagen la guardamos
                    $fileData = $this->upload->data();
                    $ext = explode(".",$_FILES['tempFile']['name']);
                    $path = $urlDoc."/".$urlNombre.".".$ext[count($ext)-1];

                    $evidencia = array(
                        "id" => NULL,
                        "id_orden" => $_POST["id_infop"],
                        "inventario" => "servicio",
                        "repositorio" => "2",
                        "evidencia" => $path,
                        "activo" => 1,
                        "fecha_alta" => date("Y-m-d H:i:s")
                    );

                    $servicio = $this->consulta->save_register('evidencia_inventario',$evidencia);

                    $respuesta = "OK";
                }
            }
        }else{
            $respuesta = "OK";
        }

        return $respuesta;
    }

    function base64ToImage($imgBase64 = "",$direcctorio = "") {
        //Verificamos si se guarda la imagen o si se envio las url
        if (substr($imgBase64,0,8) != "statics/") {
            //Generamos un nombre random para la imagen
            $str = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            $urlNombre = date("YmdHi")."_";
            for($i=0; $i<=7; $i++ ){
                $urlNombre .= substr($str, rand(0,strlen($str)-1) ,1 );
            }

            $urlDoc = 'statics/inventario/'.$direcctorio;

            $data = explode(',', $imgBase64);
            //Creamos la ruta donde se guardara la firma y su extensión
            if ($data[0] == "data:image/png;base64") {
                $base ='statics/inventario/'.$direcctorio.'/'.$urlNombre.".png";
                $Base64Img = base64_decode($data[1]);
                file_put_contents($base, $Base64Img);
                
            }elseif ($data[0] == "data:image/jpeg;base64") {
                $base ='statics/inventario/'.$direcctorio.'/'.$urlNombre.".jpeg";

                $Base64Img = base64_decode($data[1]);
                file_put_contents($base, $Base64Img);
            }else {
                $base = "";
            }
        } else {
            $base = $imgBase64;
        }

        return $base;
    }

    public function validateCheck($campo = NULL,$elseValue = "",$separador = "")
    {
        if ($campo == NULL) {
            $respuesta = $elseValue;
        }else {
            $respuesta = "";
            if (count($campo) >1){
                //Separamos los index de los id de tipo de usuario
                for ($i=0; $i < count($campo) ; $i++) {
                    $respuesta .= $campo[$i].$separador;
                }
            }
        }
        return $respuesta;
    }

    public function guardar_movimiento($id_servicio='',$movimiento = '',$id_lavador = '')
    {
        //Recuperamos los datos del servicio
        $datos_servicio = $this->consulta->datos_base_servicio($id_servicio);

        //Recuperamos el nombre del ususrio
        $usuario = $this->consulta->nombre_lavador($id_lavador);

        if (count($datos_servicio)>0) {
            $contenedor = array(
                "id" => NULL,
                "id_servicio" => $id_servicio,
                "id_lavador" => $usuario,
                "id_cliente" => $datos_servicio[0],
                "movimento" => $movimiento. ".Movimiento por api",
                "tipo_usuario" => "Externo (App)",
                "fecha_alta" => date("Y-m-d H:i:s")
            );

            $this->consulta->save_register('historial_procesos_servicio',$contenedor);
        }

        return TRUE;
    }

    public function actualizar_pago($id_servicio = '',$id_lavador = '')
    {
        //Actualizamos el cambo en la tabla de servicio lavado
        $proceso = array(
            "pago" => 1
        );

        $this->consulta->update_table_row('servicio_lavado',$proceso,'id',$id_servicio);
        //Recuperamos el nombre del ususrio
        $usuario = $this->consulta->nombre_lavador($id_lavador);
        //Registramos el movimiento en historial de pagos
        $contenedor = array(
            "id" => NULL,
            "id_servicio" => $id_servicio,
            "usuario" => $usuario,
            "created_at" => date("Y-m-d H:i:s")
        );

        $this->consulta->save_register('historial_pago',$contenedor);
    }

    public function cambio_status($status = '',$id_servicio = '',$id_lavador = '')
    {
        $usuario = $this->consulta->nombre_lavador($id_lavador);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://hexadms.com/xehos/index.php/api/cambiarStatus");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt(
            $ch,
            CURLOPT_POSTFIELDS,
            "id_status_actual=" . ((int)$status - 1) . 
            "&usuario=" . $usuario . 
            "&id_status_nuevo=" . $status .
            "&id_servicio=" . $id_servicio . ""
        );
        // Receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        //var_dump($server_output );
        curl_close($ch);
    }

    public function validar_seguro_lluvia($id_servicio = '')
    {
        $data = json_decode(file_get_contents('php://input'), true);
        // $curl is the handle of the resource
        $curl = curl_init();
        // set the URL and other options
        curl_setopt($curl, CURLOPT_URL, "https://xehos.com/xehos_web/api/segurolluvia?servicio_id=".$id_servicio);
        //set the content type to application/json
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        //return response instead of outputting
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        // execute and pass the result to browser
        $server_output = curl_exec($curl);
        // close the cURL resource
        curl_close($curl);
        //var_dump($server_output );
        $respuesta = json_decode($server_output);
        //var_dump($respuesta->seguro_lluvia);
        return ((isset($respuesta->seguro_lluvia)) ? $respuesta->seguro_lluvia : FALSE);
    }

    public function envio_factura($factura = NULL,$id_lavador = '')
    {
        //var_dump($_REQUEST);
        $url = "https://sohexs.com/facturacion/index.php/api_factura/crear_factura";

        $curl = curl_init($url);
        //Para que la peticion no imprima el resultado como una cadena, y podamos manipularlo
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        //Adjuntamos el json a nuestra petición
        curl_setopt($curl, CURLOPT_POSTFIELDS,$factura);
        //return response instead of outputting
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        //Obtenemos la respuesta
        $response = curl_exec($curl);

        // Se cierra el recurso CURL y se liberan los recursos del sistema
        curl_close($curl);
        $result = json_decode($response,TRUE);
        //echo $response;
        //if($response) {
            $id_paso = $this->consulta->id_paso_seguimiento($factura["factura_serie"],$id_lavador);

            $proceso = array(
                "pago" => 1,
                "evidencia_pago" => 1
            );

            if ($result["error"] == "0") {
                $proceso["error"] = $result["error_mensaje"];
                $proceso["timbre_factura"] = $result["factura"];

                $this->guardar_movimiento($factura["factura_serie"],"Envio de facturación. ".$result["error_mensaje"],$id_lavador);
            }

            $proceso["fecha_actualiza"] = date("Y-m-d H:i:s");

            $this->consulta->update_table_row('proceso_servicio_lavado',$proceso,'id',$id_paso);

            //Guardamos el movimiento del servicio
            $this->guardar_movimiento($factura["factura_serie"],"Finaliza toma de evidencia del comprobante de pago",$id_lavador);

            //Actualizamos el historial de pagos
            $this->actualizar_pago($factura["factura_serie"],$id_lavador);

            return $result["error_mensaje"];
        //}else{
            //echo "ERROR";
        //}
    }

    public function facturacion_generica($id_servicio = 0)
    {
        //Recuperamos algunos datos de facturacion 
        $facturacion = $this->consulta->datos_factura_gen($id_servicio);
        if ($facturacion != NULL) {
            $datos = array(
                "razon_social" => "General",
                "id_usuario" => $facturacion[0]->id_usuario,
                "cp" => "28983",
                "email_facturacion" => "jalomo@hotmail.es",
                "nomenclatura" => $facturacion[0]->nomenclatura,
                "id" => $facturacion[0]->id,
                "domicilio" => "colonia lejos",
                "factura_formaPago" => "04",
                "factura_medotoPago" => "PUE",
                "rfc" => "EKU9003173C9",
                "clave" => "G01",
                "concepto_NoIdentificacion" => "1",
                "servicioNombre" => $facturacion[0]->servicioNombre,
                "total" => $facturacion[0]->total,
                "id_servicio" => $facturacion[0]->id_servicio,
                "id_cupon_servicio" => $facturacion[0]->id_cupon_servicio,
                "cupon" => $facturacion[0]->cupon,
                "descuento" => $facturacion[0]->descuento,
            );
        }else{
            $datos = NULL;
        }
        return $datos;
    }
// ------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------
    public function service_contract()
    {
        $_POST = json_decode(file_get_contents('php://input'), TRUE);
        $contenido = [];

        try {
            $id_servicio = $_POST["no_servicio"];
            //Confirmamos si existe un contrato para vaciar o es nuevo
            $contrato = $this->datos_contrato($id_servicio);
            
            $contenido["contract"]["header"] = "SISTEMAS OPERATIVOS HEXADECIMAL, S.A. DE C.V. 
                                        AV. IGNACIO SANDOVAL 1939. LOC. 6. PLAZA MILAN. 
                                        COL. PASEO DE LA CANTERA. C.P. 28017, COLIMA, COLIMA 
                                        RFC: SOH190520AR4.   Tels. 33 3835 2043 / 33 3835 2058
                                        Email: info@xehos.com (Quejas y Reclamaciones)";

            $contenido["contract"]["header2_a"] = "Folio No. ";
            $contenido["contract"]["folio_info"] = $contrato["folio"];
            $contenido["contract"]["header2_b"] = "Fecha: ";
            $contenido["contract"]["fecha_contrato"] = $contrato["fecha_contrato"];
            $contenido["contract"]["header2_c"] = "Hora: ";
            $contenido["contract"]["hora_contrato"] = $contrato["hora_contrato"];

            $contenido["contract"]["section_1a"] = "CONTRATO DE PRESTACIÓN DE SERVICIO DE AUTOLAVADO Y DETALLADO  DE VEHÍCULOS QUE CELEBRAN POR UNA PARTE";
            $contenido["contract"]["empresa_contrato"] = $contrato["empresa_contrato"];
            $contenido["contract"]["section_1b"] = ", AUTORIZADO ";
            $contenido["contract"]["autoriza_contrato"] = $contrato["autoriza_contrato"];
            $contenido["contract"]["section_1c"] = "Y POR LA OTRA EL CONSUMIDOR CUYO NOMBRE SE SEÑALA EN EL INICIO DEL PRESENTE, A QUIENES EN LO SUCESIVO Y PARA EFECTO DEL PRESENTE CONTRATO SE LE DENOMINARA “EL PROVEEDOR” Y “EL CONSUMIDOR”, RESPECTIVAMENTE. ";

            $contenido["contract"]["title_section_a"] = "DECLARACIONES";
            $contenido["contract"]["section_a"] = "Las partes denominadas CONSUMIDOR y PROVEEDOR, se reconocen las personalidades con las cuales se ostentan y que se encuentran especificadas en este documento (orden de servicio y contrato), por lo cual, están dispuestas a sujetarse a las condiciones que se establecen en las siguientes:";

            $contenido["contract"]["title_section_b"] = "CLÁUSULAS";
            $contenido["contract"]["section_b"] = "1.- Objeto: El PROVEEDOR realizará todo el  Lavado. Limpieza. Detallado y/o Semi-Detallados, solicitados por el CONSUMIDOR que suscribe el presente contrato, a las que se someterá el vehículo que al inicio se detalla, para obtener las condiciones de limpieza de acuerdo a lo solicitado por el CONSUMIDOR y que serán realizadas a cargo y por cuenta del CONSUMIDOR. EL PROVEEDOR no condicionará en modo alguno la prestación de servicios de Lavado. Limpieza. Detallado y/o Semi-Detallados del vehículo, a la adquisición o renta de otros productos o servicios en el mismo servicio, predeterminada. El precio total de los servicios contratados se establece en el presupuesto que forma parte del presente y se describe en su inicio; dicho precio será pagado por el CONSUMIDOR antes de darle el servicio al vehículo;  Lavado. Detallado y/o Semi-Detallados, todo pago efectuado por el CONSUMIDOR deberá efectuarse en el instrumento App. Ios y Android, descargables de tiendas oficiales. O en la terminal punto de venta, portada por el operador que prestara el servicio del PROVEEDOR solo de forma digital o tarjetas debito y/o tarjeta de crédito  y en Moneda Nacional o extranjera al tipo de cambio vigente al día de pago, NO SE RECIBEN PAGOS EN EFECTIVO. Solo mediante tarjeta de crédito o transferencia bancaria efectuada con anterioridad a realizar el servicio al vehículo. ";

            $contenido["contract"]["title_section_c"] = "DE LAS CONDICIONES GENERALES:";
            $contenido["contract"]["section_c"] = "2.- Las partes están de acuerdo en que las condiciones generales en las que se encuentra el automóvil de acuerdo con el inventario visual al momento de su recepción, son las que se definen en la orden de servicio-presupuesto que se encuentra al inicio del presente contrato. El CONSUMIDOR tendrá la obligación de aceptar dicho inventario con firma fotostática, misma que será de forma digital, con la que da aceptación y en conformidad con lo expuesto ";

            $contenido["contract"]["title_section_d"] = "DEL PRESUPUESTO";
            $contenido["contract"]["section_d1"] = "3.- El PROVEEDOR se obliga ante el CONSUMIDOR a respetar el presupuesto contenido en el presente contrato. ".
                "4.- El CONSUMIDOR se obliga pagar a el PROVEEDOR por el servicio solicitado para su automóvil la cantidad de $";
            $contenido["contract"]["cantidad_contrato"] = $contrato["cantidad_contrato"];
            $contenido["contract"]["section_d2"] = ". (MONEDA NACIONAL), siempre y cuando no sea un vehículo distinto a lo solicitado en el servicio, en caso contrario será modificada la orden de servicio y se realizaran ajustes en costos y/o presupuestos por prestación de servicio. ";


            $contenido["contract"]["title_section_e"] = "DE LA PRESTACIÓN DE SERVICIOS";
            $contenido["contract"]["section_e"] = "5.- Las partes convienen en que la fecha de aceptación del presupuesto es la que se indica en el inicio de este contrato.
                6.- El PROVEEDOR se obliga a realizar el servicio de  Lavado. Detallado y/o Semi-Detallados, según sea el servicio contratado, en hora previamente establecido en el presupuesto. El PROVEEDOR se obliga a emplear productos adecuados y afines para los servicios contratados salvo que el CONSUMIDOR ordene o autorice expresamente y por escrito se utilicen otros o las provea, de conformidad con lo establecido en el artículo 60 de la Ley Federal de Protección al Consumidor, caso en el cual el Lavado. Detallado y/o Semi-Detallados del vehículo tendrá garantía por lluvias, en lo que se relacione a ese servicio, durante las primeras 24 horas continuas a su servicio. Siendo este un Lavado Express solo en el exterior del vehículo.
                7.- El PROVEEDOR no podrá usar el vehículo para fines propios o de terceros. El PROVEEDOR se hace responsable por los daños causados al vehículo, como consecuencia del servicio efectuados por parte de su personal. Cuando el CONSUMIDOR, solicite que él o un representante suyo sea quien conduzca el automóvil para poder realizar el servicio, el riesgo, será por su cuenta.
                8.- El PROVEEDOR se hace responsable por las posibles, daños o pérdidas parciales o totales imputables a él o a sus subalternos que sufra el vehículo, el equipo y los aditamentos adicionales que el CONSUMIDOR le haya notificado que existen en el momento de la recepción del mismo, o que se causen a terceros, mientras el vehículo se encuentre bajo su resguardo, salvo los ocasionados por desperfectos mecánicos o a resultas de piezas gastadas o sentidas, por incendio motivado por deficiencia eléctrica, o fallas en el sistema de combustible, y no causadas por el PROVEEDOR; para tal efecto el PROVEEDOR cuenta con un seguro suficiente para cubrir dichas eventualidades, bajo póliza expedida por compañía de seguros autorizada al efecto. El PROVEEDOR no se hace responsable por la pérdida de objetos dejados en el interior del automóvil, aun con la cajuela cerrada, salvo que estos le hayan sido notificados y puestos bajo su resguardo al momento de la recepción del automóvil. El PROVEEDOR tampoco se hace responsable por daños causados por fuerza mayor o caso fortuito, ni por la situación legal del automóvil cuando éste previamente haya sido robado o se hubiere utilizado en la comisión de algún ilícito; lo anterior, salvo que alguna de éstas cuestiones resultara legalmente imputable al PROVEEDOR; así mismo, el CONSUMIDOR, libera al PROVEEDOR de cualquier responsabilidad que surja o pueda surgir con relación al origen, posesión o cualquier otro derecho inherente al vehículo o a partes y componentes del mismo, obligándose en lo personal y en nombre del propietario, a responder de su procedencia. ";

            $contenido["contract"]["title_section_f"] = "DEL PRECIO Y FORMAS DE PAGO";
            $contenido["contract"]["section_f"] = "9.- El CONSUMIDOR acepta haber tenido a su disposición los precios de la tarifa de mano de obra, Lavado. Detallado y/o Semi-Detallados, según sea el servicio contratado. El PROVEEDOR; los incrementos que resulten durante el servicio, por costos no previsibles y/o incrementos que resulten al momento de la ejecución del servicio por ser un vehículo distinto al cual se le realizara el servicio, deberán ser autorizados por el CONSUMIDOR, antes de realizar el servicio, y teniendo que pagar la s diferencias correspondientes, equivalentes al automóvil, El tiempo que, en su caso, que transcurra para cumplir esta condición, modificará la hora de entrega, en la misma proporción. Todas las quejas y sugerencias serán atendidas en el correo, teléfonos y horarios de atención señalados en la parte superior del presente o en su inicio.
                10.- Será obligación del PROVEEDOR expedir la factura correspondiente por los servicios y productos que preste o enajene; el importe total del servicio, así como el precio por concepto del servicio, quedará especificado en ella, conforme a la ley.
                11.- El CONSUMIDOR se obliga a pagar de contado al PROVEEDOR (conforme a lo establecido en la cláusula 1), en los medios dispuestos ; DIGITALES APP. WEB, de éste y POR ADELANTADO AL SERVICO CONTRATADO. A realizar al  automóvil, el importe del servicio, de conformidad con el presupuesto elaborado para tal efecto.";

            $contenido["contract"]["title_section_g"] = "DE LA ENTREGA.";
            $contenido["contract"]["section_g"] = "12.- El PROVEEDOR se obliga a hacer la entrega del automóvil LIMPIO, en la hora establecida en este contrato, pudiendo ampliarse dicho plazo en caso fortuito o de fuerza mayor, en cuyo caso será obligación del PROVEEDOR dar aviso previo al CONSUMIDOR de la causa y del tiempo que se ampliará el plazo de entrega. Que en ese caso no podrá ser más de 30 Min. 
                13.- La basura o residuos, derivados del servicio de lavado, quedarán a disposición del CONSUMIDOR al momento de hacerle entrega del automóvil.
                14.- El CONSUMIDOR se obliga a hacer el pago, antes de realizarse el servicio. Y al momento que reciba el automóvil con el servicio de Lavado. Detallado y/o Semi-Detallados, según sea el servicio contratado  realizado , se entenderá que esto fue a completa satisfacción del CONSUMIDOR en lo que respecta a sus condiciones generales de acuerdo al inventario visual, mismas que fueron descritos en la orden de servicio, así como también en cuanto al Lavado. Detallado y/o Semi-Detallados, según sea el servicio contratado efectuada, sin afectar sus derechos a ejercer la garantía.";

            $contenido["contract"]["title_section_h"] = "DE LA RESCISIÓN Y PENAS CONVENCIONALES";
            $contenido["contract"]["section_h"] = "15.- Es causa de rescisión del presente contrato: A.- Que EL PROVEEDOR incumpla en la hora y lugar de entrega del vehículo por causas propias, caso en el cual el CONSUMIDOR notificará por escrito del incumplimiento al PROVEEDOR, y éste hará entrega inmediata del vehículo debidamente con su servicio conforme y presupuesto establecido, descontando del precio pactado para la prestación del servicio, la suma equivalente al 2% (dos por ciento) del total del servicio, que se pacta como pena convencional; B.- Que el CONSUMIDOR incumpla con el pago del servicio ordenado, en el término previsto en la cláusula 19, caso en el cual el PROVEEDOR le notificará por escrito su incumplimiento, y podrá optar por exigir la recisión o cumplimiento forzoso de la obligación, cobrando la misma pena pactada del 2% al CONSUMIDOR, por la mora.";

            $contenido["contract"]["title_section_i"] = "DE LAS GARANTÍAS";
            $contenido["contract"]["section_i"] = "16.- <u>Las reparaciones a que se refiere el presupuesto aceptado por el CONSUMIDOR y éste contrato están garantizadas por 24 horas naturales contados a partir de la hora de la entrega del vehículo ya Lavado</u>. Detallado y/o Semi-Detallados, según sea el servicio contratado, Si el vehículo es intervenido por un tercero, el PROVEEDOR no será responsable y la garantía quedará sin efecto. Las reclamaciones por garantía o solicitud de la misma  se harán vía App. Correo y/o telefónica  del PROVEEDOR, para lo cual el CONSUMIDOR pueda optar por su derecho, el servicio efectuado por el PROVEEDOR en cumplimiento a la garantía del servicio serán sin cargo alguno para el CONSUMIDOR. Esta garantía cubre cualquier lluvia o llovizna  por causas imputables al mismo y solo será válida siempre y cuando el automóvil se haya utilizado en condiciones de uso normal, se hayan observado en su uso las indicaciones de manejo y servicio que se le hubiera dado. En todo caso, el PROVEEDOR será corresponsable y solidario con los terceros del cumplimiento o incumplimiento de las garantías por ellos otorgadas en lo que se relacione al servicios a éste realizados, siempre que hayan sido contratados ante el CONSUMIDOR.
                17.- Toda reclamación dentro del término de garantía, deberá ser realizada ante el PROVEEDOR que efectuó el servicio y en el correo, teléfonos y horarios de atención señalados en la parte superior del presente o en su inicio del presente contrato. En caso de que sea necesario hacer válida la garantía en un domicilio diverso al del PROVEEDOR, los gastos por ello deberán ser cubiertos por éste, siempre y cuando la garantía proceda, dichos gastos sean indispensables para tal fin, y sea igualmente indispensable realizar el servicio al vehículo en domicilio diverso al del PROVEEDOR, pero en dado caso, si el automóvil esta fuera de la entidad donde se localiza el PROVEEDOR, éste inmediatamente después de que tenga conocimiento, podrá indicar al CONSUMIDOR en donde se encuentra una filial de la marca más cercana, para hacer efectiva la garantía por conducto de ésta, si procede, debiendo acreditar con la factura correspondiente al servicio efectuado, con el objeto, de no hacer ningún cargo por ello al CONSUMIDOR. Con el objeto de dar cumplimiento a las obligaciones que ésta cláusula le impone, El PROVEEDOR señala como teléfonos de atención al CONSUMIDOR los que aparecen en éste contrato. Para los efectos de la atención y resolución de quejas y reclamaciones, estas deberán ser presentadas dentro de días y horas hábiles, que son los detallados en la parte superior del presente, en el correo, teléfonos y horarios de atención señalados en la parte superior del presente o en su inicio, al  PROVEEDOR, detallando en forma expresa la causa o motivo de la reclamación; la Gerencia de Servicio procederá a analizar la queja y resolverá lo conducente dentro de un lapso de tres días hábiles, procediendo al servicio del vehículo o manifestando las causas de improcedencia de la reclamación, en forma escrita. ";

            $contenido["contract"]["title_section_j"] = "DE LA INFORMACIÓN Y PUBLICIDAD";
            $contenido["contract"]["section_j1"] = "18.- El PROVEEDOR se obliga a observar en todo momento lo dispuesto por los capítulos III y IV de la Ley Federal de Protección al Consumidor, en cuanto a la información, publicidad, promociones y ofertas.".
                "19.- El CONSUMIDOR  ".(( $contrato["publicidad_contrato"] == "1") ? "( Si [ X ]   No [  ] )" : "( Si [  ]   No [ X ] )" )."  acepta que el PROVEEDOR ceda o transmita a tercero, con fines mercadotécnicos o publicitarios, la información proporcionada por él con motivo del presente contrato, y ".(( $contrato["envio_publicidad"] == "1") ? "( Si [ X ]   No [  ] )" : "( Si [  ]   No [ X ] )" )." acepta que el PROVEEDOR le envíe publicidad sobre bienes y servicios, firmando en éste espacio.";
            $contenido["contract"]["firma_cliente_1_contrato"] = $contrato["firma_cliente_c1"];

            $contenido["contract"]["title_section_k"] = "DE LA INFORMACIÓN Y PUBLICIDAD";
            $contenido["contract"]["section_k1"] = "20.- La Procuraduría Federal del Consumidor, es competente para conocer y resolver en la vía administrativa de cualquier controversia que se suscite sobre la interpretación o cumplimiento del presente contrato, por lo que las partes están de acuerdo en someterse a ella en términos de ley, para resolver sobre la interpretación o cumplimiento de los términos del presente contrato y de las disposiciones de la Ley Federal de Protección al Consumidor, la Norma Oficial Mexicana NOM-174-SCFI-2007, Prácticas Comerciales- Elementos de Información para la Prestación de Servicios en General y cualquier otra disposición aplicable. Sin perjuicio de lo anterior en caso de persistir la inconformidad, las partes se someten a la jurisdicción de los tribunales competentes del domicilio del PROVEEDOR, renunciando en forma expresa a cualquier otra jurisdicción o al fuero que pudiera corresponderles en razón de sus domicilios presente o futuros o por cualquier otra razón. ";
            $contenido["contract"]["firma_asesor_contrato"] = $contrato["ccfirma_asesor"];
            $contenido["contract"]["nombre_asesor"] = $contrato["cnombre_asesor"];
            $contenido["contract"]["section_k2"] = "EL PROVEEDOR (NOMBRE Y FIRMA). OPERADOR";
            $contenido["contract"]["firma_cliente_2_contrato"] = $contrato["ccfirma_cliente"];
            $contenido["contract"]["nombre_cliente"] = $contrato["cnombre_cliente"];
            $contenido["contract"]["section_k3"] = "EL CONSUMIDOR (NOMBRE Y FIRMA) .CLIENTE";

            $contenido["contract"]["title_section_l"] = "AVISO DE PRIVACIDAD";
            $contenido["contract"]["section_l1"] = "EL Aviso de Privacidad Integral podrá ser consultado en nuestra página en internet www.xehos.com/privacidad/, o lo puede solicitar al correo electrónico info@xehos.com, u obtener personalmente en el Área de Atención a la Privacidad.";
            $contenido["contract"]["section_l2"] = (( $contrato["privacidad_contrato"] == "1") ? "( Si [ X ]   No [  ] )" : "( Si [  ]   No [ X ] )" )." Al marcar el recuadro precedente y remitir mis datos, otorgo mi consentimiento para que mis datos personales sean tratados conforme a lo señalado en el Aviso de Privacidad. 
                Este contrato fue registrado ante la Procuraduría Federal del Consumidor. Registro público de contratos de adhesión y aprobado e inscrito con el número ";
            $contenido["contract"]["registro_publico"] = $contrato["registro_publico"];
            $contenido["contract"]["section_l3"] = " Expediente No. ";
            $contenido["contract"]["expediente_publico"] = $contrato["expediente_publico"];
            $contenido["contract"]["section_l4"] = " de fecha ";
            $contenido["contract"]["registro_publico_dia"] = $contrato["registro_publico_dia"];
            $contenido["contract"]["section_l5"] = " de ";
            $contenido["contract"]["registro_publico_mes"] = $contrato["registro_publico_mes"];
            $contenido["contract"]["section_l6"] = " de ";
            $contenido["contract"]["registro_publico_anio"] = $contrato["registro_publico_anio"];
            $contenido["contract"]["section_l7"] = "Cualquier variación del presente contrato en perjuicio de EL CONSUMIDOR, frente al contrato de adhesión registrado, se tendrá por no puesta.";
            $contenido["contract"]["section_l8"] = "“DESEO ADHERIRME AL CONTRATO TIPO DE SERVICIO DE AUTOLAVADO DETALLADO Y SEMI DETALLO  DE VEHICULOS XEHOS AUTOLAVADO DE PROFECO”";

            $contenido["status"] = "OK";
            $contenido["message"] = "OK";
            
        } catch (Exception $e) {
            $contenido["status"] = "ERROR";
            $contenido["message"] = "ERROR";
        }

        echo json_encode($contenido);
    }

    function datos_contrato($id_servicio = "")
    {
        $contrato = NULL;

        $contrato_info = $this->consulta->get_result("id_orden",$id_servicio,"contrato");
        foreach ($contrato_info as $row){
            $contrato["folio"] = $row->folio;
            
            $contrato["fecha_contrato"] = $row->fecha;
            //$fecha = new DateTime($row->fecha.' 00:00:00');
            //$contrato["fecha_contrato_visual"] = $fecha->format('d-m-Y');

            $contrato["hora_contrato"] = $row->hora;
            $contrato["empresa_contrato"] = $row->empresa;
            $contrato["autoriza_contrato"] = $row->delegado;
            $contrato["cantidad_contrato"] = $row->monto;
            $contrato["publicidad_contrato"] = $row->compartir_datos;
            $contrato["envio_publicidad"] = $row->envio_publicidad;

            $contrato["cnombre_asesor"] = $row->nombre_asesor;
            $contrato["cnombre_cliente"] = $row->nombre_cliente;
            $contrato["privacidad_contrato"] = $row->privacidad;
            $contrato["registro_publico"] = $row->registro_publico;
            $contrato["expediente_publico"] = $row->expediente_publico;
            $contrato["registro_publico_dia"] = $row->d_expediente;
            $contrato["registro_publico_mes"] = $row->m_expediente;
            $contrato["registro_publico_anio"] = $row->a_expediente;

            $firma_cliente_c1 = REPO_XEHOS_OP.$row->firma_cliente_publicidad;
            $ccfirma_asesor = REPO_XEHOS_OP.$row->firma_asesor;
            $ccfirma_cliente = REPO_XEHOS_OP.$row->firma_cliente;

            $contrato["firma_cliente_c1"] = (($this->url_exists($firma_cliente_c1)) ? $firma_cliente_c1 : "") ;
            $contrato["ccfirma_asesor"] = (($this->url_exists($ccfirma_asesor)) ? $ccfirma_asesor : "") ;
            $contrato["ccfirma_cliente"] = (($this->url_exists($ccfirma_cliente)) ? $ccfirma_cliente : "") ;
        }

        //Verificamos si se recupero la informacion de aun contrato lleno
        if ($contrato == NULL) {
            // Si no se encontro ningun contrato, se hace un prellenado de informacion
            $datos = $this->consulta->datos_servicio($id_servicio);
            foreach ($datos as $row) {

                $contrato["folio"] = $row->folio_mostrar;
                $contrato["fecha_contrato"] = date("Y-m-d");
                $contrato["hora_contrato"] = date("H:i:s");
                $contrato["empresa_contrato"] = "SISTEMAS OPERATIVOS HEXADECIMAL , S.A. DE C.V.";
                $contrato["autoriza_contrato"] = "XEHOS AUTOLAVADO";
                $contrato["cantidad_contrato"] = $row->total;

                $contrato["publicidad_contrato"] = "1";
                $contrato["envio_publicidad"] = "1";

                $contrato["cnombre_asesor"] = $row->lavadorNombre;
                $contrato["cnombre_cliente"] = $row->nombre." ".$row->apellido_paterno." ".$row->apellido_materno;
                $contrato["privacidad_contrato"] = "1";
                $contrato["registro_publico"] = "XX-XXXX";
                $contrato["expediente_publico"] = "XXX.XX.X.X-XXXXX-XXXX";
                $contrato["registro_publico_dia"] = "01";
                $contrato["registro_publico_mes"] = "Octubre";
                $contrato["registro_publico_anio"] = "2020";

                $contrato["firma_cliente_c1"] = "";
                $contrato["ccfirma_asesor"] = "";
                $contrato["ccfirma_cliente"] = "";
            }
        }

        return $contrato;
    }
// ------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------

    public function redireccionar($id='')
    {
        $registro = $this->encrypt($id);
        echo $registro;
    }

    public function url_exists($url) {
        $context = stream_context_create( [
            'ssl' => [
                'verify_peer' => false,
                'verify_peer_name' => false,
            ],
        ]);
        
        $h = get_headers($url, 0, $context);
        
        $status = array();
        preg_match('/HTTP\/.* ([0-9]+) .*/', $h[0] , $status);
        return ($status[1] == 200);
    }

    function encrypt($data){
        $id = (double)$data*CONST_ENCRYPT;
        $url_id = base64_encode($id);
        $url = str_replace("=", "" ,$url_id);
        return $url;
        //return $data;
    }

    function decrypt($data){
        $url_id = base64_decode($data);
        $id = (double)$url_id/CONST_ENCRYPT;
        return $id;
        //return $data;
    }


  }
