@layout('layout')

@section('included_css')
    <style type="text/css">
        
    </style>
@endsection

@section('contenido')
    <div class="row">
        <div class="col-sm-12 encabezado-info">
            <h5>
                <i class="fa fa-file-image-o "></i>
                Evidencia
            </h5>
        </div>  
        <div class="cuadro-info">
            <div class="row">
                <div class="col-sm-4">
                    <span class="info-titulo">
                        TITULO:
                    </span>
                    <br>
                    <span class="info-cuerpo">
                        <?= ((isset($evidencia_titulo) ? $evidencia_titulo : "")) ?>
                    </span>
                </div>
                <div class="col-sm-8">
                    <span class="info-titulo">
                        COMENTARIOS:
                    </span>
                    <br>
                    <span class="info-cuerpo">
                        <?= ((isset($evidencia_comentarios) ? $evidencia_comentarios : "")) ?>
                    </span>
                </div>
            </div>

            <br>
            <div class="row">
                <div class="col-sm-4">
                    <span class="info-titulo">
                        FECHA REGISTRO:
                    </span>
                    <br>
                    <span class="info-cuerpo">
                        <?php  
                            if (isset($evidencia_fecha_alta)) {
                                $fecha = new DateTime($evidencia_fecha_alta);
                                echo $fecha->format('d-m-Y H:i');
                            }
                        ?>
                    </span>
                </div>
                <div class="col-sm-4">
                    <span class="info-titulo">
                        TIPO DE EVIDENCIA:
                    </span>
                    <br>
                    <span class="info-cuerpo">
                        <?php if (isset($extension)): ?>
                            <?php if (in_array($extension, $array_imagen) ): ?>
                                IMAGEN
                            <?php elseif (in_array($extension, $array_doc) ): ?>
                                DOCUMENTO
                            <?php elseif (in_array($extension, $array_audio) ): ?>
                                AUDIO
                            <?php elseif (in_array($extension, $array_video) ): ?>
                                VIDEO
                            <?php else: ?>
                                SIN IDENTIFICAR
                            <?php endif ?>
                        <?php endif ?>
                    </span>
                </div>
            </div>

            <br>
            <div class="row">
                <div class="col-sm-12 table-responsive" style="width: 100%;">
                    <?php if (isset($evidencia_url)): ?>
                        <div class="caja">
                            <div class="box">
                                <?php if (in_array($extension, $array_imagen) ): ?>
                                    <img src="<?= base_url().$evidencia_url ?>" alt="Cargando imagen..." style="margin-left: -8px;">
                                <?php elseif (in_array($extension, $array_doc) ): ?>
                                    <iframe class="embed-responsive-item" src="<?= base_url().$evidencia_url ?>" frameborder="0" style="height:100%;width:100%;margin-left: -8px;"></iframe>
                                <?php elseif (in_array($extension, $array_audio) ): ?>
                                    <audio src="<?= base_url().$evidencia_url ?>" controls>
                                        <p>Tu navegador no implementa el elemento audio.</p>
                                    </audio>
                                <?php elseif (in_array($extension, $array_video) ): ?>
                                    <video src="<?= base_url().$evidencia_url ?>" controls>
                                        Tu navegador no implementa el elemento <code>video</code>.
                                    </video>
                                <?php else: ?>
                                    <iframe class="embed-responsive-item" src="<?= base_url().$evidencia_url ?>" frameborder="0" style="height:100%;width:100%;margin-left: -8px;"></iframe>
                                <?php endif ?>
                            </div>
                        </div>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" id="paginado" value="<?= ((isset($servicioId) ? $servicioId : "0")) ?>">
    <?php if (isset($paso)): ?>
        <input type="hidden" name="paso_evidencia" value="<?= ((($paso->termina_servicio == '1')||($paso->servicio_cancelado == '1')) ? '6' : '1') ?>">
    <?php endif ?>
@endsection
@section('included_js')

@endsection