@layout('layout')

@section('included_css')

@endsection

@section('contenido')
<div class="container-fluid panel-body">
	<h1 class="mt-4 encabezado-info">
		<i class="fa fa-address-book-o"></i>
		Panel Chat
	</h1>

	@if(isset($datos) && $datos['lavadorTelefono'])

	<div class="row">
		<div class="col-md-3">
			<h2>Mis datos</h2>
			{{ $datos['lavadorNombre']}} <br />
			{{ $datos['lavadorTelefono'] }} <br />
			{{ $datos['lavadorEmail']}}
			<input type="hidden" value="{{ $datos['lavadorTelefono']}}" id="telefono_remitente" />
		</div>
		<div class="col-md-9">
			<ul class="nav nav-tabs" role="tablist">
				<li class="nav-item">
					<a class="nav-link active show" data-toggle="tab" href="#operadores">Operadores</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" data-toggle="tab" href="#clientes">Clientes</a>
				</li>
			</ul>
			<div class="tab-content">
				<div id="operadores" class="container tab-pane active"><br>
					<h3>Listado de Operadores</h3>
					<div class="row">
						<?php
						if (isset($operadores) && $operadores) {
							foreach ($operadores as $operador) {  ?>
								<div class="col-md-4 mt-3">
									<div class="panel panel-filled " style="border:2px solid #ddd; border-radius:8px; background-color:#f9f9f9; min-height:200px;">
										<div style="padding:10px" align="center">
											<i class="fa fa-user-circle fa-2x" style="color:darkblue;"></i>
											<h5>
												{{ ucwords(strtolower($operador->adminNombre)) }}
											</h5>
											<p>
												<b> {{ $operador->cargo }}</b>
											</p>
											<?php if ($operador->notificacion == true) { ?>
												<span class="text-info text-right bold"><b>Chat pendiente</b></span>
											<?php } ?>
											<div class="clearfix"></div>
											<button class="btn btn-primary btn-large col-12 mt-2" title="Comenzar chat" onclick="gotoChat(this)" data-telefono="{{ $operador->telefono }}">
												<span style="font-size:16px">Iniciar</span>
											</button>
										</div>
									</div>
								</div>
						<?php
							}
						} ?>
					</div>
				</div>
				<div id="clientes" class="container tab-pane"><br>
					<h3>Listado de Clientes</h3>
					<div class="row">
						<?php
						if (isset($clientes) && $clientes) {
							foreach ($clientes as $cliente) {  ?>
								<div class="col-md-4 mt-3">
									<div class="panel panel-filled " style="border:1px solid #ccc; border-radius:8px; background-color:#f9f9f9; min-height:200px;">
										<div style="padding:10px" align="center">
											<i class="fa fa-user-circle fa-2x" style="color:darkblue;"></i>
											<h5>
												{{ ucwords(strtolower($cliente->nombre)) }}
											</h5>
											<?php if ($cliente->notificacion == true) { ?>
												<span class="text-info text-right bold"><b>Chat pendiente</b></span>
											<?php } ?>
											<button class="btn btn-primary btn-large col-12 mt-2" title="Comenzar chat" onclick="gotoChat(this)" data-telefono="{{ $cliente->telefono }}">
												<span style="font-size:16px">Iniciar</span>
											</button>
										</div>
									</div>
								</div>
						<?php
							}
						} else {
							echo '<h4 class="mt-5 mb-5 text-center">Sin clientes asignados para el servicio</h4>';
						} ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	@else
	<h3 class="mt-5 mb-5 text-center">El lavador no esta registrado en el sistema</h3>
	@endif
</div>

<input type="hidden" id="paginado" value="<?= ((isset($servicioId) ? $servicioId : "0")) ?>">
@endsection

@endsection
@section('included_js')
<script type="text/javascript">
	var site_url = "{{site_url()}}";

	function gotoChat(_this) {
		window.location.href = site_url + '/operadores/ChatLavador/chat?telefono_remitente=' + document.getElementById("telefono_remitente").value + '&telefono_destinatario=' + $(_this).data('telefono')
	}
</script>
@endsection