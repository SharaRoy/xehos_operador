@layout('layout')

@section('included_css')

@endsection

@section('contenido')
    <div class="row" style="padding-left: 10px;padding-right: 10px;">
        <div class="col-sm-12 encabezado-info">
            <h5>
                <i class="fa fa-book"></i>
                Agendar Cita
            </h5>
        </div>  
        <div class="cuadro-info">
            <div class="row">
                <div class="col-sm-12" align="center">
                    <iframe class="embed-responsive-item" src="https://hexadms.com/xehos/" frameborder="0" style="height:11cm;width:100%;"></iframe>
                </div>
            </div>
            <br>
        </div>
    </div>

    <br><br>
    <input type="hidden" id="paginado" value="<?= ((isset($servicioId) ? $servicioId : "0")) ?>">
@endsection
@section('included_js')
    
@endsection