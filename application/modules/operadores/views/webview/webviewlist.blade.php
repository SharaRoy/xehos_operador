@layout('layout')

@section('included_css')

@endsection

@section('contenido')
    <div class="row" style="padding-left: 15px;padding-right: 15px;"> 
        <div class="col-sm-12 encabezado-info">
            <h5>
                <i class="fa fa-list"></i>
                Menu
            </h5>
        </div>  
        <div class="cuadro-info table-responsive" style="width: 100%;">
            <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-10" align="center">
                    <a href="<?= base_url().'inventario_lavadores/inventario/alta' ?>" class="btn btn-webview" style="background-color:  #45b39d ;">
                        Inventario Unidad Lavador
                    </a>
                </div>
                <div class="col-sm-1"></div>
            </div>

            <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-10" align="center">
                    <a href="<?= base_url() . 'index.php/operadores/agendar_cita' ?>" class="btn btn-webview" style="background-color:  #45b39d ;">
                        Agendar Cita
                    </a>
                </div>
                <div class="col-sm-1"></div>
            </div>

            <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-10" align="center">
                    <a href="<?= base_url() . 'index.php/operadores/proactivo_agencias' ?>" class="btn btn-webview"  style="background-color:  #45b39d ;">
                        Agendar citas agencias
                    </a>
                </div>
                <div class="col-sm-1"></div>
            </div>

            <!--<div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-10" align="center">
                    <a onclick="event.preventDefault();" class="btn btn-info btn-webview">
                        Seguro de Lluvia
                    </a>
                </div>
                <div class="col-sm-1"></div>
            </div>-->

            <!--<div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-10" align="center">
                    <a onclick="event.preventDefault();" class="btn btn-info btn-webview">
                        E-commerce
                    </a>
                </div>
                <div class="col-sm-1"></div>
            </div>-->

            <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-10" align="center">
                    <a href="<?= base_url() . 'index.php/operadores/combustible' ?>" class="btn btn-webview"  style="background-color:  #45b39d ;">
                        Combustible
                    </a>
                </div>
                <div class="col-sm-1"></div>
            </div>

            <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-10" align="center">
                    <!--<a href="<?= base_url() . 'index.php/operadores/chat' ?>" class="btn btn-info btn-webview">-->
                    <a href="<?= base_url() . 'index.php/operadores/ChatLavador/index' ?>" class="btn btn-webview"  style="background-color:  #45b39d ;">
                        Chat
                    </a>
                </div>
                <div class="col-sm-1"></div>
            </div>

            <br>
        </div>
    </div>

    <br><br>

    <input type="hidden" id="paginado" value="<?= ((isset($servicioId) ? $servicioId : "0")) ?>">
@endsection
@section('included_js')
    <script>
        function revisar_datos(indice) {
            var base = $("#sitio").val();
            var url_sis = base+"operadores/redireccionar/"+indice;
            $.ajax({
                url: url_sis,
                method: 'get',
                data: {
                    
                },
                success:function(resp){
                  //console.log(resp);
                    if (resp.indexOf("handler           </p>")<1) {
                        location.href = "<?php echo base_url(); ?>"+"index.php/operadores/ver_servicio/"+resp;
                    }
                //Cierre de success
                },
                error:function(error){
                    console.log(error);
                //Cierre del error
                }
            //Cierre del ajax
            });
        }
    </script>
@endsection