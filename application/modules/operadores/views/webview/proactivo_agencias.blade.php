@layout('layout')

@section('included_css')

@endsection

@section('contenido')
    <div class="row" style="padding-left: 10px;padding-right: 10px;">
        <div class="col-sm-12 encabezado-info">
            <h5>
                <i class="fa fa-pencil-square-o"></i> 
                Agendar citas agencias
            </h5>
        </div>  
        <div class="cuadro-info">
            <div class="row">
                <div class="col-sm-6">
                    <h5>Lista de agencias</h5>
                    <select id="tt_agencia" class="form-control" style="width: 85%;">
                        <option value="">Seleccionar</option>
                        @if ($agencias != NULL)
                            @foreach ($agencias as $i => $reg)
                                <option value="{{$reg->url}}">{{$reg->agencia}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>

            <br>
            <div class="row">
                <div class="col-sm-12" id="div-iframe" style="display:none;">
                    <iframe class="embed-responsive-item" id="agencia-interna" src="" frameborder="0" style="height:11cm;width:95%;"></iframe>
                </div>
            </div>
            <br>
        </div>
    </div>

    <br><br>

    <input type="hidden" id="paginado" value="<?= ((isset($servicioId) ? $servicioId : "0")) ?>">
@endsection
@section('included_js')
    <script type="text/javascript">
        $('#tt_agencia').on('change', function() {
            var url = $('#tt_agencia').val();
            var cita = "index.php/xehos/agendar_cita";
            if (url != "") {
                $("#agencia-interna").attr("src",url+"/"+cita);
                $("#div-iframe").css("display","inline-block");
            } else {
                $("#div-iframe").css("display","none");
            }
        });
    </script>

@endsection