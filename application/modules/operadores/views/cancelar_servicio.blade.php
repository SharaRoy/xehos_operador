@layout('layout')

@section('included_css')
    <style type="text/css">
        #cargaIcono {
            /*-webkit-animation: rotation 1s infinite linear;*/
            font-size: 55px !important;
            color: darkblue;
            display: none;
        }
    </style>
@endsection

@section('contenido')
    <form id="form" method="post">
        <div class="row">
            <div class="col-sm-12 encabezado-info" style="background-color: darkred !important;">
                <h5>
                    <i class="fa fa-times"></i>
                    Cancelar Servicio
                </h5>
            </div>  
            <div class="cuadro-info">
                <div class="row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-6" align="center">
                        <span class="info-titulo">
                            Motivos de la cancelación:
                        </span>
                        <br>
                        <textarea class="form-control" name="motivos" rows="2"></textarea>
                        <div id="motivos_error"></div>
                    </div>
                    <div class="col-sm-3" align="right"></div>
                </div>

                <br>
                <div class="row">
                    <div class="col-sm-4"></div>
                    <div class="col-sm-4" align="center">
                        <br>
                        <i id="cargaIcono" class="fa fa-spinner cargaIcono fa-spin"></i>
                        <h5 class="error" id="formulario_error"></h5>

                        <a class="btn btn-success" id="envio_form" style="color: white;">
                            <i class="fa fa-save"></i>
                            Cancelar Servicio
                        </a>
                        <input type="hidden" name="servicio" value="<?= ((isset($servicioId) ? $servicioId : "0")) ?>">
                    </div>
                    <div class="col-sm-4 table-responsive" align="center"></div>
                </div>               
                <br>
            </div>
        </div>
    </form>

    <br>
    <div class="row">
        <div class="col-sm-12 encabezado-info">
            <h5>
                <i class="fa fa-wrench"></i>
                Información del servicio
            </h5>
        </div>  
        <div class="cuadro-info">
            <div class="row">
                <div class="col-sm-6">
                    <span class="info-titulo">
                        Servicio:
                    </span>
                    <br>
                    <span class="info-cuerpo">
                        <?= ((isset($servicio->servicioNombre) ? $servicio->servicioNombre : "")) ?>
                    </span>
                </div>
                <div class="col-sm-3">
                    <span class="info-titulo">
                        Método de pago:
                    </span>
                    <br>
                    <span class="info-cuerpo">
                        <?= ((isset($servicio->metodo_pago) ? $servicio->metodo_pago : "")) ?>
                    </span>
                </div>
                <div class="col-sm-3">
                    <span class="info-titulo">
                        Folio:
                    </span>
                    <br>
                    <span class="info-cuerpo">
                        <?= ((isset($servicio->folio_mostrar) ? $servicio->folio_mostrar : "")) ?>
                    </span>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <span class="info-titulo">
                        Fecha programada:
                    </span>
                    <br>
                    <span class="info-cuerpo">
                        @if(isset($servicio->fecha))
                            <?php 
                                $fecha = new DateTime($servicio->fecha);
                                echo $fecha->format('d-m-Y');
                            ?>
                        @endif
                    </span>
                </div>
                <div class="col-sm-4">
                    <span class="info-titulo">
                        Hora programada:
                    </span>
                    <br>
                    <span class="info-cuerpo">
                        <?= ((isset($servicio->hora) ? $servicio->hora : "")) ?>
                    </span>
                </div>
                <div class="col-sm-4">
                    <span class="info-titulo">
                        Estatus del servicio:
                    </span>
                    <br>
                    <span class="info-cuerpo">
                        <?= ((isset($servicio->estatus) ? $servicio->estatus : "")) ?>
                    </span>
                </div>
            </div>
        </div>
    </div>

    <br>
    <div class="row">
        <div class="col-sm-12 encabezado-info">
            <h5>
                <i class="fa fa-user-circle"></i>
                Información del cliente
            </h5>
        </div>  
        <div class="cuadro-info">
            <div class="row">
                <div class="col-sm-4">
                    <span class="info-titulo">
                        Nombre:
                    </span>
                    <br>
                    <span class="info-cuerpo">
                        <?= ((isset($servicio->nombre) ? $servicio->nombre : "")) ?>
                    </span>
                </div>
                <div class="col-sm-4">
                    <span class="info-titulo">
                        Apellido Paterno:
                    </span>
                    <br>
                    <span class="info-cuerpo">
                        <?= ((isset($servicio->apellido_paterno) ? $servicio->apellido_paterno : "")) ?>
                    </span>
                </div>
                <div class="col-sm-4">
                    <span class="info-titulo">
                        Apellido Materno:
                    </span>
                    <br>
                    <span class="info-cuerpo">
                        <?= ((isset($servicio->apellido_materno) ? $servicio->apellido_materno : "")) ?>
                    </span>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <span class="info-titulo">
                        Domicilio:
                    </span>
                    <br>
                    <span class="info-cuerpo">
                        @if(isset($servicio->calle))
                            <?php 
                                echo $servicio->calle. " int. " . $servicio->numero_int . " ext. " . $servicio->numero_ext . " Col. " . $servicio->colonia . " " . $servicio->municipio . " " . $servicio->estado; 
                            ?>
                        @endif
                    </span>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-5">
                    <span class="info-titulo">
                        Correo electrónico:
                    </span>
                    <br>
                    <span class="info-cuerpo">
                        <?= ((isset($servicio->email) ? $servicio->email : "")) ?>
                    </span>
                </div>
                <div class="col-sm-3">
                    <span class="info-titulo">
                        Teléfono:
                    </span>
                    <br>
                    <span class="info-cuerpo">
                        <?= ((isset($servicio->telefono) ? $servicio->telefono : "")) ?>
                    </span>
                </div>
                <div class="col-sm-4">
                    <span class="info-titulo">
                        Servicio:
                    </span>
                    <br>
                    <span class="info-cuerpo">
                        <?= ((isset($servicio->servicioNombre) ? $servicio->servicioNombre : "")) ?>
                    </span>
                </div>
            </div>
        </div>
    </div>

    <br>
    <div class="row">
        <div class="col-sm-12 encabezado-info">
            <h5>
                <i class="fa fa-car"></i>
                Información del vehículo
            </h5>
        </div>  
        <div class="cuadro-info">
            <div class="row">
                <div class="col-sm-4">
                    <span class="info-titulo">
                        Color:
                    </span>
                    <br>
                    <span class="info-cuerpo">
                        <?= ((isset($servicio->color) ? $servicio->color : "")) ?>
                    </span>
                </div>
                <div class="col-sm-4">
                    <span class="info-titulo">
                        Marca:
                    </span>
                    <br>
                    <span class="info-cuerpo">
                        <?= ((isset($servicio->marca) ? $servicio->marca : "")) ?>
                    </span>
                </div>
                <div class="col-sm-4">
                    <span class="info-titulo">
                        Modelo:
                    </span>
                    <br>
                    <span class="info-cuerpo">
                        <?= ((isset($servicio->modelo) ? $servicio->modelo : "")) ?>
                    </span>
                </div>
            </div>

            <div class="row">
                 <div class="col-sm-4">
                    <span class="info-titulo">
                        Tipo de vehículo:
                    </span>
                    <br>
                    <span class="info-cuerpo">
                        <?= ((isset($servicio->tipo_auto) ? $servicio->tipo_auto : "")) ?>
                    </span>
                </div>
                 <div class="col-sm-4">
                    <span class="info-titulo">
                        Placas:
                    </span>
                    <br>
                    <span class="info-cuerpo">
                        <?= ((isset($servicio->placas) ? strtoupper($servicio->placas) : "")) ?>
                    </span>
                </div>
                <div class="col-sm-4">
                    <span class="info-titulo">
                        Serie:
                    </span>
                    <br>
                    <span class="info-cuerpo">
                        <?= ((isset($servicio->numero_serie) ? strtoupper($servicio->numero_serie) : "")) ?>
                    </span>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" id="paginado" value="<?= ((isset($servicioId) ? $servicioId : "0")) ?>">
    <?php if (isset($paso)): ?>
        <input type="hidden" name="paso_evidencia" value="<?= ((($paso->termina_servicio == '1')||($paso->servicio_cancelado == '1')) ? '6' : '1') ?>">
    <?php endif ?>
    <br>
@endsection
@section('included_js')
    <script>
        $("#file-upload").change(function(){
            var archivos = this.files.length;
            $(".subir").text("Archivos seleccionados ("+archivos+")");

            if (this.files.length > 0) {
                $("#archivos").val(1);
            }else{
                $("#archivos").val(0);
            }
            /*console.log(this.files);
            for (var i = 0; i < archivos; i++) {
                console.log(this.files[i]["name"]);
            }*/
         });
    </script>

    <script type="text/javascript">
        $("#envio_form").on('click', function (e){
            // Evitamos que salte el enlace.
            e.preventDefault(); 
            var validaciones = validar();
            var evidencia = $("input[name='paso_evidencia']").val();
            if(validaciones){
                var base = $("#sitio").val();
                var paqueteDatos = new FormData(document.getElementById('form'));
                $.ajax({
                    url: base+"operadores/act_cancelar_servivio",
                    type: 'post',
                    contentType: false,
                    data: paqueteDatos,
                    processData: false,
                    cache: false,
                    success:function(resp){
                        console.log(resp);
                        if (resp.indexOf("handler         </p>")<1) {
                            if (resp == "OK") {
                                location.href = base+"index.php/operadores/index";
                                //$("#llenar_inventario").css("display","inline-block");
                                //location.reload();
                            } else {
                                $("#formulario_error").text(resp);
                            }

                        }else{
                            $("#formulario_error").text(resp);
                        }
                    //Cierre de success
                    },
                      error:function(error){
                        console.log(error);
                    }
                });
            }else{

            }
        });

        function validar() {
            var retorno = true;
            var campo_a = $("#motivos").val();
            if (campo_a == "") {
                var error_a = $("#motivos_error");
                error_a.empty();
                //$("#motivos_error").css("display","inline-block");
                error_a.append('<label class="form-text text-danger">Campo requerido</label>');
                retorno = false;
            }else{
                var error_a = $("#motivos_error");
                error_a.empty();
            }

            return retorno;
        }
    </script>
@endsection