@layout('layout')

@section('included_css')

@endsection

@section('contenido')
    <div class="row">
        <div class="col-sm-12 encabezado-info">
            <h5>
                <i class="fa fa-file-image-o "></i>
                Mis evidencias
            </h5>
        </div>  
        <div class="cuadro-info">
            <br>
            <div class="animated fadeIn" style="width: 95%;">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="table-responsive" style="width: 100%;">
                                <table <?php if ($evidencias != NULL) echo 'id="bootstrap-data-table"'; ?> class="table table-striped table-bordered" >
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Titulo</th>
                                            <th>Comentario</th>
                                            <th>Fecha Registro</th>
                                            <th>Ver</th>
                                        </tr>
                                    </thead>
                                    <tbody id="cuerpo">
                                        @if ($evidencias != NULL)
                                            @foreach ($evidencias as $i => $registro)
                                                <tr>
                                                    <td style="vertical-align: middle;">
                                                        <?= $i+1 ?>
                                                    </td>
                                                    <td style="vertical-align: middle;">
                                                        <?= $registro->titulo ?>
                                                    </td>
                                                    <td style="vertical-align: middle;" align="center">
                                                        <?= $registro->comentarios ?>
                                                    </td>
                                                    <td style="vertical-align: middle;">
                                                        <?php 
                                                            $fecha = new DateTime($registro->fecha_alta);
                                                            echo $fecha->format('d-m-Y H:i');
                                                        ?>
                                                    </td>                                            
                                                    <td style="vertical-align: middle;" align="center">
                                                        <a onclick="revisar_datos(<?= $registro->id ?>)" class="btn btn-info" style="color:white;font-size: 10px;" >
                                                            Ver
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="5" align="center">
                                                    Sin evidencias cargadas
                                                </td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- .animated -->
        </div>
    </div>

    <input type="hidden" id="paginado" value="<?= ((isset($servicioId) ? $servicioId : "0")) ?>">

    <?php if (isset($paso)): ?>
        <input type="hidden" name="paso_evidencia" value="<?= ((($paso->termina_servicio == '1')||($paso->servicio_cancelado == '1')) ? '6' : '1') ?>">
    <?php endif ?>      
@endsection
@section('included_js')
    @include('main/scripts_dt')
    <script>
        function revisar_datos(indice) {
            var base = $("#sitio").val();
            var url_sis = base+"operadores/redireccionar/"+indice;
            $.ajax({
                url: url_sis,
                method: 'get',
                data: {
                    
                },
                success:function(resp){
                  //console.log(resp);
                    if (resp.indexOf("handler           </p>")<1) {
                        location.href = "<?php echo base_url(); ?>"+"index.php/operadores/ver_evidencia/"+resp;
                    }
                //Cierre de success
                },
                error:function(error){
                    console.log(error);
                //Cierre del error
                }
            //Cierre del ajax
            });
        }
    </script>
@endsection