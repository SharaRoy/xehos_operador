@layout('layout')

@section('included_css')
    <style type="text/css">
        #cargaIcono {
            /*-webkit-animation: rotation 1s infinite linear;*/
            font-size: 55px !important;
            color: darkblue;
            display: none;
        }
    </style>
@endsection

@section('contenido')
    <div class="row">
        <div class="col-sm-12 encabezado-info">
            <h5>
                <i class="fa fa-user-circle"></i>
                Información del cliente
            </h5>
        </div>  
        <div class="cuadro-info">
            <div class="row">
                <div class="col-sm-4">
                    <span class="info-titulo">
                        Nombre:
                    </span>
                    <br>
                    <span class="info-cuerpo">
                        <?= ((isset($servicio[0]->nombre) ? $servicio[0]->nombre : "")) ?>
                    </span>
                </div>
                <div class="col-sm-4">
                    <span class="info-titulo">
                        Apellido Paterno:
                    </span>
                    <br>
                    <span class="info-cuerpo">
                        <?= ((isset($servicio[0]->apellido_paterno) ? $servicio[0]->apellido_paterno : "")) ?>
                    </span>
                </div>
                <div class="col-sm-4">
                    <span class="info-titulo">
                        Apellido Materno:
                    </span>
                    <br>
                    <span class="info-cuerpo">
                        <?= ((isset($servicio[0]->apellido_materno) ? $servicio[0]->apellido_materno : "")) ?>
                    </span>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <span class="info-titulo">
                        Domicilio:
                    </span>
                    <br>
                    <span class="info-cuerpo">
                        @if(isset($servicio[0]->calle))
                            <?php 
                                echo $servicio[0]->calle. " int. " . $servicio[0]->numero_int . " ext. " . $servicio[0]->numero_ext . " Col. " . $servicio[0]->colonia . " " . $servicio[0]->municipio . " " . $servicio[0]->estado; 
                            ?>
                        @endif
                    </span>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-5">
                    <span class="info-titulo">
                        Correo electrónico:
                    </span>
                    <br>
                    <span class="info-cuerpo">
                        <?= ((isset($servicio[0]->email) ? $servicio[0]->email : "")) ?>
                    </span>
                </div>
                <div class="col-sm-3">
                    <span class="info-titulo">
                        Teléfono:
                    </span>
                    <br>
                    <span class="info-cuerpo">
                        <?= ((isset($servicio[0]->telefono) ? $servicio[0]->telefono : "")) ?>
                    </span>
                </div>
                <div class="col-sm-4">
                    <span class="info-titulo">
                        Servicio:
                    </span>
                    <br>
                    <span class="info-cuerpo">
                        <?= ((isset($servicio[0]->servicioNombre) ? $servicio[0]->servicioNombre : "")) ?>
                    </span>
                </div>
            </div>
        </div>
    </div>

    <br>
    <div class="row">
        <div class="col-sm-12 encabezado-info">
            <h5>
                <i class="fa fa-car"></i>
                Información del vehículo
            </h5>
        </div>  
        <div class="cuadro-info">
            <div class="row">
                <div class="col-sm-4">
                    <span class="info-titulo">
                        Color:
                    </span>
                    <br>
                    <span class="info-cuerpo">
                        <?= ((isset($servicio[0]->color) ? $servicio[0]->color : "")) ?>
                    </span>
                </div>
                <div class="col-sm-4">
                    <span class="info-titulo">
                        Marca:
                    </span>
                    <br>
                    <span class="info-cuerpo">
                        <?= ((isset($servicio[0]->marca) ? $servicio[0]->marca : "")) ?>
                    </span>
                </div>
                <div class="col-sm-4">
                    <span class="info-titulo">
                        Modelo:
                    </span>
                    <br>
                    <span class="info-cuerpo">
                        <?= ((isset($servicio[0]->modelo) ? $servicio[0]->modelo : "")) ?>
                    </span>
                </div>
            </div>

            <div class="row">
                 <div class="col-sm-4">
                    <span class="info-titulo">
                        Tipo de vehículo:
                    </span>
                    <br>
                    <span class="info-cuerpo">
                        <?= ((isset($servicio[0]->tipo_auto) ? $servicio[0]->tipo_auto : "")) ?>
                    </span>
                </div>
                 <div class="col-sm-4">
                    <span class="info-titulo">
                        Placas:
                    </span>
                    <br>
                    <span class="info-cuerpo">
                        <?= ((isset($servicio[0]->placas) ? strtoupper($servicio[0]->placas) : "")) ?>
                    </span>
                </div>
                <div class="col-sm-4">
                    <span class="info-titulo">
                        Serie:
                    </span>
                    <br>
                    <span class="info-cuerpo">
                        <?= ((isset($servicio[0]->numero_serie) ? strtoupper($servicio[0]->numero_serie) : "")) ?>
                    </span>
                </div>
            </div>
        </div>
    </div>

    <br>
    <div class="row">
        <div class="col-sm-12 encabezado-info">
            <h5>
                <i class="fa fa-check-square-o"></i>
                Pasos Servicio
            </h5>
        </div>  
        <div class="cuadro-info" align="center">
            <div class="row">
                <div class="col-sm-6" align="center">
                    <table style="width: 100%;margin: 10px;">
                        <tr>
                            <td style="width: 30%;" align="center">
                                <?php if (isset($paso->inicia_ruta)): ?>
                                    <?php if ($paso->inicia_ruta == "1"): ?>
                                        <img src="<?= base_url().'statics/inventario/imgs/check2.png'; ?>" style="width:20px;" >
                                    <?php else: ?>
                                        <img src="<?= base_url().'statics/inventario/imgs/check_ng.png'; ?>" style="width:20px;" >
                                    <?php endif ?>
                                <?php else: ?>
                                    <img src="<?= base_url().'statics/inventario/imgs/check_ng.png'; ?>" style="width:20px;" >
                                <?php endif ?>
                            </td>
                            <td style="width: 70%;font-weight: bold;">
                                1.- RUTA INICIADA
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 30%;" align="center">
                                <?php if (isset($paso->termina_ruta)): ?>
                                    <?php if ($paso->termina_ruta == "1"): ?>
                                        <img src="<?= base_url().'statics/inventario/imgs/check2.png'; ?>" style="width:20px;" >
                                    <?php else: ?>
                                        <img src="<?= base_url().'statics/inventario/imgs/check_ng.png'; ?>" style="width:20px;" >
                                    <?php endif ?>
                                <?php else: ?>
                                    <img src="<?= base_url().'statics/inventario/imgs/check_ng.png'; ?>" style="width:20px;" >
                                <?php endif ?>
                            </td>
                            <td style="width: 70%;font-weight: bold;">
                                2.- RUTA TERMINADA
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 30%;" align="center">
                                <?php if (isset($paso->evidencia_pago)): ?>
                                    <?php if ($paso->evidencia_pago == "1"): ?>
                                        <img src="<?= base_url().'statics/inventario/imgs/check2.png'; ?>" style="width:20px;" >
                                    <?php else: ?>
                                        <img src="<?= base_url().'statics/inventario/imgs/check_ng.png'; ?>" style="width:20px;" >
                                    <?php endif ?>
                                <?php else: ?>
                                    <img src="<?= base_url().'statics/inventario/imgs/check_ng.png'; ?>" style="width:20px;" >
                                <?php endif ?>
                            </td>
                            <td style="width: 70%;font-weight: bold;">
                                3.- COMPROBANTE DE PAGO
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 30%;" align="center">
                                <?php if (isset($paso->evidencia_inicio)): ?>
                                    <?php if ($paso->evidencia_inicio == "1"): ?>
                                        <img src="<?= base_url().'statics/inventario/imgs/check2.png'; ?>" style="width:20px;" >
                                    <?php else: ?>
                                        <img src="<?= base_url().'statics/inventario/imgs/check_ng.png'; ?>" style="width:20px;" >
                                    <?php endif ?>
                                <?php else: ?>
                                    <img src="<?= base_url().'statics/inventario/imgs/check_ng.png'; ?>" style="width:20px;" >
                                <?php endif ?>
                            </td>
                            <td style="width: 70%;font-weight: bold;">
                                4.- EVIDENCIA (COMO SE RECIBE LA UNIDAD)
                            </td>
                        </tr>
                        
                    </table>
                </div>
                <div class="col-sm-6" align="center">
                    <table style="width: 100%;margin: 10px;">
                        <tr>
                            <td style="width: 30%;" align="center">
                                <?php if (isset($paso->inventario)): ?>
                                    <?php if ($paso->inventario == "1"): ?>
                                        <img src="<?= base_url().'statics/inventario/imgs/check2.png'; ?>" style="width:20px;" >
                                    <?php else: ?>
                                        <img src="<?= base_url().'statics/inventario/imgs/check_ng.png'; ?>" style="width:20px;" >
                                    <?php endif ?>
                                <?php else: ?>
                                    <img src="<?= base_url().'statics/inventario/imgs/check_ng.png'; ?>" style="width:20px;" >
                                <?php endif ?>
                            </td>
                            <td style="width: 70%;font-weight: bold;">
                                5.- INVENTARIO
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 30%;" align="center">
                                <?php if (isset($paso->evidencia_lavado)): ?>
                                    <?php if ($paso->evidencia_lavado == "1"): ?>
                                        <img src="<?= base_url().'statics/inventario/imgs/check2.png'; ?>" style="width:20px;" >
                                    <?php else: ?>
                                        <img src="<?= base_url().'statics/inventario/imgs/check_ng.png'; ?>" style="width:20px;" >
                                    <?php endif ?>
                                <?php else: ?>
                                    <img src="<?= base_url().'statics/inventario/imgs/check_ng.png'; ?>" style="width:20px;" >
                                <?php endif ?>
                            </td>
                            <td style="width: 70%;font-weight: bold;">
                                6.- EVIDENCIA (PROCESO DE LAVADO)
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 30%;" align="center">
                                <?php if (isset($paso->evidencia_finaliza)): ?>
                                    <?php if ($paso->evidencia_finaliza == "1"): ?>
                                        <img src="<?= base_url().'statics/inventario/imgs/check2.png'; ?>" style="width:20px;" >
                                    <?php else: ?>
                                        <img src="<?= base_url().'statics/inventario/imgs/check_ng.png'; ?>" style="width:20px;" >
                                    <?php endif ?>
                                <?php else: ?>
                                    <img src="<?= base_url().'statics/inventario/imgs/check_ng.png'; ?>" style="width:20px;" >
                                <?php endif ?>
                            </td>
                            <td style="width: 70%;font-weight: bold;">
                                7.- EVIDENCIA (LAVADO TERMINADO: UNIDAD PARA ENTREGA)
                            </td>
                        </tr>
                        
                        <tr>
                            <td style="width: 30%;" align="center">
                                <?php if (isset($paso->termina_servicio)): ?>
                                    <?php if ($paso->termina_servicio == "1"): ?>
                                        <img src="<?= base_url().'statics/inventario/imgs/check2.png'; ?>" style="width:20px;" >
                                    <?php else: ?>
                                        <img src="<?= base_url().'statics/inventario/imgs/check_ng.png'; ?>" style="width:20px;" >
                                    <?php endif ?>
                                <?php else: ?>
                                    <img src="<?= base_url().'statics/inventario/imgs/check_ng.png'; ?>" style="width:20px;" >
                                <?php endif ?>
                            </td>
                            <td style="width: 70%;font-weight: bold;">
                                8.- SERVICIO TERMINADO
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

            <br>
            <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-10" style="background-color: <?= ((isset($servicio[0]) ? (($servicio[0]->pago == '1') ? "aliceblue" : "antiquewhite") : "white")) ?>;width: 90%;" align="center"> 
                    <br>
                    <h6 style="color:darkblue;">
                        Tipo de Pago:
                        &nbsp;&nbsp;
                        <span>
                            <u>
                                <?= ((isset($servicio[0]->metodo_pago) ? $servicio[0]->metodo_pago : "No definido")) ?>
                            </u>
                        </span>

                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <span style="color: <?= ((isset($servicio[0]) ? (($servicio[0]->pago == '1') ? "darkgreen" : "red") : "darkblue")) ?>; font-weight: bold;">
                            (<?= ((isset($servicio[0]) ? (($servicio[0]->pago == '1') ? "Pagado" : "Pendiente") : "")) ?>)
                        </span>
                    </h6>
                    <br>
                </div>
            </div>
        </div>
    </div>

    <br>
    <form id="form-evidencia" method="post">
        <div class="row">
            <div class="col-sm-12 encabezado-info">
                <h5>
                    <i class="fa fa-camera-retro"></i>
                    <?= ((isset($evidencia)) ? $evidencia : "" ) ?>
                </h5>
            </div>  
            <div class="cuadro-info table-responsive" style="width: 100%;padding-left: 0px!important;">
                <br>
                <!-- Si el servicio ya esta terminado -->
                <?php if ($paso_evidencia == "6"): ?>
                    <div class="row">
                        <div class="col-sm-3"></div>
                        <div class="col-sm-6" align="center">
                            <a onclick="revisar_datos_2(<?= ((isset($servicioId) ? $servicioId : "0")) ?>)" class="btn btn-dark btn-vista" style="color:white;">
                                <i class="fa fa-file-image-o "></i>
                                Ver evidencia
                            </a>
                        </div>
                    </div>

                    <br>
                    <div class="row">
                        <div class="col-sm-3"></div>
                        <div class="col-sm-6" align="center">
                            <a onclick="revisar_datos_a(<?= ((isset($servicioId) ? $servicioId : "0")) ?>)" class="btn btn-vista" id="llenar_inventario" style="background-color:  #34495e;color:white;">
                                <i class="fa fa-list-ul"></i>
                                Ver Inventario
                            </a>
                        </div> 
                    </div>

                    <br>
                    <div class="row">
                        <div class="col-sm-3"></div>
                        <div class="col-sm-6" align="center">
                            <a href="<?= base_url().'index.php/operadores/index' ?>" class="btn btn-vista" style="background-color:  #581845;color:white;">
                                <i class="fa fa-arrow-circle-left"></i>
                                Regresar al Inicio
                            </a>
                        </div>
                    </div>

                    <!--<input type="hidden" name="paso_evidencia" value="<?= ((isset($paso_evidencia) ? $paso_evidencia : "0")) ?>">-->
                
                <!-- De lo contrario , aun falta algun paso -->
                <?php else: ?>
                    <?php if (isset($facturacion)): ?>
                        <?php if ($paso_evidencia == "4"): ?>
                            <div class="row">
                                <div class="col-sm-2"></div>
                                <div class="col-sm-8" style="background-color: khaki;">
                                   <h6 style="color:darkgreen;font-weight: bold; text-align: center;">
                                       Datos del pago
                                   </h6>

                                   <label style="font-size: 12px;">
                                        <span class="info-titulo">Se aplico un cupón: &nbsp;&nbsp;</span>
                                        <!-- Revisamos si hay algun cupón aplicado -->
                                        <?php if (($facturacion->id_cupon_servicio != NULL)&&($facturacion->id_cupon_servicio != "")): ?>
                                            SI
                                        <?php else: ?>
                                            NO
                                        <?php endif ?>
                                   </label>
                                   
                                   <br>
                                   <label style="font-size: 12px;">
                                        <span class="info-titulo">Datos del cupón: &nbsp;&nbsp;</span>
                                        <!-- Revisamos si hay algun cupón aplicado -->
                                        <?php if (($facturacion->cupon != NULL)&&($facturacion->cupon != "")): ?>
                                            <?php echo $facturacion->cupon." descuento ".$facturacion->descuento."%"; ?> 
                                        <?php else: ?>
                                            NINGUNO
                                        <?php endif ?>
                                   </label>
                                   
                                   <br>
                                   <label style="font-size: 12px;">
                                        <span class="info-titulo">Monto total a pagar: &nbsp;&nbsp;</span>
                                        $ <?= number_format($facturacion->total,2) ?>
                                   </label>
                                </div>
                                <div class="col-sm-2" align="right"></div>
                            </div>

                            <br>
                            <div class="row">
                                <div class="col-sm-3"></div>
                                <div class="col-sm-6" align="center">
                                    <span class="info-titulo">
                                        Tipo de tarjeta:
                                    </span>
                                    <br>
                                    <select id="tt_pago" name="tt_pago" class="form-control" style="width: 85%;">
                                        <option value="">Seleccionar ..</option>
                                        <option value="04">Tarjeta de Crédito</option>
                                        <option value="28">Tarjeta de debito</option>
                                    </select>
                                    <div id="tt_pago_error"></div>
                                </div>
                                <div class="col-sm-3" align="right"></div>
                            </div>
                        <?php endif ?>
                    <?php endif ?>

                    <br>
                    <div class="row">
                        <div class="col-sm-3"></div>
                        <div class="col-sm-6" align="center">
                            <span class="info-titulo">
                                Observaciones:
                            </span>
                            <br>
                            <textarea class="form-control" name="comentarios" rows="2"></textarea>
                            <input type="hidden" id="tt_evidencia" name="tt_evidencia" value="<?= ((isset($titulo) ? $titulo : "")) ?>">
                            <div id="comentarios_error"></div>
                        </div>
                        <div class="col-sm-3" align="right"></div>
                    </div>

                    <br>
                    <div class="row">
                        <div class="col-sm-3"></div>
                        <div class="col-sm-6" align="center">
                            <label for="file-upload" class="subir2">
                                <i class='fas fa-cloud-upload-alt'></i>
                            </label>
                            <label for="file-upload" class="subir">
                                Subir archivo(s)
                            </label>
                            <input id="file-upload" type="file" accept="image/*,video/*" capture="camera" name="archivo[]" multiple style='display: none;cursor: pointer;'/>
                            <div id="info"></div>
                            <input type="hidden" id="archivos" value="0">
                            <div id="evidencias_error"></div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-4"></div>
                        <div class="col-sm-4" align="center">
                            <br>
                            <i id="cargaIcono" class="fa fa-spinner cargaIcono fa-spin"></i>
                            <h5 class="error" id="formulario_error"></h5>

                            <a class="btn btn-success" id="envio_form" style="color:white;">
                                <i class="fa fa-save"></i>
                                Guardar Evidencia
                            </a>
                            <input type="hidden" name="servicio" value="<?= ((isset($servicioId) ? $servicioId : "0")) ?>">
                            
                        </div>
                        <div class="col-sm-4 table-responsive" align="center">
                            <?php if (($paso_evidencia == "4")&&($evidencia_prev == "1")): ?>
                                <a onclick="generar_factura(<?= ((isset($servicioId) ? $servicioId : "0")) ?>)" class="btn btn-vista" style="background-color:  #f39c12 ;color:white;">
                                    Finaliza evidencia <br>de pago ***
                                </a>
                            <?php endif ?>
                        </div>
                    </div>

                    <hr>
                    <div class="row">
                        <div class="col-sm-12" align="center">
                            <?php if (isset($paso)): ?>
                                <?php if ($paso->termina_servicio == "0"): ?>
                                    <a onclick="cancelar_servicio(<?= ((isset($servicioId) ? $servicioId : "0")) ?>)" class="btn-vista btn btn-danger">
                                        <i class="fa fa-times"></i>
                                        Cancelar Servicio
                                    </a>
                                <?php endif ?>
                            <?php endif ?>

                            <?php if (isset($paso->inventario)): ?>
                                <?php if ($paso->evidencia_inicio == "1"): ?>
                                    <a onclick="revisar_datos_2(<?= ((isset($servicioId) ? $servicioId : "0")) ?>)" class="btn btn-dark btn-vista">
                                        <i class="fa fa-file-image-o "></i>
                                        Ver evidencia
                                    </a>
                                <?php endif ?>
                            <?php endif ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12" align="center">
                            <?php if (isset($paso->inventario)): ?>
                                <?php if ($paso->inventario == "0"): ?>
                                    <a onclick="revisar_datos(<?= ((isset($servicioId) ? $servicioId : "0")) ?>)" class="btn btn-primary btn-vista" id="llenar_inventario" style="display: <?= ((isset($paso->evidencia_inicio) ? (($paso->evidencia_inicio == '1') ? 'inline-block' : 'none') : 'none')) ?> ;">
                                        <i class="fa fa-list-ul"></i>
                                        Llenar Inventario
                                    </a>
                                <?php else: ?>
                                    <a onclick="revisar_datos_a(<?= ((isset($servicioId) ? $servicioId : "0")) ?>)" class="btn btn-vista" id="llenar_inventario" style="background-color: #dd33ff;color:white;">
                                        <i class="fa fa-list-ul"></i>
                                        Ver Inventario
                                    </a>

                                    <?php if ($paso_evidencia == "2"): ?>
                                        <a onclick="termina_evidencia(<?= ((isset($servicioId) ? $servicioId : "0")) ?>)" class="btn btn-vista" style="background-color:  #f39c12 ;color:white;">
                                            Finaliza proceso <br>de lavado
                                        </a>
                                    <?php elseif ($paso_evidencia == "3"): ?>
                                        <a onclick="termina_evidencia(<?= ((isset($servicioId) ? $servicioId : "0")) ?>)" class="btn btn-vista" style="background-color:  #f39c12 ;color:white;">
                                            Finaliza evidencia <br>de entrega
                                        </a>
                                    <?php elseif ($paso_evidencia == "4"): ?>
                                        <a onclick="generar_factura(<?= ((isset($servicioId) ? $servicioId : "0")) ?>)" class="btn btn-vista" style="background-color:  #f39c12 ;color:white;">
                                            Finaliza evidencia <br>de pago
                                        </a>
                                    <?php elseif ($paso_evidencia == "5"): ?>
                                        <a onclick="termina_evidencia(<?= ((isset($servicioId) ? $servicioId : "0")) ?>)" class="btn btn-vista" style="background-color:  #16a085;color:white;">
                                            <i class="fa fa-flag-checkered"></i>
                                            Terminar Servicio
                                        </a>
                                    <?php endif ?>
                                <?php endif ?>
                            <?php endif ?>
                        </div>
                    </div>
                <?php endif ?>
                
                <br>
            </div>
        </div>
    </form>

    <?php if (isset($facturacion)): ?>
        <?php if ($paso_evidencia == "4"): ?>
            <div style="display: none;">
                <form id="datafacturacion" method="post" action="" autocomplete="on" enctype="multipart/form-data">
                    <?php 
                        if ((float)$facturacion->total > 0) {
                            $total_neto = (float)$facturacion->total;
                            $total_sinva = $total_neto/1.16;
                            $iva = $total_neto - $total_sinva;
                        } else {
                            $total_sinva = 0;
                            $iva = 0;
                        }                                                
                     ?>

                    <input type="hidden" name="receptor_nombre" value="<?= trim($facturacion->razon_social) ?>">
                    <input type="hidden" name="receptor_id_cliente" value="<?= $facturacion->id_usuario ?>">
                    <input type="hidden" name="fatura_lugarExpedicion" value="<?= $facturacion->cp ?>">
                    <input type="hidden" name="receptor_email" value="<?= $facturacion->email_facturacion ?>">
                    <input type="hidden" name="factura_folio" value="<?= $facturacion->nomenclatura.'-'.$facturacion->id ?>">
                    <input type="hidden" name="factura_serie" value="<?= $facturacion->id ?>">
                    <input type="hidden" name="receptor_direccion" value="<?= $facturacion->domicilio ?>">
                    <input type="hidden" name="factura_formaPago" value="04">
                    <input type="hidden" name="factura_medotoPago" value="PUE">
                    <!--<input type="hidden" name="receptor_RFC" value="<?= $facturacion->rfc ?>">-->
                    <input type="hidden" name="receptor_RFC" value="EKU9003173C9">
                    <input type="hidden" name="receptor_uso_CFDI" value="<?= $facturacion->clave ?>">
                    <input type="hidden" name="concepto_NoIdentificacion" value="1">
                    <input type="hidden" name="concepto_nombre" value="<?= $facturacion->servicioNombre ?>">
                    <input type="hidden" name="concepto_precio" value="<?= $total_sinva ?>">
                    <input type="hidden" name="concepto_importe" value="<?= $total_sinva ?>">
                    <input type="hidden" name="id_producto_servicio_interno" value="<?= $facturacion->id_servicio ?>"> 
                    <input type="hidden" name="concepto_cantidad" value="1">
                    <input type="hidden" name="importe_iva" value="<?= $iva ?>">
                </form>
            </div>                                        
        <?php endif ?>    
    <?php endif ?>
    
    <input type="hidden" id="paginado" value="<?= ((isset($servicioId) ? $servicioId : "0")) ?>">
    <input type="hidden" name="paso_evidencia" value="<?= ((isset($paso_evidencia) ? $paso_evidencia : "0")) ?>">
@endsection
@section('included_js')
    <script type="text/javascript">
        var site_url = "{{site_url()}}";
        var notnotification = true;
    </script>
      
    <script>
        function revisar_datos(indice) {
            var base = $("#sitio").val();
            var url_sis = base+"operadores/redireccionar/"+indice;
            $.ajax({
                url: url_sis,
                method: 'get',
                data: {
                    
                },
                success:function(resp){
                  //console.log(resp);
                    if (resp.indexOf("handler           </p>")<1) {
                        location.href = "<?php echo base_url(); ?>"+"index.php/inventario/index/"+resp;
                    }
                //Cierre de success
                },
                error:function(error){
                    console.log(error);
                //Cierre del error
                }
            //Cierre del ajax
            });
        }

        function revisar_datos_a(indice) {
            var base = $("#sitio").val();
            var url_sis = base+"operadores/redireccionar/"+indice;
            $.ajax({
                url: url_sis,
                method: 'get',
                data: {
                    
                },
                success:function(resp){
                  //console.log(resp);
                    if (resp.indexOf("handler           </p>")<1) {
                        location.href = "<?php echo base_url(); ?>"+"index.php/inventario/generar_revision/"+resp;
                    }
                //Cierre de success
                },
                error:function(error){
                    console.log(error);
                //Cierre del error
                }
            //Cierre del ajax
            });
        }

        function revisar_datos_2(indice) {
            var base = $("#sitio").val();
            var url_sis = base+"operadores/redireccionar/"+indice;
            $.ajax({
                url: url_sis,
                method: 'get',
                data: {
                    
                },
                success:function(resp){
                  //console.log(resp);
                    if (resp.indexOf("handler           </p>")<1) {
                        location.href = "<?php echo base_url(); ?>"+"index.php/operadores/listar_evidencia/"+resp;
                    }
                //Cierre de success
                },
                error:function(error){
                    console.log(error);
                //Cierre del error
                }
            //Cierre del ajax
            });
        }

        function cancelar_servicio(indice) {
            var base = $("#sitio").val();
            var url_sis = base+"operadores/redireccionar/"+indice;
            $.ajax({
                url: url_sis,
                method: 'get',
                data: {
                    
                },
                success:function(resp){
                  //console.log(resp);
                    if (resp.indexOf("handler           </p>")<1) {
                        location.href = "<?php echo base_url(); ?>"+"index.php/operadores/cancelar_servicio/"+resp;
                    }
                //Cierre de success
                },
                error:function(error){
                    console.log(error);
                //Cierre del error
                }
            //Cierre del ajax
            });
        }

        function termina_evidencia(indice) {
            var base = $("#sitio").val();
            var evidencia = $("input[name='paso_evidencia']").val();
            var url_sis = base+"operadores/avance_evidencia";

            $.ajax({
                url: url_sis,
                method: 'post',
                data: {
                    indice : indice,
                    evidencia : evidencia
                },
                success:function(resp){
                  //console.log(resp);
                    if (resp.indexOf("handler           </p>")<1) {
                        location.reload();
                    }
                //Cierre de success
                },
                error:function(error){
                    console.log(error);
                //Cierre del error
                }
            //Cierre del ajax
            });
        }

        function generar_factura(indice) {
            var resultado = false;
            var base = $("#sitio").val();
            var url_sis = base+"operadores/envio_factura";
            $("#cargaIcono").css("display","inline-block");

            var formElement = document.getElementById("datafacturacion");
            //console.log(formElement);
            var datos_facturacion = new FormData(formElement);

            $.ajax({
                url: url_sis,
                //url: "https://sohexs.com/facturacion/index.php/api_factura/crear_factura",
                type: 'post',
                contentType: false,
                data: datos_facturacion,
                processData: false,
                cache: false,
                success:function(resp){
                    console.log(resp);
                    if (resp.indexOf("handler         </p>")<1) {
                        location.reload();
                    }
                    $("#cargaIcono").css("display","none");
                //Cierre de success
                },
                error:function(error){
                    console.log(error);
                    $("#cargaIcono").css("display","none");
                //Cierre del error
                }
            //Cierre del ajax
            });
        }

        $("#file-upload").change(function(){
            var archivos = this.files.length;
            $(".subir").text("Archivos seleccionados ("+archivos+")");

            if (this.files.length > 0) {
                $("#archivos").val(1);
            }else{
                $("#archivos").val(0);
            }
            /*console.log(this.files);
            for (var i = 0; i < archivos; i++) {
                console.log(this.files[i]["name"]);
            }*/
         });
    </script>

    <script type="text/javascript">
        $("#tt_pago").change(function() {
            if ($("#tt_pago").val() != "") {
                var formato = $("#tt_pago").val();
                $("input[name='factura_formaPago']").val(formato);

                //var evidencia = $("#tt_evidencia").val();
                var combo = document.getElementById("tt_pago");
                var tipo = combo.options[combo.selectedIndex].text;

                $("#tt_evidencia").val("Evidencia de pago. "+tipo); 
            }
                
        });

        $("#envio_form").on('click', function (e){
            // Evitamos que salte el enlace.
            e.preventDefault(); 
            var validaciones = validar();
            var evidencia = $("input[name='paso_evidencia']").val();
            if(validaciones){
                $("#cargaIcono").css("display","inline-block");
                var base = $("#sitio").val();
                var paqueteDatos = new FormData(document.getElementById('form-evidencia'));
                paqueteDatos.append('paso_evidencia', $("input[name='paso_evidencia']").prop('value'));
                $.ajax({
                    url: base+"operadores/guardar_evidencia",
                    type: 'post',
                    contentType: false,
                    data: paqueteDatos,
                    processData: false,
                    cache: false,
                    success:function(resp){
                        console.log(resp);
                        if (resp.indexOf("handler         </p>")<1) {
                            if (resp == "OK") {
                                //location.href = base+"index.php/operadores/index";
                                //$("#llenar_inventario").css("display","inline-block");
                                location.reload();
                            } else {
                                $("#formulario_error").text(resp);
                            }

                        }else{
                            $("#formulario_error").text(resp);
                        }
                    //Cierre de success
                    },
                      error:function(error){
                        console.log(error);
                    }
                });
            }else{

            }
        });

        function validar() {
            var retorno = true;
            var campo_a = $("#tt_evidencia").val();
            if (campo_a == "") {
                var error_a = $("#tt_evidencia_error");
                error_a.empty();
                //$("#tt_evidencia_error").css("display","inline-block");
                error_a.append('<label class="form-text text-danger">Campo requerido</label>');
                retorno = false;
            }else{
                var error_a = $("#tt_evidencia_error");
                error_a.empty();
            }

            var campo_b = $("#archivos").val();
            if (campo_b == "0") {
                var error_b = $("#evidencias_error");
                error_b.empty();
                //$("#evidencias_error").css("display","inline-block");
                error_b.append('<label class="form-text text-danger">No se ha seleccionado ningun archivo</label>');
                retorno = false;
            }else{
                var error_b = $("#evidencias_error");
                error_b.empty();
            }

            if ($("#tt_pago").length) {
                var campo_c = $("#tt_pago").val();
                if (campo_c == "") {
                    var error_c = $("#tt_pago_error");
                    error_c.empty();
                    error_c.append('<label class="form-text text-danger">Campo requerido</label>');
                    retorno = false;
                }else{
                    var error_c = $("#tt_pago_error");
                    error_c.empty();
                }
            }

            return retorno;
        }
    </script>
@endsection