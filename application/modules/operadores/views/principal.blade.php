@layout('layout')

@section('included_css')

@endsection

@section('contenido')
    <div class="row">
        <div class="col-sm-12 encabezado-info">
            <h5>
                <i class="fa fa-calendar"></i>
                Mis Servicios
            </h5>
        </div>  
        <div class="cuadro-info">
            <div class="animated fadeIn table-responsive" style="width: 95%;">
                <div class="row">
                    <div class="col-sm-12">
                        <div style="background-color: antiquewhite;width: 100%;">
                            <?php if (isset($inventario)): ?>
                                <?php if ($inventario == "0"): ?>
                                    <br>
                                    <h6 style="color:darkred;text-align: center;">
                                        NO SE PUEDEN INICIAR LOS SERVICIOS SIN QUE SE REALICE <br>
                                        PREVIAMENTE EL INVENTARIO DE LA UNIDAD DEL LAVADOR
                                    </h6>
                                    <br>
                                <?php endif ?>
                            <?php endif ?>
                        </div>

                        <br>
                            
                        <div class="card">
                            <div class="">
                                <table <?php if ($servicios != NULL) echo 'id="bootstrap-data-table"'; ?> class="table table-striped table-bordered" >
                                    <thead>
                                        <tr>
                                            <th>Folio</th>
                                            <th>Servicio</th>
                                            <th>Fecha</th>
                                            <th>Hora</th>
                                            <th>Estatus</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody id="cuerpo">
                                        @if ($servicios != NULL)
                                            @foreach ($servicios as $i => $registro)
                                                <tr style="background-color: <?= (($registro->cancelado == '1') ? 'antiquewhite': (($registro->id_estatus_lavado == '6') ? 'aliceblue' : 'white')) ?> ;">
                                                    <td style="vertical-align: middle;">
                                                        <?= $registro->folio_mostrar ?>
                                                    </td>
                                                    <td style="vertical-align: middle;">
                                                        <?= $registro->servicioNombre ?>
                                                    </td>
                                                    <td style="vertical-align: middle;">
                                                        <?php 
                                                            $fecha = new DateTime($registro->fecha);
                                                            echo $fecha->format('d-m-Y');
                                                        ?>
                                                    </td>
                                                    <td style="vertical-align: middle;" align="center">
                                                        <?= $registro->hora ?>
                                                    </td>
                                                    <td style="vertical-align: middle;" align="center">
                                                        <?php if ($registro->cancelado == '1'): ?>
                                                            CANCELADO
                                                        <?php elseif ($registro->reagendado == '1'): ?>
                                                            REAGENDADO
                                                        <?php else: ?>
                                                            <?= $registro->estatus ?>    
                                                        <?php endif ?>
                                                    </td>
                                                    <td style="vertical-align: middle;" align="center">
                                                        <?php if (($registro->cancelado == '0')&&($registro->reagendado == '0')): ?>
                                                            <?php if ($serv_activo == '1'): ?>
                                                                <?php if (($registro->id_estatus_lavado != '1')&&($registro->id_estatus_lavado != '6')): ?>
                                                                    <a onclick="revisar_datos(<?= $registro->id ?>)" class="btn btn-warning" style="color:white; font-size: 10px;" <?= (($inventario == "0") ? "hidden" : "") ?>>
                                                                        Continuar
                                                                    </a>
                                                                <?php elseif ($registro->id_estatus_lavado == '6'): ?>
                                                                    <a onclick="revisar_servicio(<?= $registro->id ?>)" class="btn btn-primary" style="color:white; font-size: 10px;" <?= (($inventario == "0") ? "hidden" : "") ?>>
                                                                        Ver
                                                                    </a>
                                                                <?php endif ?>

                                                            <?php else: ?>
                                                                <?php if ($registro->id_estatus_lavado == '6'): ?>
                                                                    <a onclick="revisar_servicio(<?= $registro->id ?>)" class="btn btn-primary" style="color:white; font-size: 10px;" <?= (($inventario == "0") ? "hidden" : "") ?>>
                                                                        Ver
                                                                    </a>
                                                                <?php else: ?>
                                                                    <a onclick="revisar_datos(<?= $registro->id ?>)" class="btn btn-success" style="color:white; font-size: 10px;" <?= (($inventario == "0") ? "hidden" : "") ?>>
                                                                        Iniciar
                                                                    </a>
                                                                <?php endif ?>
                                                            <?php endif ?>
                                                        <?php endif ?>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="6" align="center">
                                                    Sin servicios pendientes
                                                </td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- .animated -->
        </div>
    </div>

    <input type="hidden" id="paginado" value="<?= ((isset($servicioId) ? $servicioId : "0")) ?>">
@endsection
@section('included_js')
    @include('main/scripts_dt')
    <script>
        function revisar_datos(indice) {
            var base = $("#sitio").val();
            var url_sis = base+"operadores/redireccionar/"+indice;
            $.ajax({
                url: url_sis,
                method: 'get',
                data: {
                    
                },
                success:function(resp){
                  //console.log(resp);
                    if (resp.indexOf("handler           </p>")<1) {
                        location.href = "<?php echo base_url(); ?>"+"index.php/operadores/ver_servicio/"+resp;
                    }
                //Cierre de success
                },
                error:function(error){
                    console.log(error);
                //Cierre del error
                }
            //Cierre del ajax
            });
        }

        function revisar_servicio(indice) {
            var base = $("#sitio").val();
            var url_sis = base+"operadores/redireccionar/"+indice;
            $.ajax({
                url: url_sis,
                method: 'get',
                data: {
                    
                },
                success:function(resp){
                  //console.log(resp);
                    if (resp.indexOf("handler           </p>")<1) {
                        location.href = "<?php echo base_url(); ?>"+"index.php/operadores/inicia_servicio/"+resp;
                    }
                //Cierre de success
                },
                error:function(error){
                    console.log(error);
                //Cierre del error
                }
            //Cierre del ajax
            });
        }
    </script>
@endsection