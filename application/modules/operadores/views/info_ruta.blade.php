@layout('layout')

@section('included_css')

@endsection

@section('contenido')
    <div class="row">
        <div class="col-sm-12 encabezado-info">
            <h5>
                <i class="fa fa-user-circle"></i>
                Información del cliente
            </h5>
        </div>  
        <div class="cuadro-info">
            <div class="row">
                <div class="col-sm-4">
                    <span class="info-titulo">
                        Nombre:
                    </span>
                    <br>
                    <span class="info-cuerpo">
                        <?= ((isset($servicio[0]->nombre) ? $servicio[0]->nombre : "")) ?>
                    </span>
                </div>
                <div class="col-sm-4">
                    <span class="info-titulo">
                        Apellido Paterno:
                    </span>
                    <br>
                    <span class="info-cuerpo">
                        <?= ((isset($servicio[0]->apellido_paterno) ? $servicio[0]->apellido_paterno : "")) ?>
                    </span>
                </div>
                <div class="col-sm-4">
                    <span class="info-titulo">
                        Apellido Materno:
                    </span>
                    <br>
                    <span class="info-cuerpo">
                        <?= ((isset($servicio[0]->apellido_materno) ? $servicio[0]->apellido_materno : "")) ?>
                    </span>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <span class="info-titulo">
                        Domicilio:
                    </span>
                    <br>
                    <span class="info-cuerpo">
                        @if(isset($servicio[0]->calle))
                            <?php 
                                echo $servicio[0]->calle. " int. " . $servicio[0]->numero_int . " ext. " . $servicio[0]->numero_ext . " Col. " . $servicio[0]->colonia . " " . $servicio[0]->municipio . " " . $servicio[0]->estado; 
                            ?>
                        @endif
                    </span>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-5">
                    <span class="info-titulo">
                        Correo electrónico:
                    </span>
                    <br>
                    <span class="info-cuerpo">
                        <?= ((isset($servicio[0]->email) ? $servicio[0]->email : "")) ?>
                    </span>
                </div>
                <div class="col-sm-3">
                    <span class="info-titulo">
                        Teléfono:
                    </span>
                    <br>
                    <span class="info-cuerpo">
                        <?= ((isset($servicio[0]->telefono) ? $servicio[0]->telefono : "")) ?>
                    </span>
                </div>
                <div class="col-sm-4">
                    <span class="info-titulo">
                        Servicio:
                    </span>
                    <br>
                    <span class="info-cuerpo">
                        <?= ((isset($servicio[0]->servicioNombre) ? $servicio[0]->servicioNombre : "")) ?>
                    </span>
                </div>
            </div>
        </div>
    </div>

    <br>
    <div class="row">
        <div class="col-sm-12 encabezado-info">
            <h5>
                <i class="fa fa-map"></i>
                Ubicación aproximada
            </h5>
        </div>  
        <div class="cuadro-info table-responsive" align="right">
            <a onclick="gelocalizacion()" class="btn btn-info" align="right" style="color:white;margin-right: 15px;">
                <i class="fa fa-map-marker"></i>
                Trazar ruta
            </a>

            <a onclick="avance_ruta()" class="btn btn-success" id="trayectoria" style="color:white;margin-right: 15px;display: none;">
                <i class="fa fa-map-signs"></i>
                Iniciar trayectoria
            </a>

            <a href="https://www.google.es/maps?q=<?= ((isset($servicio[0]->latitud) ? $servicio[0]->latitud.','.$servicio[0]->longitud : '0,0')) ?>" target="_blank" class="btn btn-warning" align="right" style="color:white;">
                <i class="fa fa-location-arrow "></i>
                Abrir Mapa
            </a>

            <br>
            <label style="font-weight: bold;color:darkblue;text-align: left;font-size: 14px;" id="datos_viaje">
                
            </label>

            <br>
            <div id="map"></div>

            <input type="hidden" id="lat_cliente" value="<?= ((isset($servicio[0]->latitud) ? $servicio[0]->latitud : '0')) ?>">
            <input type="hidden" id="log_cliente" value="<?= ((isset($servicio[0]->longitud) ? $servicio[0]->longitud : '0')) ?>">
            <input type="hidden" id="lat_lavador" value="">
            <input type="hidden" id="log_lavador" value="">
            <input type="hidden" id="inicia_seguimiento" value="0">
        </div>
        
    </div>

    <br>
    <?php if ($paso->termina_ruta == "1"): ?>
        <div id="ruta_mensaje" style="background-color: aliceblue;width: 100%;">
            <br>
            <h6 style="color:darkblue;text-align: center;" id="tl_ruta">RUTA TERMINADA</h6>
            <br>
        </div>
    <?php elseif (($paso->pausar_ruta == "0")&&($paso->reanudar_ruta == "0")): ?>
        <div id="ruta_mensaje" style="background-color: aliceblue;width: 100%;">
            <br>
            <h6 style="color:darkblue;text-align: center;" id="tl_ruta">RUTA EN PROCESO</h6>
            <br>
        </div>
    <?php elseif (($paso->pausar_ruta == "1")&&($paso->reanudar_ruta == "0")): ?>
        <div id="ruta_mensaje" style="background-color: antiquewhite;width: 100%;">
            <br>
            <h6 style="color:darkblue;text-align: center;" id="tl_ruta">RUTA PAUSADA</h6>
            <br>
        </div>
    <?php elseif (($paso->pausar_ruta == "1")&&($paso->reanudar_ruta == "1")): ?>
        <div id="ruta_mensaje" style="background-color: aliceblue;width: 100%;">
            <br>
            <h6 style="color:darkblue;text-align: center;" id="tl_ruta">RUTA REANUDADA</h6>
            <br>
        </div>
    <?php else: ?>
        <div id="ruta_mensaje" style="background-color: aliceblue;width: 100%;">
            <br>
            <h6 style="color:darkblue;text-align: center;" id="tl_ruta">RUTA EN PROCESO</h6>
            <br>
        </div>
    <?php endif ?>
    

    <hr>
    <div align="center">
        <a onclick="revisar_datos('3')" class="btn-vista btn btn-danger">
            <i class="fa fa-times"></i>
            Cancelar Servicio
        </a>

        <?php if (($paso->pausar_ruta == "1")&&($paso->reanudar_ruta == "0")): ?>
            <a onclick="revisar_datos('2')" class="btn-vista btn btn-info" id="btn-pause" style="display: none;">
                <i class="fa fa-pause"></i>
                Pausar Ruta
            </a>

            <a onclick="revisar_datos('4')" class="btn-vista btn btn-info" id="btn-continue">
                <i class="fa fa-play"></i>
                Reanudar Ruta
            </a>
        <?php else: ?>
            <a onclick="revisar_datos('2')" class="btn-vista btn btn-info" id="btn-pause">
                <i class="fa fa-pause"></i>
                Pausar Ruta
            </a>

            <a onclick="revisar_datos('4')" class="btn-vista btn btn-info" id="btn-continue" style="display: none;">
                <i class="fa fa-play"></i>
                Reanudar Ruta
            </a>
        <?php endif ?>
    </div>

    <br>
    <div align="center">
        <a onclick="revisar_datos('1')" class="btn-vista btn btn-success">
            <i class="fa fa-map-marker"></i>
            Terminar Ruta
        </a>

        <a href="tel:+52<?= ((isset($servicio[0]->telefono) ? $servicio[0]->telefono : "")) ?>" class="btn-vista btn btn-warning">
            <i class="fa fa-phone"></i>
            Llamar al cliente
        </a>

        <input type="hidden" id="paginadoRuta" value="<?= ((isset($servicioId) ? $servicioId : "0")) ?>">
    </div>


@endsection
@section('included_js')
    <script type="text/javascript">
        var site_url = "{{site_url()}}";
        var notnotification = true;
    </script>
    
    <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyACGPrTl8SJN62FlAetUjwc9cutmFTBrTA&callback=initMap&libraries=places"
        async defer>
    </script> <!-- &libraries=places-->
    
    <script>
        var map;
        var lat = parseFloat(document.getElementById('lat_cliente').value);
        var lng = parseFloat(document.getElementById('log_cliente').value);
        var directionsService;
        var directionsDisplay;
        var marker2 = null;
        var geocoder;
        var service;

        function initMap() {
            directionsService = new google.maps.DirectionsService();
            directionsDisplay = new google.maps.DirectionsRenderer();

            const mapOptions = {
                zoom: 15,
                center: { lat: lat, lng: lng },
            };

            map = new google.maps.Map(document.getElementById("map"), mapOptions);
            directionsDisplay.setMap(map);

            const marker = new google.maps.Marker({
                position: { lat: lat, lng: lng },
                map: map,
            });

            const infowindow = new google.maps.InfoWindow({
                content: "<p>Localizacion:" + marker.getPosition() + "</p>",
            });

            google.maps.event.addListener(marker, "click", () => {
                infowindow.open(map, marker);
            });
        }

        function gelocalizacion() {
            if ("geolocation" in navigator){
                var lat_2 = 0;
                var lng_2 = 0;
                navigator.geolocation.getCurrentPosition(function(position){ 
                    infoWindow = new google.maps.InfoWindow({map: map});
                    var pos = {lat: position.coords.latitude, lng: position.coords.longitude};
                    //infoWindow.setPosition(pos);
                    //infoWindow.setContent("Found your location <br />Lat : "+position.coords.latitude+" </br>Lang :"+ position.coords.longitude);
                    map.panTo(pos);
                    document.getElementById('lat_lavador').value = position.coords.latitude;
                    document.getElementById('log_lavador').value = position.coords.longitude;

                    document.getElementById('inicia_seguimiento').value = "1";

                    lat_2 = position.coords.latitude;
                    lng_2 = position.coords.longitude;
                    trazar_ruta(lat_2,lng_2);
                });

            }else{
                console.log("Browser doesn't support geolocation!");
            }
        }

        function trazar_ruta(lat_2,lng_2) {
            var inicio = { lat: lat_2, lng: lng_2 };
            var fin = { lat: lat, lng: lng };

            var request = {
                origin : inicio,
                destination : fin,
                travelMode: google.maps.TravelMode.DRIVING,
                provideRouteAlternatives: true,
            };

            //console.log(request);
            directionsService.route(request, function(response, status) {
                if (status == google.maps.DirectionsStatus.OK) {
                    directionsDisplay.setDirections(response);
                    directionsDisplay.setMap(map);
                    directionsDisplay.setOptions({suppressMarkers:false});
                }
            });

            //$("#trayectoria").css("display","inline-block");
        }

        function grabar_movimiento() {
            var permiso = document.getElementById('inicia_seguimiento').value;
            //if(permiso == 1){
                if ("geolocation" in navigator){
                    navigator.geolocation.getCurrentPosition(success,error);
                }
            //}
        }

        function success(position) {
            var indice = $("#paginadoRuta").val();
            var base = $("#sitio").val();
            var lat_3 = position.coords.latitude;
            var lng_3 = position.coords.longitude;
            //console.log("coordenadas: "+lat_3+","+lng_3);
            var url_sis = base+"operadores/grabar_posicion";
            $.ajax({
                url: url_sis,
                method: 'post',
                data: {
                    indice : indice,
                    lat_3 : lat_3,
                    lng_3 : lng_3
                },
                success:function(resp){
                  console.log(resp);
                    if (resp.indexOf("handler           </p>")<1) {

                    }
                //Cierre de success
                },
                error:function(error){
                    console.log(error);
                //Cierre del error
                }
            //Cierre del ajax
            });
        }

        function error() {
            console.log("Error permisos");
        }

        setInterval('grabar_movimiento()',5000);

        function avance_ruta() {
            var lat_2 = document.getElementById('lat_lavador').value;
            var lng_2 = document.getElementById('log_lavador').value;

            if (lat_2 != "") {
                var pos = new google.maps.LatLng(lat_2, lng_2);
                var pos_2 = new google.maps.LatLng(lat, lng);
                var tiempo = "";
                var distancia = "";

                geocoder = new google.maps.Geocoder();
                service = new google.maps.DistanceMatrixService();

                service.getDistanceMatrix({
                    origins: [pos],
                    destinations: [pos_2],
                    travelMode: google.maps.TravelMode.DRIVING,
                    unitSystem: google.maps.UnitSystem.METRIC,
                    avoidHighways: false,
                    avoidTolls: false,
                },(response, status) => {
                    if (status === "OK") {
                        distancia = response.rows[0].elements[0].distance.text;
                        tiempo = response.rows[0].elements[0].duration.text;
                        $("#datos_viaje").text("Distancia: "+distancia+" Tiempo estimado: "+tiempo);
                        //console.log("Distancia: "+distancia+" Tiempo estimado: "+tiempo);
                    } else {
                        console.log(status);
                    }
                });

                var request = {
                    origin : pos,
                    destination : pos_2,
                    travelMode: google.maps.TravelMode.DRIVING,
                    provideRouteAlternatives: true,
                };

                //console.log(request);
                directionsService.route(request, function(response, status) {
                    if (status == google.maps.DirectionsStatus.OK) {
                        directionsDisplay.setDirections(response);
                        directionsDisplay.setMap(map);
                        directionsDisplay.setOptions({suppressMarkers:false});
                    }
                });
            }
        }

        //if ($("#inicia_seguimiento").val() == "1") {
            setInterval('avance_ruta()',3000);
        //}

    </script>

    <script>
        function revisar_datos(tipo) {
            var indice = $("#paginadoRuta").val();
            var base = $("#sitio").val();
            var url_sis = base+"operadores/seguimiento_ruta";
            $.ajax({
                url: url_sis,
                method: 'post',
                data: {
                    indice : indice,
                    tipo : tipo
                },
                success:function(resp){
                  //console.log(resp);
                    if (resp.indexOf("handler           </p>")<1) {
                        if (tipo == "1") {
                            location.href = base+"index.php/operadores/inicia_servicio/"+resp;
                        } else {
                            if (tipo == "2") {
                                $("#ruta_mensaje").css("background-color","antiquewhite");
                                $("#tl_ruta").text("RUTA PAUSADA");
                                $("#btn-pause").css("display","none");
                                $("#btn-continue").css("display","inline-block");

                            } else if (tipo == "4") {
                                $("#ruta_mensaje").css("background-color","aliceblue");
                                $("#tl_ruta").text("RUTA REANUDADA");

                                $("#btn-pause").css("display","inline-block");
                                $("#btn-continue").css("display","none");

                            } else {
                                $("#ruta_mensaje").css("background-color","antiquewhite");
                                $("#tl_ruta").text("RUTA CANCELADA");

                                //Retornar a vista de informacion
                            }
                        }
                    }
                //Cierre de success
                },
                error:function(error){
                    console.log(error);
                //Cierre del error
                }
            //Cierre del ajax
            });
        }
    </script>
@endsection