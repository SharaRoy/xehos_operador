@layout('layout')

@section('included_css')

@endsection

@section('contenido')
    <div class="row">
        <div class="col-sm-12 encabezado-info">
            <h5>
                <i class="fa fa-wrench"></i>
                Información del servicio
            </h5>
        </div>  
        <div class="cuadro-info">
            <div class="row">
                <div class="col-sm-6">
                    <span class="info-titulo">
                        Servicio:
                    </span>
                    <br>
                    <span class="info-cuerpo">
                        <?= ((isset($servicio->servicioNombre) ? $servicio->servicioNombre : "")) ?>
                    </span>
                </div>
                <div class="col-sm-3">
                    <span class="info-titulo">
                        Seguro de Lluvia:
                    </span>
                    <br>
                    <span class="info-cuerpo">
                        <?= ((isset($paso->seguro_lluvia) ? (($paso->seguro_lluvia == TRUE) ? "SI" : "NO") : "NO")) ?>
                    </span>
                </div>
                <div class="col-sm-3">
                    <span class="info-titulo">
                        Método de pago:
                    </span>
                    <br>
                    <span class="info-cuerpo">
                        <?= ((isset($servicio->metodo_pago) ? $servicio->metodo_pago : "")) ?>
                    </span>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-3">
                    <span class="info-titulo">
                        Fecha programada:
                    </span>
                    <br>
                    <span class="info-cuerpo">
                        @if(isset($servicio->fecha))
                            <?php 
                                $fecha = new DateTime($servicio->fecha);
                                echo $fecha->format('d-m-Y');
                            ?>
                        @endif
                    </span>
                </div>
                <div class="col-sm-3">
                    <span class="info-titulo">
                        Hora programada:
                    </span>
                    <br>
                    <span class="info-cuerpo">
                        <?= ((isset($servicio->hora) ? $servicio->hora : "")) ?>
                    </span>
                </div>
                <div class="col-sm-3">
                    <span class="info-titulo">
                        Estatus del servicio:
                    </span>
                    <br>
                    <span class="info-cuerpo">
                        <?= ((isset($servicio->estatus) ? $servicio->estatus : "")) ?>
                    </span>
                </div>
                <div class="col-sm-3">
                    <span class="info-titulo">
                        Folio:
                    </span>
                    <br>
                    <span class="info-cuerpo">
                        <?= ((isset($servicio->folio_mostrar) ? $servicio->folio_mostrar : "")) ?>
                    </span>
                </div>
            </div>
        </div>
    </div>

    <br>
    <div class="row">
        <div class="col-sm-12 encabezado-info">
            <h5>
                <i class="fa fa-user-circle"></i>
                Información del cliente
            </h5>
        </div>  
        <div class="cuadro-info">
            <div class="row">
                <div class="col-sm-4">
                    <span class="info-titulo">
                        Nombre:
                    </span>
                    <br>
                    <span class="info-cuerpo">
                        <?= ((isset($servicio->nombre) ? $servicio->nombre : "")) ?>
                    </span>
                </div>
                <div class="col-sm-4">
                    <span class="info-titulo">
                        Apellido Paterno:
                    </span>
                    <br>
                    <span class="info-cuerpo">
                        <?= ((isset($servicio->apellido_paterno) ? $servicio->apellido_paterno : "")) ?>
                    </span>
                </div>
                <div class="col-sm-4">
                    <span class="info-titulo">
                        Apellido Materno:
                    </span>
                    <br>
                    <span class="info-cuerpo">
                        <?= ((isset($servicio->apellido_materno) ? $servicio->apellido_materno : "")) ?>
                    </span>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <span class="info-titulo">
                        Domicilio:
                    </span>
                    <br>
                    <span class="info-cuerpo">
                        @if(isset($servicio->calle))
                            <?php 
                                echo $servicio->calle. " int. " . $servicio->numero_int . " ext. " . $servicio->numero_ext . " Col. " . $servicio->colonia . " " . $servicio->municipio . " " . $servicio->estado; 
                            ?>
                        @endif
                    </span>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-8">
                    <span class="info-titulo">
                        Correo electrónico:
                    </span>
                    <br>
                    <span class="info-cuerpo">
                        <?= ((isset($servicio->email) ? $servicio->email : "")) ?>
                    </span>
                </div>
                <div class="col-sm-4">
                    <span class="info-titulo">
                        Teléfono:
                    </span>
                    <br>
                    <span class="info-cuerpo">
                        <?= ((isset($servicio->telefono) ? $servicio->telefono : "")) ?>
                    </span>
                </div>
            </div>
        </div>
    </div>

    <br>
    <div class="row">
        <div class="col-sm-12 encabezado-info">
            <h5>
                <i class="fa fa-car"></i>
                Información del vehículo
            </h5>
        </div>  
        <div class="cuadro-info">
            <div class="row">
                <div class="col-sm-4">
                    <span class="info-titulo">
                        Color:
                    </span>
                    <br>
                    <span class="info-cuerpo">
                        <?= ((isset($servicio->color) ? $servicio->color : "")) ?>
                    </span>
                </div>
                <div class="col-sm-4">
                    <span class="info-titulo">
                        Marca:
                    </span>
                    <br>
                    <span class="info-cuerpo">
                        <?= ((isset($servicio->marca) ? $servicio->marca : "")) ?>
                    </span>
                </div>
                <div class="col-sm-4">
                    <span class="info-titulo">
                        Modelo:
                    </span>
                    <br>
                    <span class="info-cuerpo">
                        <?= ((isset($servicio->modelo) ? $servicio->modelo : "")) ?>
                    </span>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <span class="info-titulo">
                        Tipo de vehículo:
                    </span>
                    <br>
                    <span class="info-cuerpo">
                        <?= ((isset($servicio->tipo_auto) ? $servicio->tipo_auto : "")) ?>
                    </span>
                </div>
                <div class="col-sm-4">
                    <span class="info-titulo">
                        Placas:
                    </span>
                    <br>
                    <span class="info-cuerpo">
                        <?= ((isset($servicio->placas) ? strtoupper($servicio->placas) : "")) ?>
                    </span>
                </div>
                <div class="col-sm-4">
                    <span class="info-titulo">
                        Serie:
                    </span>
                    <br>
                    <span class="info-cuerpo">
                        <?= ((isset($servicio->numero_serie) ? strtoupper($servicio->numero_serie) : "")) ?>
                    </span>
                </div>
            </div>

            <br>
            <div class="row">
                <div class="col-sm-12" align="center">
                    <a onclick="revisar_datos_vehiculo(<?= ((isset($servicioId) ? $servicioId : "0")) ?>)" class="btn btn-info" style="color:white;">
                        <i class="fa fa-refresh"></i>
                        Actualizar placas
                    </a>
                </div>  
            </div>

            <br>
        </div>
    </div>

    <br>
    <div class="row">
        <div class="col-sm-12 encabezado-info">
            <h5>
                <i class="fa fa-check-square-o"></i>
                Pasos Servicio
            </h5>
        </div>  
        <div class="cuadro-info" align="center">
            <div class="row">
                <div class="col-sm-6" align="center">
                    <table style="width: 100%;margin: 10px;">
                        <tr>
                            <td style="width: 30%;" align="center">
                                <?php if (isset($paso->inicia_ruta)): ?>
                                    <?php if ($paso->inicia_ruta == "1"): ?>
                                        <img src="<?= base_url().'statics/inventario/imgs/check2.png'; ?>" style="width:20px;" >
                                    <?php else: ?>
                                        <img src="<?= base_url().'statics/inventario/imgs/check_ng.png'; ?>" style="width:20px;" >
                                    <?php endif ?>
                                <?php else: ?>
                                    <img src="<?= base_url().'statics/inventario/imgs/check_ng.png'; ?>" style="width:20px;" >
                                <?php endif ?>
                            </td>
                            <td style="width: 70%;font-weight: bold;">
                                1.- RUTA INICIADA
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 30%;" align="center">
                                <?php if (isset($paso->termina_ruta)): ?>
                                    <?php if ($paso->termina_ruta == "1"): ?>
                                        <img src="<?= base_url().'statics/inventario/imgs/check2.png'; ?>" style="width:20px;" >
                                    <?php else: ?>
                                        <img src="<?= base_url().'statics/inventario/imgs/check_ng.png'; ?>" style="width:20px;" >
                                    <?php endif ?>
                                <?php else: ?>
                                    <img src="<?= base_url().'statics/inventario/imgs/check_ng.png'; ?>" style="width:20px;" >
                                <?php endif ?>
                            </td>
                            <td style="width: 70%;font-weight: bold;">
                                2.- RUTA TERMINADA
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 30%;" align="center">
                                <?php if (isset($paso->evidencia_pago)): ?>
                                    <?php if ($paso->evidencia_pago == "1"): ?>
                                        <img src="<?= base_url().'statics/inventario/imgs/check2.png'; ?>" style="width:20px;" >
                                    <?php else: ?>
                                        <img src="<?= base_url().'statics/inventario/imgs/check_ng.png'; ?>" style="width:20px;" >
                                    <?php endif ?>
                                <?php else: ?>
                                    <img src="<?= base_url().'statics/inventario/imgs/check_ng.png'; ?>" style="width:20px;" >
                                <?php endif ?>
                            </td>
                            <td style="width: 70%;font-weight: bold;">
                                3.- COMPROBANTE DE PAGO
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 30%;" align="center">
                                <?php if (isset($paso->evidencia_inicio)): ?>
                                    <?php if ($paso->evidencia_inicio == "1"): ?>
                                        <img src="<?= base_url().'statics/inventario/imgs/check2.png'; ?>" style="width:20px;" >
                                    <?php else: ?>
                                        <img src="<?= base_url().'statics/inventario/imgs/check_ng.png'; ?>" style="width:20px;" >
                                    <?php endif ?>
                                <?php else: ?>
                                    <img src="<?= base_url().'statics/inventario/imgs/check_ng.png'; ?>" style="width:20px;" >
                                <?php endif ?>
                            </td>
                            <td style="width: 70%;font-weight: bold;">
                                4.- EVIDENCIA (COMO SE RECIBE LA UNIDAD)
                            </td>
                        </tr>
                        
                    </table>
                </div>
                <div class="col-sm-6" align="center">
                    <table style="width: 100%;margin: 10px;">
                        <tr>
                            <td style="width: 30%;" align="center">
                                <?php if (isset($paso->inventario)): ?>
                                    <?php if ($paso->inventario == "1"): ?>
                                        <img src="<?= base_url().'statics/inventario/imgs/check2.png'; ?>" style="width:20px;" >
                                    <?php else: ?>
                                        <img src="<?= base_url().'statics/inventario/imgs/check_ng.png'; ?>" style="width:20px;" >
                                    <?php endif ?>
                                <?php else: ?>
                                    <img src="<?= base_url().'statics/inventario/imgs/check_ng.png'; ?>" style="width:20px;" >
                                <?php endif ?>
                            </td>
                            <td style="width: 70%;font-weight: bold;">
                                5.- INVENTARIO
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 30%;" align="center">
                                <?php if (isset($paso->evidencia_lavado)): ?>
                                    <?php if ($paso->evidencia_lavado == "1"): ?>
                                        <img src="<?= base_url().'statics/inventario/imgs/check2.png'; ?>" style="width:20px;" >
                                    <?php else: ?>
                                        <img src="<?= base_url().'statics/inventario/imgs/check_ng.png'; ?>" style="width:20px;" >
                                    <?php endif ?>
                                <?php else: ?>
                                    <img src="<?= base_url().'statics/inventario/imgs/check_ng.png'; ?>" style="width:20px;" >
                                <?php endif ?>
                            </td>
                            <td style="width: 70%;font-weight: bold;">
                                6.- EVIDENCIA (PROCESO DE LAVADO)
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 30%;" align="center">
                                <?php if (isset($paso->evidencia_finaliza)): ?>
                                    <?php if ($paso->evidencia_finaliza == "1"): ?>
                                        <img src="<?= base_url().'statics/inventario/imgs/check2.png'; ?>" style="width:20px;" >
                                    <?php else: ?>
                                        <img src="<?= base_url().'statics/inventario/imgs/check_ng.png'; ?>" style="width:20px;" >
                                    <?php endif ?>
                                <?php else: ?>
                                    <img src="<?= base_url().'statics/inventario/imgs/check_ng.png'; ?>" style="width:20px;" >
                                <?php endif ?>
                            </td>
                            <td style="width: 70%;font-weight: bold;">
                                7.- EVIDENCIA (LAVADO TERMINADO: UNIDAD PARA ENTREGA)
                            </td>
                        </tr>
                        
                        <tr>
                            <td style="width: 30%;" align="center">
                                <?php if (isset($paso->termina_servicio)): ?>
                                    <?php if ($paso->termina_servicio == "1"): ?>
                                        <img src="<?= base_url().'statics/inventario/imgs/check2.png'; ?>" style="width:20px;" >
                                    <?php else: ?>
                                        <img src="<?= base_url().'statics/inventario/imgs/check_ng.png'; ?>" style="width:20px;" >
                                    <?php endif ?>
                                <?php else: ?>
                                    <img src="<?= base_url().'statics/inventario/imgs/check_ng.png'; ?>" style="width:20px;" >
                                <?php endif ?>
                            </td>
                            <td style="width: 70%;font-weight: bold;">
                                8.- SERVICIO TERMINADO
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            
            <br>
        </div>
    </div>

    <?php if (isset($servicio->cancelado)): ?>
        <?php if ($servicio->cancelado == "1"): ?>
            <div style="background-color: antiquewhite;width: 100%;">
                <br>
                <h6 style="color:darkblue;text-align: center;">SERVICIO CANCELADO</h6>
                <br>
            </div>
        <?php endif ?>    
    <?php endif ?>

    <br>
    <div align="center">
        <?php if (isset($paso)): ?>
            <?php if ($paso->termina_servicio == "0"): ?>
                <a onclick="cancelar_servicio(<?= ((isset($servicioId) ? $servicioId : "0")) ?>)" class="btn-vista btn btn-danger">
                    <i class="fa fa-times"></i>
                    Cancelar Servicio
                </a>
            <?php endif ?>
        <?php endif ?>

        <?php if (isset($paso)): ?>
            <?php if (($servicio->cancelado == "0")&&($servicio->reagendado == "0")): ?>
                <?php if ($paso->termina_ruta == "1"): ?>
                    <a onclick="revisar_datos_2(<?= ((isset($servicioId) ? $servicioId : "0")) ?>)" class="btn-vista btn btn-success">
                        <i class="fa fa-wrench"></i>
                        Ir al servicio
                    </a>
                <?php else: ?>
                    <a onclick="revisar_datos(<?= ((isset($servicioId) ? $servicioId : "0")) ?>)" class="btn-vista btn btn-success">
                        <i class="fa fa-map-o"></i>
                        <?php if (($paso->inicia_ruta == "1")&&($paso->pausar_ruta == "1")): ?>
                            Continuar Ruta
                        <?php else: ?>
                            Iniciar Ruta
                        <?php endif ?>
                    </a>
                <?php endif ?>
            <?php endif ?>
        <?php else: ?>
            <?php if (isset($servicio->cancelado)): ?>
                <?php if (($servicio->cancelado == "0")&&($servicio->reagendado == "0")): ?>
                    <a onclick="revisar_datos(<?= ((isset($servicioId) ? $servicioId : "0")) ?>)" class="btn-vista btn btn-success">
                        <i class="fa fa-map-o"></i>
                        Iniciar Ruta
                    </a>
                <?php endif ?>
            <?php endif ?>
        <?php endif ?>
            

        <a href="tel:+52<?= ((isset($servicio->telefono) ? $servicio->telefono : "")) ?>" class="btn-vista btn btn-warning">
            <i class="fa fa-phone"></i>
            Llamar al cliente
        </a>
        <input type="hidden" id="paginado" value="<?= ((isset($servicioId) ? $servicioId : "0")) ?>">
        
        <?php if (isset($paso)): ?>
            <input type="hidden" name="paso_evidencia" value="<?= ((($paso->termina_servicio == '1')||($paso->servicio_cancelado == '1')) ? '6' : '1') ?>">
        <?php endif ?>
        
    </div>

@endsection
@section('included_js')
    <script>
        function revisar_datos(indice) {
            var base = $("#sitio").val();
            var url_sis = base+"operadores/redireccionar/"+indice;
            $.ajax({
                url: url_sis,
                method: 'get',
                data: {
                    
                },
                success:function(resp){
                  //console.log(resp);
                    if (resp.indexOf("handler           </p>")<1) {
                        location.href = "<?php echo base_url(); ?>"+"index.php/operadores/inicar_ruta/"+resp;
                    }
                //Cierre de success
                },
                error:function(error){
                    console.log(error);
                //Cierre del error
                }
            //Cierre del ajax
            });
        }

        function revisar_datos_2(indice) {
            var base = $("#sitio").val();
            var url_sis = base+"operadores/redireccionar/"+indice;
            $.ajax({
                url: url_sis,
                method: 'get',
                data: {
                    
                },
                success:function(resp){
                  //console.log(resp);
                    if (resp.indexOf("handler           </p>")<1) {
                        location.href = "<?php echo base_url(); ?>"+"index.php/operadores/inicia_servicio/"+resp;
                    }
                //Cierre de success
                },
                error:function(error){
                    console.log(error);
                //Cierre del error
                }
            //Cierre del ajax
            });
        }

        function revisar_datos_vehiculo(indice) {
            var base = $("#sitio").val();
            var url_sis = base+"operadores/redireccionar/"+indice;
            $.ajax({
                url: url_sis,
                method: 'get',
                data: {
                    
                },
                success:function(resp){
                  //console.log(resp);
                    if (resp.indexOf("handler           </p>")<1) {
                        location.href = "<?php echo base_url(); ?>"+"index.php/operadores/editar_vehiculo/"+resp;
                    }
                //Cierre de success
                },
                error:function(error){
                    console.log(error);
                //Cierre del error
                }
            //Cierre del ajax
            });
        }

        function cancelar_servicio(indice) {
            var base = $("#sitio").val();
            var url_sis = base+"operadores/redireccionar/"+indice;
            $.ajax({
                url: url_sis,
                method: 'get',
                data: {
                    
                },
                success:function(resp){
                  //console.log(resp);
                    if (resp.indexOf("handler           </p>")<1) {
                        location.href = "<?php echo base_url(); ?>"+"index.php/operadores/cancelar_servicio/"+resp;
                    }
                //Cierre de success
                },
                error:function(error){
                    console.log(error);
                //Cierre del error
                }
            //Cierre del ajax
            });
        }
    </script>
@endsection