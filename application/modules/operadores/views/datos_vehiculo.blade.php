@layout('layout')

@section('included_css')

@endsection

@section('contenido')
    <div class="row">
        <div class="col-sm-12 encabezado-info">
            <h5>
                <i class="fa fa-car"></i>
                Información del vehículo
            </h5>
        </div>  
        <div class="cuadro-info">
            <form id="form-vehiculo" method="post">
                <div class="row">
                    <div class="col-sm-4">
                        <span class="info-titulo">
                            Color:
                        </span>
                        <br>
                        <input type="text" class="form-control" name="" value="<?= ((isset($servicio[0]->color) ? $servicio[0]->color : "")) ?>" disabled>
                    </div>
                    <div class="col-sm-4">
                        <span class="info-titulo">
                            Marca:
                        </span>
                        <br>
                        <input type="text" class="form-control" name="" value="<?= ((isset($servicio[0]->marca) ? $servicio[0]->marca : "")) ?>" disabled>
                    </div>
                    <div class="col-sm-4">
                        <span class="info-titulo">
                            Modelo:
                        </span>
                        <br>
                        <input type="text" class="form-control" name="" value="<?= ((isset($servicio[0]->modelo) ? $servicio[0]->modelo : "")) ?>" disabled>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-4">
                        <span class="info-titulo">
                            Tipo de vehículo:
                        </span>
                        <br>
                        <input type="text" class="form-control" name="" value="<?= ((isset($servicio[0]->tipo_auto) ? $servicio[0]->tipo_auto : "")) ?>" disabled>
                    </div>
                    <div class="col-sm-4">
                        <span class="info-titulo">
                            Placas:
                        </span>
                        <br>
                        <input type="text" class="form-control" maxlength="10" name="placas" value="<?= ((isset($servicio[0]->placas) ? strtoupper($servicio[0]->placas) : "")) ?>" style="text-transform: uppercase;">
                        <div id="placas_error"></div>
                    </div>
                    <div class="col-sm-4">
                        <span class="info-titulo">
                            Serie:
                        </span>
                        <br>
                        <input type="text" class="form-control" maxlength="20" name="serie" value="<?= ((isset($servicio[0]->numero_serie) ? (($servicio[0]->numero_serie != '') ? strtoupper($servicio[0]->numero_serie) : 'XXXXXXXXXXXXXX') : "XXXXXXXXXXXXXX")) ?>" style="text-transform: uppercase;">
                        <div id="serie_error"></div>
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-sm-12" align="center">
                        <input type="button" class="btn btn-success" name="envio_form" id="envio_form" value="Actualizar Datos">

                        <input type="hidden" name="servicio" value="<?= ((isset($servicioId) ? $servicioId : "0")) ?>">
                        <input type="hidden" name="auto" value="<?= ((isset($servicio[0]->id_auto) ? $servicio[0]->id_auto : "")) ?>">
                    </div>  
                </div>
            </form>

            <br>
        </div>
    </div>

    <input type="hidden" id="paginado" value="<?= ((isset($servicioId) ? $servicioId : "0")) ?>">
    <?php if (isset($paso)): ?>
        <input type="hidden" name="paso_evidencia" value="<?= ((($paso->termina_servicio == '1')||($paso->servicio_cancelado == '1')) ? '6' : '1') ?>">
    <?php endif ?>

@endsection
@section('included_js')
    <script type="text/javascript">
        $("#envio_form").on('click', function (e){
            // Evitamos que salte el enlace.
            e.preventDefault(); 
            var validaciones = validar();
            if(validaciones){
                var base = $("#sitio").val();
                var paqueteDatos = new FormData(document.getElementById('form-vehiculo'));
                $.ajax({
                    url: base+"operadores/act_datos_vehiculo",
                    type: 'post',
                    contentType: false,
                    data: paqueteDatos,
                    processData: false,
                    cache: false,
                    success:function(resp){
                        console.log(resp);
                        if (resp.indexOf("handler         </p>")<1) {
                            var resp2 = resp.split("=");
                            if (resp2[0] == "OK") {
                                location.href = base+"index.php/operadores/ver_servicio/"+resp2[1];
                            } else {
                                $("#formulario_error").text(resp);
                            }

                        }else{
                            $("#formulario_error").text(resp);
                        }
                    //Cierre de success
                    },
                      error:function(error){
                        console.log(error);
                    }
                });
            }else{

            }
        });

        function validar() {
            var retorno = true;
            var campo_a = $("input[name='placas']").val();
            if (campo_a == "") {
                var error_a = $("#placas_error");
                error_a.empty();
                error_a.append('<label class="form-text text-danger">Campo requerido</label>');
                retorno = false;
            }else{
                var error_a = $("#placas_error");
                error_a.empty();
            }

            var campo_b = $("input[name='serie']").val();
            if (campo_b == "") {
                var error_b = $("#serie_error");
                error_b.empty();
                error_b.append('<label class="form-text text-danger">Campo requerido</label>');
                retorno = false;
            }else{
                var error_b = $("#serie_error");
                error_b.empty();
            }

            return retorno;
        }
    </script>
@endsection