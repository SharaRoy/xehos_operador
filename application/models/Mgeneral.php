<?php
/**

 **/
class Mgeneral extends CI_Model{

    /**

     **/
    public function __construct()
    {
        parent::__construct();
    }

    public function qr($url,$serie){

      //cargamos la librería
      $this->load->library('ciqrcode');
      //hacemos configuraciones
      $params['data'] = "https://sohex.mx/cs/autolavapp/index.php/gafete/lavador/".$serie;
      $params['level'] = 'H';
      $params['size'] = 5;
      //decimos el directorio a guardar el codigo qr, en este
      //caso una carpeta en la raíz llamada qr_code
      $anio=date('Y');
      $mes=date('m');
      $carpeta = 'assets/images/qr/'.$serie;
  
        if (!file_exists($carpeta)) {
            mkdir($carpeta, 0777, true);
        }
  
      $nombre_imagen=$carpeta.'/'.$serie.'.png';
      $params['savename'] = FCPATH.$nombre_imagen;
      //generamos el código qr
      $this->ciqrcode->generate($params);
      return $nombre_imagen;
    }
     //guarda los modelos
    public function guardarUsuario(){
       $datos = array('adminNombre' => $this->input->post('name'),
                      'adminUsername' => $this->input->post('user'),
                      'adminPassword' => $this->input->post('password'),
                      'adminEmail' => $this->input->post('email'),
                      'adminStatus' => 1,
                      'adminFecha' => date('Y-m-d H:i:s')
        );
        $q= $this->db->where('adminUsername',$datos['adminUsername'])->get('admin');
        if($q->num_rows()==1){
          echo -1;exit();
        }
        if($this->db->insert('admin',$datos)){
          echo 1;
        }else{
          echo 0;
        }
        die();
    }

    public function total_factura_cliente($id_cliente){
      $general_total = 0;
      $facturas = $this->get_result('receptor_id_cliente',$id_cliente,'factura');
      foreach($facturas as $fac):
        $general_total+= $this->factura_subtotal($fac->facturaID) + $this->factura_iva_total($fac->facturaID);
      endforeach;
      return $general_total;
    }

    public function abono_factura_cliente($id_cliente){
      $general_abono = 0;
      $facturas = $this->get_result('receptor_id_cliente',$id_cliente,'factura');
      foreach($facturas as $fac):
       $general_abono+= $this->complementoPagoPagado($fac->sat_uuid);
      endforeach;
      return $general_abono;
    }

    public function saldo_factura_cliente($id_cliente){
      $total_saldo = 0;

      $facturas = $this->get_result('receptor_id_cliente',$id_cliente,'factura');
      foreach($facturas as $fac):
        if($fac->factura_medotoPago == "PPD"):
        $saldo = $this->Mgeneral->complementoPagoPagado($fac->sat_uuid);
        $total_saldo+= (($this->factura_subtotal($fac->facturaID) + $this->factura_iva_total($fac->facturaID))-$saldo);
       endif;
      endforeach;
      return $total_saldo;
    }

    public function complementos_total_pagado($uuid){
      $com = $this->get_row('uuid',$uuid,'complemento_cfdi_relacionado_p');
      $filas = $this-> get_result('id_complemento',$com->id_complemento,'complemento_datos');
      $subtotal = 0;
      foreach($filas as $fil):
        $subtotal+= $fil->total_pago;
      endforeach;
      return $subtotal;
    }

    public function complementoPagoPagado($uuid){
      $this->db->select('complemento_datos.total_pago');
      $this->db->from('complemento_cfdi_relacionado_p');
      $this->db->join('complemento_datos', 'complemento_cfdi_relacionado_p.id_complemento = complemento_datos.id_complemento');
      $this->db->where('complemento_cfdi_relacionado_p.uuid',$uuid);
      $resultado = $this->db->get()->result();
      
      $subtotal = 0;
      foreach($resultado as $fil):
        $subtotal+= $fil->total_pago;
      endforeach;
      return $subtotal;
    }

    public function factura_subtotal($id_factura){
      $filas = $this-> get_result('concepto_facturaId',$id_factura,'factura_conceptos');
      $subtotal = 0;
      foreach($filas as $fil):
        $subtotal+= $fil->concepto_importe;
      endforeach;
      return $subtotal;
    }

    public function factura_iva_total($id_factura){
      $filas = $this-> get_result('concepto_facturaId',$id_factura,'factura_conceptos');
      $subtotal = 0;
      foreach($filas as $fil):
        $subtotal+= $fil->concepto_importe;
      endforeach;
      return $subtotal*.16;
    }

    public function factura_descuento($id_factura){
      $filas = $this-> get_result('concepto_facturaId',$id_factura,'factura_conceptos');
      $descuento = 0;
      foreach($filas as $fil):
        $descuento+= $fil->concepto_descuento;
      endforeach;
      return $descuento;
    }

    public function factura_iva(){
      $filas = $this-> get_result('concepto_facturaId',$id_factura,'factura_conceptos');
      $iva = 0;
      foreach($filas as $fil):
        $descuento+= $fil->concepto_descuento;
      endforeach;
      return $descuento;
    }

    public function factura_traslados($id_factura){

    }

    public function factura_retenciones($id_factura){

    }

    public function factura_total($id_factura){

    }

    public function get_productos_orden($id_producto){
      $query = $this->db->query("SELECT SUM(orden_compra_productos.ordenP_cantidad) AS total
      FROM orden_compra_productos
      INNER JOIN orden_compra
      ON orden_compra_productos.ordenP_idOrden = orden_compra.ordenId
      WHERE orden_compra.orden_status = 2 AND orden_compra_productos.ordenP_idProducto = ".$id_producto);

      //$data = $this->db->get('productos');
      return $query->row();
    }

    public function buscar_producto($busqueda){
      $this->db->select("productoNombre AS label, `productoId`, `productoNombre`, `productoMedida`, `productoReferencia`, `productoClasificado`, `productoModelo`, `productoIdMarca`, `productoFlete`, `productoTipo`, `procuctoContenido`, `productoIngredienteActivo`, `productoRegCofepris`, `productoRegEpa`, `productoPresantacion`, `productoDosisAlta`, `productoDosisBaja`, `productoMedidaAlta`, `productoMedidaBaja`, `productoFichaTecnica`, `productoEspecificaciones`, `productoPrecioPublico`, `productoPrecioMayoreo`, `productoPrecioCredito`, `productoGrupo`, `productoCantidadMinima`, `productoCantidadMaxima`, `poductoInventariar`");
      $this->db->like('productoNombre', $busqueda);
      $data = $this->db->get('productos','both');
      return $data->result();

    }

    public function buscar_cliente($busqueda){
      $this->db->select("nombre AS label, `id`, `rfc`, `razon_social`, `calle`, `numero`, `colonia`, `cp`, `municipio`, `ciudad`, `estado`, `pais`, `telefono`, `email`, `tados_bancarios`, `nombre`, `fecha_actualizacion`, `regimen`");
      $this->db->like('nombre', $busqueda);
      $data = $this->db->get('clientes','both');
      return $data->result();

    }

    public function buscar_proveedor($busqueda){
      $this->db->select("CONCAT(rfc,'-',nombre) AS label, `id`, `rfc`, `razon_social`, `calle`, `numero`, `colonia`, `cp`, `municipio`, `ciudad`, `estado`, `pais`, `telefono`, `email`, `tados_bancarios`, `nombre`, `fecha_actualizacion`, `regimen`");
      $this->db->like('nombre', $busqueda,'both');
      $data = $this->db->get('clientes');
      return $data->result();

    }

    public function ver_permiso_menu($id_usuario){
      $respuesta = $this->get_row('id_usuario',$id_usuario,'menu');
      return $respuesta;
    }

    public function save_register($table, $data)
      {
          $this->db->insert($table, $data);
          return $this->db->insert_id();
      }

      public function delete_row($tabla,$id_tabla,$id){
		   $this->db->delete($tabla, array($id_tabla=>$id));
    	}

      public function get_table($table){
    		$data = $this->db->get($table)->result();
    		return $data;
    	}

      public function get_row($campo,$value,$tabla){
    		return $this->db->where($campo,$value)->get($tabla)->row();
    	}

      public function get_result($campo,$value,$tabla){
    		return $this->db->where($campo,$value)->get($tabla)->result();
    	}

      public function update_table_row($tabla,$data,$id_table,$id){
    		$this->db->update($tabla, $data, array($id_table=>$id));
    	}

      public function update_permiso_rol($data,$permiso,$id_rol){
        $this->db->update('permisos_roles', $data, array("permisoCategoria"=>$permiso,"permisoIdRol"=>$id_rol));

      }

      public function save_admin($data){
    		$this->db->insert('admin', $data);
            return $this->db->insert_id();
      }
      
      public function update_admin($data, $id){
    		return $this->db->update('admin', $data,array('adminId' => $id));
    	}

      public function count_results_users($user, $pass)
        {
            $this->db->where('adminUsername', $user);
            $this->db->where('adminPassword', $pass);
            $total = $this->db->count_all_results('admin');
            return $total;
        }

        public function get_all_data_users_specific($username, $pass)
          {
              $this->db->where('adminUsername', $username);
              $this->db->where('adminPassword', $pass);
              $data = $this->db->get('admin');
              return $data->row();
          }

      public function get_contacto_usuario($id_usuario){
        return $this->db->where('tipo',1)->where('id_tabla',$id_usuario)->get('usuarios_contactos')->result();
      }

      public function get_contacto_proveedor($id_usuario){
        return $this->db->where('tipo',2)->where('id_tabla',$id_usuario)->get('usuarios_contactos')->result();
      }

      public function get_result_field($campo_1 = "",$value_1 = "",$campo_2 = "",$value_2 = "",$tabla = ""){
          $result = $this->db->where($campo_1,$value_1)->where($campo_2,$value_2)->get($tabla)->result();
          return $result;
      }
}
