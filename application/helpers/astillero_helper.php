<?php
function get_guid()
{
        mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
        $charid = strtoupper(md5(uniqid(rand(), true)));
        return $charid;
}
function nombre_unidad($id)
{
    $CI =& get_instance();
    $CI->db->select('*')
           ->from('unidades')
           ->where('id', $id);
    $data = $CI->db->get();
    $nombre = $data->row();
    return $nombre->placas;
}

function nomenclatura_sucursal($id)
{
    $CI =& get_instance();
    $CI->db->select('*')
           ->from('sucursales')
           ->where('id', $id);
    $data = $CI->db->get();
    $nombre = $data->row();
    return $nombre->nomenclatura;
}
function nombre_lavador($id)
{
    $CI =& get_instance();
    $CI->db->select('*')
           ->from('lavadores')
           ->where('lavadorId', $id);
    $data = $CI->db->get();
    $nombre = $data->row();
    return $nombre->lavadorNombre;
}
function nombre_producto($id)
{
    $CI =& get_instance();
    $CI->db->select('*')
           ->from('productos')
           ->where('productoId', $id);
    $data = $CI->db->get();
    $nombre = $data->row();
    return $nombre->productoNombre;
}

function nombre_categoria($id)
{
    $CI =& get_instance();
    $CI->db->select('*')
           ->from('categoria')
           ->where('categoriaId', $id);
    $data = $CI->db->get();
    $nombre = $data->row();
    return $nombre->categoriaNombre;
}

function nombre_subcategoria($id)
{
    $CI =& get_instance();
    $CI->db->select('*')
           ->from('subcategoria')
           ->where('subcategoriaId', $id);
    $data = $CI->db->get();
    $nombre = $data->row();
    return $nombre->subcategoriaNombre;
}


function nombre_medida($id)
{
    $CI =& get_instance();
    $CI->db->select('*')
           ->from('medidas')
           ->where('medidaId', $id);
    $data = $CI->db->get();
    $nombre = $data->row();
    return $nombre->medidaNombre;
}

function nombre_boquilla($id)
{
    $CI =& get_instance();
    $CI->db->select('*')
           ->from('boquillas')
           ->where('id', $id);
    $data = $CI->db->get();
    $nombre = $data->row();
    return $nombre->boquilla;
}

function boquilla_aolicacion($id)
{
    $CI =& get_instance();
    $CI->db->select('*')
           ->from('boquillas')
           ->where('id', $id);
    $data = $CI->db->get();
    $nombre = $data->row();
    return $nombre->milimetrosxsegundo;
}

function nombre_rol($id)
{
    $CI =& get_instance();
    $CI->db->select('*')
           ->from('roles')
           ->where('rolId', $id);
    $data = $CI->db->get();
    $nombre = $data->row();
    return $nombre->rolNombre;
}

function get_rol_id($id)
{
    $CI =& get_instance();
    $CI->db->select('*')
           ->from('usuarios')
           ->where('usuarioId', $id);
    $data = $CI->db->get();
    $nombre = $data->row();
    return $nombre->idRol;
}

function get_rol_permiso($categoria,$permisoIdRol)
{
    $CI =& get_instance();
    $CI->db->select('*')
           ->from('permisos_roles')
           ->where('permisoCategoria', $categoria)
           ->where('permisoIdRol', $permisoIdRol);
    $data = $CI->db->get();
    $nombre = $data->row();
    return $nombre;
}

function codigo_postal()
{
    $CI =& get_instance();
    $CI->db->select('*')
           ->from('datos')
           ->where('id', 1);
    $data = $CI->db->get();
    $nombre = $data->row();
    return $nombre->cp;
}

function nombre_actividad($id)
{
    $CI =& get_instance();
    $CI->db->select('*')
           ->from('actividades')
           ->where('actividadId', $id);
    $data = $CI->db->get();
    $nombre = $data->row();
    return $nombre->actividadNombre;
}

function nombre_lugar_aplicacion($id)
{
    $CI =& get_instance();
    $CI->db->select('*')
           ->from('lugares_aplicacion')
           ->where('lugaresId', $id);
    $data = $CI->db->get();
    $nombre = $data->row();
    return $nombre->lugaresNombre;
}

function nombre_plaga($id)
{
    $CI =& get_instance();
    $CI->db->select('*')
           ->from('plagas')
           ->where('plagaId', $id);
    $data = $CI->db->get();
    $nombre = $data->row();
    return $nombre->plagaNombre;
}

function nombre_metodo($id)
{
    $CI =& get_instance();
    $CI->db->select('*')
           ->from('metodos')
           ->where('metodoId', $id);
    $data = $CI->db->get();
    $nombre = $data->row();
    return $nombre->metodoNombre;
}

function nombre_proveedor($id)
{
    $CI =& get_instance();
    $CI->db->select('*')
           ->from('proveedores')
           ->where('id', $id);
    $data = $CI->db->get();
    if($data->num_rows() > 0){
      $nombre = $data->row();
      return $nombre->nombre;
    }else{
      return "sin proveedor";
    }
}

function nombre_cliente($id)
{
    $CI =& get_instance();
    $CI->db->select('*')
           ->from('clientes')
           ->where('id', $id);
    $data = $CI->db->get();
    if($data->num_rows() > 0){
      $nombre = $data->row();
      return $nombre->nombre;
    }else{
      return "sin cliente";
    }
}
function encrypt($data){
    $id = (double)$data*CONST_ENCRYPT;
    $url_id = base64_encode($id);
    $url = str_replace("=", "" ,$url_id);
    return $url;
    //return $data;
}
function decrypt($data){
	$url_id=base64_decode($data);
	$id=(double)$url_id/CONST_ENCRYPT;
	return $id;
}
?>
