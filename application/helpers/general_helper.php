<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
function existe_cita($id_operador = '', $fecha = '', $hora = '')
{
	$ins = &get_instance();
	$ins->load->model('citas/m_citas', 'mc');
	return $ins->mc->existe_cita($id_operador, $fecha, $hora);
}
function datos_cita($id_horario = '', $tipo)
{
	$ins = &get_instance();
	$ins->load->model('citas/m_citas', 'mc');
	return $ins->mc->datos_cita($id_horario, $tipo);
}
function getHoraComidaTecnico($hora = '', $fecha = '', $id_tecnico = '')
{
	$ins = &get_instance();
	$ins->load->model('citas/m_citas', 'mc');
	return $ins->mc->getHoraComidaTecnico($hora, $fecha, $id_tecnico);
}
function getHoraLaboralTecnico($hora = '', $fecha = '', $id_tecnico = '')
{
	$ins = &get_instance();
	$ins->load->model('citas/m_citas', 'mc');
	return $ins->mc->getHoraLaboralTecnico($hora, $fecha, $id_tecnico);
	//1
}
function getStatusCita($id_horario = '')
{
	$ins = &get_instance();
	$ins->load->model('citas/m_citas', 'mc');
	return $ins->mc->getStatusCita($id_horario);
}
function HoraBetweenCita($hora = '', $fecha = '', $id_tecnico = '')
{
	$ins = &get_instance();
	$ins->load->model('citas/m_citas', 'mc');
	return $ins->mc->HoraBetweenCita($hora, $fecha, $id_tecnico);
}
function getColorCita($id_cita = '')
{
	$ins = &get_instance();
	$ins->load->model('citas/m_citas', 'mc');
	return $ins->mc->getColorCita($id_cita);
}
function getHoraWork($fecha = '', $idtecnico = '')
{
	$ins = &get_instance();
	$ins->load->model('citas/m_citas', 'mc');
	return $ins->mc->getHoraWork($fecha, $idtecnico);
	//1
}
function getIdIntelisis($id_cita = '')
{
	$ins = &get_instance();
	$ins->load->model('intelisis/m_intelisis', 'mi');
	return $ins->mi->getIdIntelisis($id_cita);
}
//Regresa si el usuario tiene permisos para realizar una acción, ej. editar el horario o precio de una operación
function PermisoAccion($accion = '', $conAdmin = true)
{
	$CI = &get_instance();
	if ($CI->session->userdata('id') == '') {
		redirect(site_url());
	}
	if ($conAdmin) {
		$q_user = $CI->db->where('adminId', $CI->session->userdata('id'))
			->select('tipo')
			->get('admin');

		if ($q_user->row()->tipo == 1) {
			return true;
		}
	}
	$q = $CI->db->where('id', $CI->session->userdata('id'))
		->where('accion', $accion)
		->get('permisos_acciones_usuarios');
	if ($q->num_rows() == 1) {
		return true;
	}
	return false;
}
//Regresa si el usuario tiene permisos para realizar una acción, ej. editar el horario o precio de una operación
function PermisoModulo($controlador = '', $accion = '')
{
	$CI = &get_instance();
	//Revisar si es admin o gerente
	//adminStatus 1.-activada,0.-desactivada
	//status 1.-admin, 2.-empleado,3.-contacto, 4 gerente
	if ($CI->session->userdata('id') == '') {
		redirect(site_url());
	}
	$q_user = $CI->db->where('adminId', $CI->session->userdata('id'))
		->select('tipo')
		->get('admin');

	if ($q_user->row()->tipo == 1) {
		return true;
	}
	if ($accion != '') {
		$CI->db->where('accion', $accion);
	}
	if ($controlador == '') {
		return false;
	}
	$q = $CI->db->where('id', $CI->session->userdata('id'))
		->where('controlador', $controlador)
		->get('permisos_modulos');
	if ($q->num_rows() == 1) {
		return true;
	}
	return false;
}
function minutosTranscurridos($fecha_i, $fecha_f)
{
	$minutos = (strtotime($fecha_i) - strtotime($fecha_f)) / 60;
	$minutos = abs($minutos);
	$minutos = floor($minutos);
	return $minutos;
}
function dateDiffMinutes($fecha_inicio = '', $fecha_fin = '')
{
	if ($fecha_inicio == '0000-00-00 00:00:00' || $fecha_fin == '0000-00-00 00:00:00') {
		return 0;
	}
	// ESTABLISH THE MINUTES PER DAY FROM START AND END TIMES
	$start_time = '08:0:00';
	$end_time = '18:00:00';

	$start_ts = strtotime($start_time);
	$end_ts = strtotime($end_time);
	$minutes_per_day = (int)(($end_ts - $start_ts) / 60) + 1;

	// ESTABLISH THE HOLIDAYS
	$holidays = array(
		//'Feb 04', // MLK Day
	);

	// CONVERT HOLIDAYS TO ISO DATES
	foreach ($holidays as $x => $holiday) {
		$holidays[$x] = date('Y-m-d', strtotime($holiday));
	}

	$fecha_sol = $fecha_inicio;
	$fecha_menor = $fecha_fin;
	// CHECK FOR VALID DATES
	$start = strtotime($fecha_sol);
	$end = strtotime($fecha_menor);
	$start_p = date('Y-m-d H:i:s', $start);
	$end_p = date('Y-m-d H:i:s', $end);

	// MAKE AN ARRAY OF DATES
	$workdays = array();
	$workminutes = array();
	// ITERATE OVER THE DAYS
	$start = $start - 60;
	while ($start < $end) {
		$start = $start + 60;
		// ELIMINATE WEEKENDS - SAT AND SUN
		$weekday = date('D', $start);
		//if (substr($weekday,0,1) == 'S') continue;
		// ELIMINATE HOURS BEFORE BUSINESS HOURS
		$daytime = date('H:i:s', $start);
		if (($daytime < date('H:i:s', $start_ts))) continue;
		// ELIMINATE HOURS PAST BUSINESS HOURS
		$daytime = date('H:i:s', $start);
		if (($daytime > date('H:i:s', $end_ts))) continue;
		// ELIMINATE HOLIDAYS
		$iso_date = date('Y-m-d', $start);
		if (in_array($iso_date, $holidays)) continue;
		$workminutes[] = $iso_date;
		// END ITERATOR
	}
	$number_of_workminutes = (count($workminutes));
	$number_of_minutes = number_format($minutes_per_day);
	$horas_habiles = number_format($number_of_workminutes / 60, 2);
	if ($number_of_workminutes > 0) {
		$number_of_minutes = $number_of_workminutes - 1;
	}
	return $number_of_workminutes;
}
function random($num)
{
	$characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
	$string = '';
	for ($i = 0; $i < $num; $i++) {
		$string .= $characters[rand(0, strlen($characters) - 1)];
	}
	return $string;
}
function dayOfweek($day)
{
	switch ($day) {
		case 1:
			return 'Lunes';
			break;
		case 2:
			return 'Martes';
			break;
		case 3:
			return 'Míercoles';
			break;
		case 4:
			return 'Jueves';
			break;
		case 5:
			return 'Viernes';
			break;
		case 6:
			return 'Sábado';
			break;
		case 7:
			return 'Domingo';
			break;
	}
}
function getFolioMostrar($id_sucursal = '',$paquete=false)
{
	$ins = &get_instance();
	$ins->load->model('servicios/m_servicios', 'ms');
	//$siglas = $ins->ms->getSiglasEstado($id_estado);
	$siglas = $ins->ms->getSiglasSucursal($id_sucursal);
	$folio = (int)$ins->ms->getFolio()+1;
	return ($paquete?'P-':'S-').$siglas . '-' . $folio;
}
function getFolioControl()
{
	$ins = &get_instance();
	$ins->load->model('servicios/m_servicios', 'ms');
	$folio = $ins->ms->getFolio();
	return (int)$folio + 1;
}
function getFolioSubPaquete($idservicio='')
{
	$ins = &get_instance();
	$ins->load->model('servicios/m_paquetes', 'mp');
	return $ins->mp->getFolioSubpaquete($idservicio);
}
