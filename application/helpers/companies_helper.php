<?php

function encrypt_password($username, $password, $key)
{
    $pass = sha1($password.$key.$username);
    return $pass;
}

function nombre_usuario($id)
{
    $CI =& get_instance();
    $CI->db->select('*')
           ->from('admin')
           ->where('adminId', $id);
    $data = $CI->db->get();
    $nombre = $data->row();
    return $nombre->adminUsername;
}


function nombre_sucursal_n($id)
{
    $CI =& get_instance();
    $CI->db->select('*')
           ->from('sucursales')
           ->where('id', $id);
    $data = $CI->db->get();
    $nombre = $data->row();
    return $nombre->sucursal;
}

function nombre_cargo_n($id)
{
    $CI =& get_instance();
    $CI->db->select('*')
           ->from('ca_cargos')
           ->where('id', $id);
    $data = $CI->db->get();
    $row = $data->row();
    return $row->nombre;
}




