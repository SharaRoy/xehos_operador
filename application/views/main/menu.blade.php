<style>
    #total_notify {
        border-radius: 40px;
        font-size: 18px;
        padding-left: 5px;
        padding-right: 5px;
        margin-top: -10px !important;
        position: absolute;
    }
</style>
<div class="menu-back">
    <div class="menuborder">
        <div class=" menu-items">
            @if($vista == "1")
                <a href="<?= base_url() . 'index.php/operadores/webviewlist' ?>" class="btn btn-menu-cabecera topcorner3" title="WEBVIEW">
                    <i class="fa fa-globe" aria-hidden="true"></i>
                </a>
            @elseif($vista == "b")
                <a href="<?= base_url() . 'index.php/operadores/webviewlist' ?>" class="btn btn-menu-cabecera topcorner3" title="regresar">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                </a>
            @elseif($vista == "3")
                <a href="<?= base_url() . 'index.php/'.$ruta_anterior.'/' . $servicio_ruta ?>" class="btn btn-menu-cabecera topcorner3" title="regresar">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                </a>
            @else
                <a href="<?= base_url() . 'index.php/operadores/index' ?>" class="btn btn-menu-cabecera topcorner3" title="regresar">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                </a>
            @endif

            <img src="<?= base_url(); ?>statics/img/logohorizontal.png" class="logo" id="encabezado">

            <!--<a href="<?= base_url() . 'index.php/operadores/chat' ?>" class="btn btn-menu-cabecera topcorner2" title="NOTIFICACIONES">-->
            <a href="<?= base_url() . 'index.php/operadores/ChatLavador/index' ?>" class="btn btn-menu-cabecera topcorner2" title="NOTIFICACIONES">
                <i class="fa fa-bell" aria-hidden="true"></i>
                <span id="total_notify" class="count bg-danger">0</span>
            </a>

            <a href="<?= base_url() . 'index.php/login/cerrar_sesion' ?>" class="btn btn-menu-cabecera topcorner" title="CERRAR SESIÓN">
                <i class="fa fa-sign-out" aria-hidden="true"></i>
            </a>
        </div>
    </div>
</div>

<br>