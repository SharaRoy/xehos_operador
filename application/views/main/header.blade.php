<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $this->config->item('TITULO');?></title>
<meta name="description" content="Sufee Admin - HTML5 Admin Template">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="shortcut icon" type="image/png" href="<?= base_url(); ?>statics/inventario/imgs/logos/xehos2.png"/>
<!--<link rel="apple-touch-icon" href="apple-icon.png">
<link rel="shortcut icon" href="favicon.ico">-->

<link rel="stylesheet" href="<?php echo base_url(); ?>statics/tema/assets/css/normalize.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>statics/tema/assets/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>statics/tema/assets/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>statics/tema/assets/css/themify-icons.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>statics/tema/assets/css/flag-icon.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>statics/tema/assets/css/cs-skin-elastic.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css" media="screen">

<link rel="stylesheet"
    href="<?php echo base_url(); ?>statics/tema/assets/css/lib/datatable/dataTables.bootstrap.min.css">
<!-- <link rel="stylesheet" href="assets/css/bootstrap-select.less"> -->
<link rel="stylesheet" href="<?php echo base_url(); ?>statics/tema/assets/scss/style.css">

<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css" media="screen">
<link rel="stylesheet" href="<?php echo base_url(); ?>statics/css/isloading.css">
<style>
    td a i {
        font-size: 20px !important;
    }
    .error p{
        color:red !important;
    }

</style>

<script src="<?php echo base_url(); ?>statics/tema/assets/js/vendor/jquery-2.1.4.min.js">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>


<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Poppins&display=swap" rel="stylesheet">
<script type="text/javascript">
    $(document).ready(function() {
        $('#menuToggle').on('click', function(event) {
            $('body').toggleClass('open');
            $("#left-panel").toggle();
        });

        $("button").addClass("btn btn-info");

    });

</script>

<link rel="stylesheet" href="<?php echo base_url();?>statics/css/styleNETO.css">
<script src="https://kit.fontawesome.com/fe47b5dbda.js" crossorigin="anonymous"></script>

