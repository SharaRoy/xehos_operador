<!doctype html>
<html class="no-js" lang="">
<base href="{{ base_url() }}">

<head>
    @include('main/header')
    @yield('included_css')
    @if (!empty($this->session->userdata('id_usuario')) )
        <script>
            var telefono = "<?php echo $this->session->userdata('telefono'); ?>";
            var user_id = "<?php echo $this->session->userdata('id_usuario'); ?>";
            var user_name = "<?php echo $this->session->userdata('adminNombre'); ?>";
            var site_url = "<?php echo site_url(); ?>";
            var notnotification = false;
        </script>
    @endif
</head>

<body class="login-image">
    <div id="right-panel" class="right-panel">
        @yield('titulo')
        @include('main/menu')
        <div class="content">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-sm-1" style="opacity: 1;"></div>
                    <div class="col-sm-10 pantalla-col">
                        <div class="card">
                            <div class="card-body">
                                @yield('contenido')
                            </div>
                            <input type="hidden" id="sitio" value="<?php echo base_url();?>">
                        </div>
                    </div>
                    <div class="col-sm-1" style="opacity: 1;"></div>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ base_url('statics/js/jquery.min.js') }}"></script>
    <script src="{{ base_url('statics/js/bootbox.min.js') }}"></script>
    <script src="{{ base_url('statics/js/general.js') }}"></script>
    <script src="{{ base_url('statics/js/isloading.js') }}"></script>
    <script src="{{ base_url('statics/tema/assets/js/popper.min.js') }}"></script>
    <script src="{{ base_url('statics/tema/assets/js/plugins.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

    <!-- guardar coordenadas -->
    <script src="{{ base_url('statics/js/registro_coordenadas.js') }}"></script>

    @yield('included_js')
</body>

</html>